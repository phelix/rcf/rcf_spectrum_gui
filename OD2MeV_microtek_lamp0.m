function [tmpImageArray, tmpErrMeVpx] = OD2MeV_microtek_lamp0(data, ~, frame)

tmpImageArray = data.data.ImageArray(:,:,:,frame);
tmpErrMeVpx = zeros(size(tmpImageArray,1),size(tmpImageArray,2));
rcf_config = upper(char(data.parameters.rcfconfig(frame)));
res = data.data.fileinfo.XResolution;

%Just to be sure that there are no negative values
tmpImageArray(tmpImageArray <= 0) = 0;

switch rcf_config
    case 'HD'
        %new calibration 2007
        aHD=[17.674;-2.3745;65.409;-244.64;494.47;-486.69;307.53;-140.23;13.846];
        b=[0.;0.1;0.5;0.9;1.3;1.7;2.3;2.7;3.3];

        tmpImageArray(tmpImageArray > 2.9) = 1.1e+12;
        tmp = tmpImageArray(tmpImageArray > 0 & tmpImageArray <= 2.9);
        tmp = exp(aHD(1)*tmp.^b(1) + aHD(2)*tmp.^b(2) + aHD(3)*tmp.^b(3) + ...
            aHD(4)*tmp.^b(4) + aHD(5)*tmp.^b(5) + aHD(6)*tmp.^b(6) + ...
            aHD(7)*tmp.^b(7) + aHD(8)*tmp.^b(8) + aHD(9)*tmp.^b(9));
        tmpImageArray(tmpImageArray > 0 & tmpImageArray <= 2.9) = tmp;
    case 'MD'
        %new calibration 2007
        aMD=[431.83;-817.88;1517.98;-3103.56;4001.76;-2784.89;1191.28;-447.226;35.509;1.2];
        b=[0.;0.1;0.5;0.9;1.3;1.7;2.3;2.7;3.3];

        tmpImageArray(tmpImageArray > 3.1) = 7.048e+11;
        tmpImageArray(tmpImageArray > 0 & tmpImageArray < 0.005) = 1.19e+8;
        tmpImageArray(tmpImageArray >= 0.005 & tmpImageArray < 0.09) = 3.73939e+010*tmpImageArray(tmpImageArray >= 0.005 & tmpImageArray < 0.09).^(1.18773)+(5*10^7);

        tmp = tmpImageArray(tmpImageArray >= 0.09 & tmpImageArray <= 3.1);
        tmp = exp(aMD(1)*tmp.^b(1) + aMD(2)*tmp.^b(2) + aMD(3)*tmp.^b(3) + ...
            aMD(4)*tmp.^b(4) + aMD(5)*tmp.^b(5) + aMD(6)*tmp.^b(6) + ...
            aMD(7)*tmp.^b(7) + aMD(8)*tmp.^b(8) + aMD(9)*tmp.^b(9));
        tmpImageArray(tmpImageArray >= 0.09 & tmpImageArray <= 3.1) = tmp;
    case 'M2'
        %new calibration 2007
        aMD=[431.83;-817.88;1517.98;-3103.56;4001.76;-2784.89;1191.28;-447.226;35.509;1.2];
        b=[0.;0.1;0.5;0.9;1.3;1.7;2.3;2.7;3.3];

        tmpImageArray(tmpImageArray > 3.1) = 7.048e+11;
        tmpImageArray(tmpImageArray > 0 & tmpImageArray < 0.005) = 1.19e+8;
        tmpImageArray(tmpImageArray >= 0.005 & tmpImageArray < 0.09) = 3.73939e+010*tmpImageArray(tmpImageArray >= 0.005 & tmpImageArray < 0.09).^(1.18773)+(5*10^7);

        tmp = tmpImageArray(tmpImageArray >= 0.09 & tmpImageArray <= 3.1);
        tmp = exp(aMD(1)*tmp.^b(1) + aMD(2)*tmp.^b(2) + aMD(3)*tmp.^b(3) + ...
            aMD(4)*tmp.^b(4) + aMD(5)*tmp.^b(5) + aMD(6)*tmp.^b(6) + ...
            aMD(7)*tmp.^b(7) + aMD(8)*tmp.^b(8) + aMD(9)*tmp.^b(9));
        tmpImageArray(tmpImageArray >= 0.09 & tmpImageArray <= 3.1) = tmp;

        %correction from cross-calibration at Sandia
        %just a shift of the MD-V1 curve
        tmpImageArray=0.72*tmpImageArray;
    case 'HS'
        %new calibration 2007
        aHS=[-855.19;1674.82;-2695.24;4872.26;-5535.87;3391.38;-1197.22;395.232;-25.846;1.2];
        b=[0.;0.1;0.5;0.9;1.3;1.7;2.3;2.7;3.3];

        tmpImageArray(tmpImageArray > 3.8) = 8.884e+11;
        tmpImageArray(tmpImageArray > 0 & tmpImageArray < 0.005) = 9.74e+7;
        tmpImageArray(tmpImageArray >= 0.005 & tmpImageArray < 0.098) = 2.60245e+010*tmpImageArray(tmpImageArray >= 0.005 & tmpImageArray < 0.098).^(1.19039)+(5*10^7);

        tmp = tmpImageArray(tmpImageArray >= 0.098 & tmpImageArray <= 3.8);
        tmp = exp(aHS(1)*tmp.^b(1) + aHS(2)*tmp.^b(2) + aHS(3)*tmp.^b(3) + ...
            aHS(4)*tmp.^b(4) + aHS(5)*tmp.^b(5) + aHS(6)*tmp.^b(6) + ...
            aHS(7)*tmp.^b(7) + aHS(8)*tmp.^b(8) + aHS(9)*tmp.^b(9));
        tmpImageArray(tmpImageArray >= 0.098 & tmpImageArray <= 3.8) = tmp;
    otherwise
        tmpImageArray = NaN;
        disp('Unknown layer.')
        return
end

clear tmp

%convert from in MeV/px
tmpImageArray = 25.4^2/res^2*1e-3 .*tmpImageArray;
%no error calculation done for 2007 calib
%assume tmpErrMeVpx=0.5*dose from calib
tmpErrMeVpx = tmpImageArray*0.5;

clear stdabwBG data rcfconfig scanner fileinfo
end