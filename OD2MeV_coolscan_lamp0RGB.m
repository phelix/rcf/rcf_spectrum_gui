function [tmpImageArray, tmpErrMeVpx] = OD2MeV_coolscan_lamp0RGB(data, handles, frame)

tmpImageArray = data.data.ImageArray(:,:,:,frame);
stdabwBG = data.data.stdabwBG(frame,:);
rcf_config = upper(char(data.parameters.rcfconfig(frame)));
res = data.data.fileinfo.XResolution;
mop = false;

%we need this, cause calibration is done from 0 to 1 color-values
%plus downward compatiblity
if isa(tmpImageArray,'uint16')
    tmpImageArray=single(tmpImageArray)/65535;
    stdabwBG=stdabwBG/65535;
end

% Just to be sure that there are no negative values and values > 1
tmpImageArray(tmpImageArray < 0) = 0;
tmpImageArray(tmpImageArray > 1) = 1.0;

% scale from dose to kev/mm^2/�m
% then later multiply by layer thickness �m
% 1.08 g/cm^3 for HD and MD type active layer
% 1.2 g/cm^3 for EBT type active layer
scaleHD = 1.08*1e-3/(10^3*10^3)/1.602176634e-16;
scaleEBT = 1.2*1e-3/(10^3*10^3)/1.602176634e-16;

switch rcf_config
    case 'HD'
        %HD-810 => 6.5�m
        scale = scaleHD*6.5;

        %IR-RGB calibration 2013; LOT-Nummer S26A41H810
        %fitting parameters sorted for each channel [R G B]
        a =[449048710448183/1000000000000; 950315574373753/1000000000000; 259376677011927/500000000000];
        b = [752175529123/1000000000000; 10264787841/12500000000; 940012606087/1000000000000];
        c = [8744557736678671/125000000000; 3599979839279723/7812500000; 1080469106498417/1953125000];
        d = [407664461280023/1000000000000; 789034318213389/1000000000000; 941593104637033/1000000000000];
        %errors from fitting [R G B]
        deltaa = [166528627943853/1000000000000; 489339723411809/1000000000000; 346017560312563/1000000000000];
        deltab = [7336901491/250000000000; 72437822751/1000000000000; 59455655403/1000000000000];
        deltac = [5961578631144981/250000000000; 1739150661390869/7812500000; 167235015557613/488281250];
        deltad = [14339925070863/100000000000; 304413662015969/1000000000000; 40383732562139/250000000000];

        %estimated asymtotic covariance from fit 
        %cov = {cov_ab; cov_ac; cov_ad; cov_bc; cov_bd; cov_cd}
        %needs to be cell-array
        covR = {-4.318565582605538; 3.958538529493643e6; 23751.41802726076; -630.1561477267536; -3.533087244544948; 3.367519448878069e6};
        covG = {-33.516572430966164; 1.08820481237688e8; 147331.28174121902; -15385.724564639191; -19.645237302334742; 6.658452290483592e7};
        covB = {-18.625659923888634; 1.184117174052207e8; 51375.12751955161; -18659.648737597865; -6.448383646615308; 5.009734082349525e7};

        %limes for derivatives[0=dose]
        limesDfit=[0.006418974006036173; 0; 0];

        %satturation color values defined by calibration [R G B]
        %dose satt is 2260Gy * 0.95=2147Gy, so 95% of max usable dose
        %from calib => about 2 times higher then spec of HD;
        %error for max dose approx 7% (real) and 5% (including fit errors)!
        %mean real error dose [0,dosemax]=4.1%
        %for red-channel here: satt(R)=max color-value where
        %derivative>0
        satt=[0.8128790954970707; 0.8612173258021284; 0.7581595499088626];

        %starting dose values! for which color-channel should
        %be taken into account => lower boundary for channel
        %from calibration*1.05
        % => red=whole range; green+blue > 10.65Gy;
        %start=(green, blue, IR)
        start=[10.647192783100735; 10.647192783100735];

        %stopping dose values, same as above, but upper
        %boundary for color-channel [R,G,B]
        %5% of maximum derivative of channel
        %inf means only that upper boundary is same like
        %satteration values
        stop=[402.252; inf; inf];
    case 'H2'
        %HDv2 => 8�m
        scale = scaleHD*8;

        %RGB calibration 2013; LOT-Nummer A01141102
        %fitting parameters sorted for each channel [R G B]
        a = [-91758538069/125000000000; 0; 0];
        b = [391733587959/500000000000; 201943245127/250000000000; 518427296099/1000000000000];
        c = [0; 0; 0];
        d = [166591520428561/1000000000000; 583605443558153/1000000000000; 230566383364597/200000000000];
        %errors from fitting [R G B]
        deltaa = [44746411823/125000000000; 0; 0];
        deltab = [3031292763/1000000000000; 2738988219/500000000000; 8221138119/1000000000000];
        deltac = [0; 0; 0];
        deltad = [614260317119/200000000000; 1467510040163/125000000000; 2780636346803/62500000000];

        %estimated asymtotic covariance from fit 
        %cov = {cov_ab; cov_ac; cov_ad; cov_bc; cov_bd; cov_cd}
        %needs to be cell-array
        covR = {0.0003164712323452551; 0; 0.7315708688574392; 0; 0.006548259795215497; 0};
        covG = {0; 0; 0; 0; 0.054729164381248; 0};
        covB = {0; 0; 0; 0; 0.33657306351876065; 0};

        %limes for derivatives[0=dose]
        limesDfit=[0.00470292349755935; 0; 0];

        %satturation color values defined by calibration [R G B IR]
        %dose satt is 4224Gy * 0.95=4013Gy, so 95% of max usable dose
        %from calib => about 4 times higher then spec of H2;
        %error for max dose approx 8% (real) and 5% (including fit errors)!
        %mean real error dose [0,dosemax]=4%
        satt=[0.7520603435342669; 0.7052041094533881; 0.4027221002766745];

        %starting dose values! for which color-channel should
        %be taken into account => lower boundary for channel
        %from calibration*1.05
        % => red=whole range; green+blue > 11.02Gy;
        %start=(green, blue)
        start=[11.022216354933702; 11.022216354933702];

        %stopping dose values, same as above, but upper
        %boundary for color-channel [R,G,B]
        %inf means only that upper boundary is same like
        %satteration values
        stop=[inf,inf,inf];
    case 'M2'
        %M2 => 35�m
        scale = scaleHD*35;

        %RGB calibration 2012; LOT-Nummer S0628MDV2 ("M2new")
        %also verified for M2old (LOT-Nummer R0551MDV2, before 2012)
        %=> Deviations for complete dose space M2new/M2old ~ 3.7 %
        %typicall uncertainty of M2-films ~6.5% (whole dose space)
        %=> M2new and M2old are identical films!
        %fitting parameters sorted for each channel [R G B]
        a = [0; -267474397389/1000000000000; -490849220861/1000000000000];
        b = [2570476837/3125000000; 1006071709729/1000000000000; 214289467569/200000000000];
        c = [9574800345933/500000000000; 0; 0];
        d = [10170703424293/500000000000; 50905070160417/500000000000; 60361020563827/250000000000];
        %errors from fitting [R G B]
        deltaa = [0; 51424018871/500000000000; 29583591921/125000000000];
        deltab = [1469226351/125000000000; 2093571199/250000000000; 1364204503/62500000000];
        deltac = [2636661848637/500000000000; 0; 0];
        deltad = [1088390978069/1000000000000; 1760464970111/1000000000000; 79854942019/10000000000];

        %estimated asymtotic covariance from fit 
        %cov = {cov_ab; cov_ac; cov_ad; cov_bc; cov_bd; cov_cd}
        %needs to be cell-array
        covR = {0; 0; 0; -0.030177395926256536; 0.011202510282298376; -4.089298512063288};
        covG = {0.0003360042871927955; 0; 0.10259206090852419; 0; 0.013818020921252725; 0};
        covB = {0.0021218788646002436; 0; 0.9895047939298204; 0; 0.16977482739610122; 0};

        %limes for derivatives[0=dose]
        limesDfit=[0; 0; 0];

        %satturation color values defined by calibration [R G B]
        %dose satt is 244Gy * 0.95=231Gy, so 95% of max usable dose
        %from calib => about 2 times higher then spec of M2;
        %error for max dose approx 5% (real) and 2% (including fit errors)!
        %mean real error dose [0,dosemax]=2.8%
        satt=[0.7558238999516863; 0.6978159571773325; 0.5232375042163496];
        start=[1e-6; 1e-6]; % for 2013 calib
        stop=[inf,inf,inf];
    case {'E3', 'ES'}
        % E3 => 27�m
        %correction for wrong IC calib-factor in calibration
        %=> old=0.891; new=1.970;
        scale = scaleEBT*27*0.891/1.970;

        %RGB calibration 2012; LOT-Nummer A12151101
        %fitting parameters sorted for each channel [R G B]
        a = [1433492557/125000000000; -118551794437/500000000000; -30776997803/100000000000];
        b = [288642911169/500000000000; 325735244173/500000000000; 177253395789/500000000000];
        c = [0; 0; 0];
        d = [338215106577/50000000000; 1518987934517/62500000000; 26979110484999/500000000000];
        %errors from fitting [R G B]
        deltaa =[27925984279/1000000000000; 26360983731/1000000000000; 21006173531/1000000000000];
        deltab = [2788861141/500000000000; 3037937191/500000000000;7056038723/1000000000000];
        deltac = [0; 0; 0];
        deltad = [12602736493/50000000000; 514150002039/1000000000000; 900793507199/500000000000];

        %estimated asymtotic covariance from fit 
        %cov = {cov_ab; cov_ac; cov_ad; cov_bc; cov_bd; cov_cd}
        %needs to be cell-array
        covR = {0.00007419630396699053; 0; 0.005471333505303955; 0; 0.001201353196697196; 0};
        covG = {0.00007872808874922909; 0; 0.009077088278091956; 0; 0.0029732971022551503; 0};
        covB = {0.000045920958826775275; 0; 0.01660610873379973; 0; 0.012454391457114345; 0};

        %limes for derivatives[0=dose]
        limesDfit=[0.08534299786018072; 0.026805285675004852; 0.006570023718436916];

        %satturation color values defined by calibration [R G B]
        %dose satt is 55Gy * 0.95=52Gy, so 95% of max usable dose
        %from calib => about 6.5 times higher then spec of E3;
        %note: calibration focused on "higher" doses
        %error for max dose approx 8% (real) and 3% (including fit errors)!
        %mean real error dose [0,dosemax]=3%
        % CB 28.10.13 extrapolate ......................
        satt=[1.1*0.5113602655074495; 1.45*0.44166990447945; 1.1*0.17158091671668854];
%         satt=[0.5113602655074495; 0.44166990447945; 0.17158091671668854];
        start=[1e-6; 1e-6]; % for 2013 calib
        stop=[52,inf,52];
%         stop=[inf,inf,inf];
    case {'H1B','HD11171501'}
        % CAL-2019_1-HD#11171501_4
        % HDv2 => 8�m
        scale = scaleHD*8;
        covR = {0.0112158; 0.0; 35.4525; 0.0; 0.0676074; 0.0};
        covG = {0.0; 0.0; 0.0; 0.0; 0.256251; 0.0};
        covB = {0.0; 0.0; 0.0; 0.0; 0.271978; 0.0};
        a = [7.84454; 0; 0];
        b = [0.602334; 0.577436; 0.387123];
        c = [0; 0; 0];
        d = [185.417; 474.011; 917.739];
        deltaa = [2.87739; 0; 0];
        deltab = [0.00596279; 0.0110739; 0.00751225];
        deltac = [0; 0; 0];
        deltad = [13.3402; 24.4498; 36.9861];
        limesDfit = [0.00302036; 0; 0];
        satt = b*0.9;%[0.531349; 0.421156; 0.225276];
        start = [89.3437; 89.3437];
        stop=[inf,inf,inf];
    case {'H1C','HD01091801'}
        % CAL-2019_1-HD#01091801_7
        % HDv2 => 8�m
        scale = scaleHD*8;
        covR = {0.000557202; 0.0; 0.721714; 0.0; 0.00342139; 0.0};
        covG = {0.0; 0.0; 0.0; 0.0; 0.0205441; 0.0};
        covB = {0.0; 0.0; 0.0; 0.0; 0.108989; 0.0};
        a = [0.364262; 0; 0];
        b = [0.530054; 0.532726; 0.415067];
        c = [0; 0; 0];
        d = [21.5716; 81.2939; 205.644];
        deltaa = [0.429002; 0; 0];
        deltab = [0.00305492; 0.00611378; 0.0083403];
        deltac = [0; 0; 0];
        deltad = [1.82547; 4.34683; 14.9386];
        limesDfit = [0.0237888; 0; 0];
        satt = b*0.9;%[0.521532; 0.500852; 0.357512];
        start = [89.3437; 89.3437];
        stop=[inf,inf,inf];
    case {'H1A','HD12151402'}
        % CAL-2019_1-HD#12151402_3
        % HDv2 => 8�m
        scale = scaleHD*8;
        covR = {0.0285689; 0.0; 83.3995; 0.0; 0.207157; 0.0};
        covG = {0.0; 0.0; 0.0; 0.0; 0.138093; 0.0};
        covB = {0.0; 0.0; 0.0; 0.0; 0.473191; 0.0};
        a = [3.3181; 0; 0];
        b = [0.706716; 0.709377; 0.383316];
        c = [0; 0; 0];
        d = [313.862; 888.149; 1365.7];
        deltaa = [3.94609; 0; 0];
        deltab = [0.00958148; 0.0071703; 0.0085451];
        deltac = [0; 0; 0];
        deltad = [23.1735; 19.605; 55.8443];
        limesDfit = [0.00221799; 0; 0];
        satt = b*0.9;%[0.569408; 0.418444; 0.185256];
        start = [172.15; 172.15];
        stop=[inf,inf,inf];
    case {'E1E','EBT09121802'}
        % CAL-2019_1-EBT#09121802_8
        % E3 => 27�m
        scale = scaleEBT*27;
        covR = {0.000369488; 0.0; 0.0172474; 0.0; 0.00338734; 0.0};
        covG = {0.0; 0.0; 0.0; 0.0; 0.0076095; 0.0};
        covB = {0.0; 0.0; 0.0; 0.0; 0.0145726; 0.0};
        a = [0.0894825; 0; 0];
        b = [0.569917; 0.562081; 0.407066];
        c = [0; 0; 0];
        d = [3.52838; 8.09534; 19.2947];
        deltaa = [0.0549286; 0; 0];
        deltab = [0.0111678; 0.0154736; 0.0131084];
        deltac = [0; 0; 0];
        deltad = [0.355822; 0.526625; 1.13471];
        limesDfit = [0.154327; 0; 0];
        satt = b*0.9;%[0.49051; 0.403887; 0.210529];
        start = [1.25489; 1.25489];
        stop=[52,inf,52];
    case {'E1D','EBT07291902'}
        % CAL-2019_1-EBT#07291902_6
        % E3 => 27�m
        scale = scaleEBT*27;
        stop=[52,inf,52];
        covR = {0.00031486; 0.0; 0.0133097; 0.0; 0.00244645; 0.0};
        covG = {0.0; 0.0; 0.0; 0.0; 0.00519055; 0.0};
        covB = {0.0; 0.0; 0.0; 0.0; 0.00303447; 0.0};
        a = [0.0941564; 0; 0];
        b = [0.615429; 0.58459; 0.416088];
        c = [0; 0; 0];
        d = [3.04617; 7.46774; 17.0581];
        deltaa = [0.0520944; 0; 0];
        deltab = [0.0101466; 0.0133629; 0.00632178];
        deltac = [0; 0; 0];
        deltad = [0.287351; 0.417451; 0.491366];
        limesDfit = [0.191874; 0; 0];
        satt = b*0.9;%[0.540347; 0.429432; 0.227953];
        start = [1.25489; 1.25489];
    case {'E1A','EBT05191502'}
        % CAL-2019_1-EBT#05191502_1
        % E3 => 27�m
        scale = scaleEBT*27;
        stop=[52,inf,52];
        covR = {0.000266379; 0.0; 0.0169835; 0.0; 0.0023742; 0.0};
        covG = {0.0; 0.0; 0.0; 0.0; 0.0106065; 0.0};
        covB = {0.0; 0.0; 0.0; 0.0; 0.0280502; 0.0};
        a = [0.0711877; 0; 0];
        b = [0.52386; 0.634962; 0.44499];
        c = [0; 0; 0];
        d = [4.2564; 14.361; 41.5946];
        deltaa = [0.0545673; 0; 0];
        deltab = [0.0078679; 0.0150423; 0.0142804];
        deltac = [0; 0; 0];
        deltad = [0.348295; 0.723673; 1.97022];
        limesDfit = [0.119141; 0; 0];
        satt = b*0.9;%[0.437257; 0.374646; 0.147716];
        start = [1.25489; 1.25489];
    case {'E1B','EBT03161503'}
        % CAL-2019_1-EBT#03161503_2
        % E3 => 27�m
        scale = scaleEBT*27;
        stop=[52,inf,52];
        covR = {0.000195211; 0.0; 0.0109264; 0.0; 0.00187864; 0.0};
        covG = {0.0; 0.0; 0.0; 0.0; 0.00636632; 0.0};
        covB = {0.0; 0.0; 0.0; 0.0; 0.00636402; 0.0};
        a = [0.0510513; 0; 0];
        b = [0.529653; 0.633424; 0.416567];
        c = [0; 0; 0];
        d = [3.81536; 12.6446; 33.5519];
        deltaa = [0.0429533; 0; 0];
        deltab = [0.00757635; 0.0123299; 0.0071293];
        deltac = [0; 0; 0];
        deltad = [0.288556; 0.534254; 0.897659];
        limesDfit = [0.135307; 0; 0];
        satt = b*0.9;%[0.449201; 0.392996; 0.158793];
        start = [1.25489; 1.25489];
    otherwise
        mop = true;
        disp('Unknown layer.')
end

if ~mop
    [tmpImageArray, tmpErrMeVpx] = RGBcalib2013_3(tmpImageArray,data.data.ImageMask(:,:,frame),satt,start,stop,stdabwBG,a,b,c,d,deltaa,deltab,deltac,deltad,covR,covG,covB,limesDfit,handles,frame);
    tmpImageArray = tmpImageArray*scale;
    tmpErrMeVpx = tmpErrMeVpx*scale;

    %just for debugging
    %setappdata(0, strcat('Image', num2str(frame)), tmpImageArray);
    %setappdata(0, strcat('err', num2str(frame)), tmpErrMeVpx);

    %convert from dose in MeV/px
    tmpImageArray = 25.4^2/res^2*1e-3 .*tmpImageArray;
    tmpErrMeVpx = 25.4^2/res^2*1e-3 .*tmpErrMeVpx;
else
    tmpErrMeVpx = zeros(size(tmpImageArray,1),size(tmpImageArray,2));
end

clear stdabwBG data rcfconfig scanner fileinfo
end