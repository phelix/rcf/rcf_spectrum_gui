function data = readRCF(data, handles)

height = data.data.fileinfo.Height;
width = data.data.fileinfo.Width;

if isfield(data.parameters, 'scannerpop')
    switch char(data.parameters.scannerpop)
        case 'ArtixScan 1800F, Lamp 0, RGB'
            data.data.ImageArray = zeros(height, width, 3, length(data.parameters.rcffiles), 'uint16');
        case 'CoolScan 9000ED, Lamp 0, RGB'
            data.data.ImageArray = zeros(height, width, 3, length(data.parameters.rcffiles), 'uint16');
        case 'Dresden Scanner'
            data.data.ImageArray = zeros(height, width, 3, length(data.parameters.rcffiles), 'single');
        case 'Dresden Dosis'
            data.data.ImageArray = zeros(height, width, 1, length(data.parameters.rcffiles), 'single');
        otherwise
            data.data.ImageArray = zeros(height, width, 1, length(data.parameters.rcffiles), 'single');
    end
else
    % old versions compatibility
    switch char(data.parameters.scanner)
        case 'microtek_lamp0RGB'
            %preallocate ImageArray 12D-array for RGB => uint16, make later a new
            %array with single for just one channel
            data.data.ImageArray = zeros(...
            height, ...      % height
            width, ...       % width
            3, ...  % RGB
            length(data.parameters.rcffiles), 'uint16');  % files
        case 'dresdenScan'
            %preallocate ImageArray 12D-array for RGB => uint16, make later a new
            %array with single for just one channel
            data.data.ImageArray = zeros(...
            height, ...      % height
            width, ...       % width
            3, ...  % RGB
            length(data.parameters.rcffiles), 'single');  % files
        case 'coolscan_lamp0RGB'
            %preallocate ImageArray 16D-array for RGB+IR => uint16, make later a new
            %array with single for just one channel
            data.data.ImageArray = zeros(...
            height, ...      % height
            width, ...       % width
            3, ... % back to good old RGB
            length(data.parameters.rcffiles), 'uint16');  % files
        otherwise
            %preallocate ImageArray array for gray => can be single, cause we need
            %later single after counts2OD!
            data.data.ImageArray = zeros(...
            height, ...      % height
            width, ...       % width
            1, ...  % only gray
            length(data.parameters.rcffiles), 'single');  % files
    end
end

%preallocate errMeV per pixel array
data.data.errMeVpx = zeros(...
    height, ...      % height
    width, ...       % width
    length(data.parameters.rcffiles), 'single');  % files

%preallocate ImageMask 2D-array for all film-types
data.data.ImageMask = true(...
    height, ...      % height
    width, ...       % width
    length(data.parameters.rcffiles));  % files

%read images
for frame = 1:length(data.parameters.rcffiles)
    file_name = fullfile(data.parameters.rcfpath, data.parameters.rcffiles{frame});
    set(handles.statustext, 'String', ['Loading RCF ' num2str(frame)], 'ForegroundColor','black');
    drawnow
    nope=false;
    if isfield(data.parameters, 'scannerpop')
        if strcmp(data.parameters.scannerpop,'CoolScan 9000ED, Lamp 0, RGB')
            nope = true;
        end
    elseif strcmp(data.parameters.scanner,'coolscan_lamp0RGB')
        nope = true;
    else
        nope = false;
    end
        
    if nope
        filepoint = Tiff(file_name,'r');
        %RGB part of image
        filepoint.setDirectory(1);
        data.data.ImageArray(:,:,1:3,frame) = filepoint.read();
        filepoint.close();
    else
        data.data.ImageArray(:,:,:,frame) = imread(file_name);
    end
    
end

%read Energyloss Files
data.data.ELOarray = single([]);
for frame = 1:length(data.parameters.elossfiles)
    file_name = fullfile(data.parameters.elosspath, data.parameters.elossfiles{frame});
    data.data.ELOarray(:,:,frame) = importdata(file_name);
end

if size(data.data.ELOarray,2)==3
    %backward compatibility to old eloss file without quenching
    %correction => give warning message and copy uncorrected values to
    %place where corrected values is in actual version
    set(handles.statustext,'String','Warning! RCF quenching effects are disabled, use new Energyloss GUI !','ForegroundColor','red');

    %copy column 2 to 4 in ELOarray:
    data.data.ELOarray(:,4,:)=data.data.ELOarray(:,2,:);
    pause(2.5)        
end