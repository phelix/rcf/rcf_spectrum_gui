function [ImageArray, fail, npixels, stderror] = counts2OD_dresdenScan(ImageArray, rcftype)

%Dresden Scanner :P

fail = 0;
npixels = 0; %percentage of pixels saturated

if ~(size(ImageArray,3) == 3) % back RGB
    fail = 3;
else
    switch(upper(char(rcftype)))
        case 'EBT3'
            bg = cat(3,171.1294,172.3882,76.68235);
            stderror = [0 0 0];
        otherwise
            fail = 4;
            bg = cat(3, 0, 0, 0);
            stderror = [0 0 0];
    end
    % avoid zeros
    ImageArray = bsxfun(@rdivide, bg, double(ImageArray+1e-6));
    ImageArray = log10(double(ImageArray));
end