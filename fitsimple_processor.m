function [simple_estimates, model, spectrum, output] = fitsimple_processor(data)

% first approximation, simple integration
ELOarray = data.data.ELOarray;
last = size(data.data.ImageArray,3);
 
E = zeros(last,1);
E_error = zeros(last,1);
spectrum = zeros(last,2);

figure();
subplot(2,2,2);

for frame = 1:last
   dose = sum(sum(data.data.ImageArray(:,:,frame)));
   %peaks_position = peaks(ELOarray(:,4,frame));
   [a, peaks_position] = max(ELOarray(:,4,frame));
   hold all
   plot(ELOarray(:,1,frame), ELOarray(:,4,frame), '-', ...
       ELOarray(peaks_position,1,frame), ELOarray(peaks_position,4,frame), '+')
   xlabel('E (MeV)')
   ylabel('\Delta E/E (MeV)')
   title('Check for proper peak auto-detection');
   box on
   number = dose/sum(ELOarray(peaks_position,4,frame));
   E(frame) = mean(ELOarray(peaks_position,1,frame));
   E_error(frame) = max(abs(ELOarray(peaks_position,1,frame)-E(frame)));
   spectrum(frame,1) = E(frame);
   spectrum(frame,2) = number;
end
hold off

xdata = spectrum(:,1);
ydata = spectrum(:,2);

[model, text] = model_handler(data.fitparam.fitfunction);
options = optimset('Display', 'none', ...
                   'MaxFunEvals', data.fitparam.maxeval, ...
                   'TolX', 1e-10, ...
                   'TolFun', 1e-10, ...
                   'MaxIter', data.fitparam.maxiter);
[simple_estimates, ~, ~, output] = fminsearch(@(x) model(x, {xdata, log(ydata), data.fitparam}), ...
    [data.fitparam.N_start data.fitparam.T_start], options);

% calculate conversion efficiency for protons > 4 MeV (threshold as in published papers)
Elaser = data.parameters.laser.energy;
%summarize over all RCFs
Etotal = 0;

if last ~=1
    for i = last:-1:1
        if (i ~= length(E)) %avoid problems at end of spectrum
           intervalpos(i) = abs(0.5*(xdata(i+1)-xdata(i)));
        else
           intervalpos(i) = abs(0.5*(xdata(i)-xdata(i-1)));
        end
        if (i ~= 1) %avoid problems at first RCF
           intervalneg(i) = abs(0.5*(xdata(i)-xdata(i-1)));
        else
           intervalneg(i) = abs(0.5*(xdata(i+1)-xdata(i)));
        end
        Ecurrent = ydata(i)*(intervalneg(i)+intervalpos(i))*xdata(i);
        if (xdata(i)>= 4)                                           
           Etotal = Etotal + Ecurrent;
        end
    end
else
    Etotal = 0;
    intervalpos = 0.5* max(xdata);
    intervalneg = 0.5* min(xdata);
end

Eproton = Etotal*1e6*1.602e-19;
conversion = Eproton/Elaser*100;

subplot(2,2,[3 4]);
hold all
x_fit = ceil(min(xdata)):0.1:ceil(max(xdata));
[~, FittedCurve] = model(simple_estimates, {x_fit, 0, data.fitparam});
plot(x_fit, exp(FittedCurve),'--k')

%histogram used for total energy integration
for i = 1:last
     xl = xdata(i)-intervalneg(i);
     xr = xdata(i)+intervalpos(i);
     xx = [xl xr xr xl xl];
     yy = [0  0  ydata(i) ydata(i) 0];
     barout(i) = fill(xx,yy,'y');
     set(barout(i),'EdgeColor','r');
end

errorbarxy(xdata, ydata, E_error, 0.5*ydata, [],[],[],[])
axis([min(xdata)-1 max(xdata)+2 min(ydata)-0.8*min(ydata) max(ydata)+0.8*max(ydata)]) 
set(gca,'YScale','log')
xlabel('E (MeV)');
ylabel('dN/dE (1 MeV^{-1})');
box on

subplot(2,2,1)
set(gca,'Visible','Off');

ll2=['N_0 = ',num2str(simple_estimates(1),'%5.2d')];
ll3=['kT = ',num2str(simple_estimates(2),'%5.2f'),' MeV'];
ll5=['Laser energy: ',num2str(Elaser,'%5.1f'),' J'];
ll6='Laser to proton energy (> 4 MeV)';
ll7=['conversion efficiency: ',num2str(conversion,'%5.3f'),' %'];

annotation('textbox',[0.01 0.85 0.48 0.15],'string',{text;ll2;ll3;ll5;ll6;ll7},...
    'LineStyle','none','FontName','NewCenturySchlbk','FontSize',10);