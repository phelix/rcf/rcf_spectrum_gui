function [tmpImageArray, tmpErrMeVpx] = OD2MeV_dresdenScan(data, handles, frame)

tmpImageArray = data.data.ImageArray(:,:,:,frame);
rcf_config = upper(char(data.parameters.rcfconfig(frame)));
res = data.data.fileinfo.XResolution;
mop = false;

Dosis = @(x,p) p(1)*x+p(2)*x.^p(3);

% scale from dose to kev/mm^2/�m
% then later multiply by layer thickness �m
% 1.08 g/cm^3 for HD and MD type active layer
% 1.2 g/cm^3 for EBT type active layer
scaleHD = 1.08*1e-3/(10^3*10^3)/1.602176634e-16;
scaleEBT = 1.2*1e-3/(10^3*10^3)/1.602176634e-16;

% Just to be sure that there are no negative values
tmpImageArray(tmpImageArray < 0) = 0;

switch rcf_config
    case 'EBT3'
        r = [10.22357 2.97505 1.8];
        g = [29.18921 38.12002 2.24444];
        b = [148.16638 161.82659 3.54985];
        scale = scaleEBT*27;
    otherwise
        mop = true;
        disp('Unknown layer.')
end

if ~mop
    switch 'r'
        case 'r'
            Ic = single(tmpImageArray(:,:,1));
            calib = r;
        case 'g'
            Ic = single(tmpImageArray(:,:,2));
            calib = g;
        case 'b'
            Ic = single(tmpImageArray(:,:,3));
            calib = b;
        otherwise
    end
    % nach r�cksprache mit flo nur ein Farbkanal
    tmpImageArray = Dosis(Ic,calib);
    tmpErrMeVpx = zeros(size(tmpImageArray,1),size(tmpImageArray,2));
    tmpImageArray = tmpImageArray*scale;
    %convert from dose in MeV/px
    tmpImageArray = 25.4^2/res^2*1e-3 .*tmpImageArray;
else
    tmpErrMeVpx = zeros(size(tmpImageArray,1),size(tmpImageArray,2));
end

clear stdabwBG data rcfconfig scanner fileinfo Ic calib
end