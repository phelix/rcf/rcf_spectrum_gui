# RCF Spectrum GUI

RCF_Spectrum_GUI Matlab scripts for proper RCF film analysis.

## Getting Started

Einfach jemand fragen der sich damit auskennt.

## zur Zeit sind folgende RCF eingerichtet

Die Liste ist stets im [PPH Wiki](http://pphwiki.gsi.de/wiki/RCF) (nur GSI intern) zu führen. Hier nur händisch kopiert.

* **HD**; HD-810 6,5 µm aktive; IR-RGB calibration 2013; LOT-Nummer S26A41H810
* **H2**; HDv2 8 µm aktive; RGB calibration 2013; LOT-Nummer A01141102
* **M2**; M2 35 µm aktive; RGB calibration 2012; LOT-Nummer S0628MDV2 ("M2new") also verified for M2old (LOT-Nummer R0551MDV2, before 2012)
* **E3**; EBT3 27 µm aktive; RGB calibration 2012; LOT-Nummer A12151101
* **ES**; same as E3 (! nur Nikon Coolscan 9000!)
* **H1B**; **HD11171501**; HDv2 8 µm aktive; CAL-2019_1-HD#11171501_4 (! nur Nikon Coolscan 9000!)
* **H1C**; **HD01091801**; HDv2 8 µm aktive; CAL-2019_1-HD#01091801_7 (! nur Nikon Coolscan 9000!)
* **H1A**; **HD12151402**; HDv2 8 µm aktive; CAL-2019_1-HD#12151402_3 (! nur Nikon Coolscan 9000!)
* **E1E**; **EBT09121802**; EBT3 27 µm aktive; CAL-2019_1-EBT#09121802_8 (! nur Nikon Coolscan 9000!)
* **E1D**; **EBT07291902**; EBT3 27 µm aktive; CAL-2019_1-EBT#07291902_6 (! nur Nikon Coolscan 9000!)
* **E1A**; **EBT05191502**; EBT3 27 µm aktive; CAL-2019_1-EBT#05191502_1 (! nur Nikon Coolscan 9000!)
* **E1B**; **EBT03161503**; EBT3 27 µm aktive; CAL-2019_1-EBT#03161503_2 (! nur Nikon Coolscan 9000!)
* **EBT3**; EBT3 27 µm aktive; Dresdner Kalibration (noch im Dresdner Branch)
