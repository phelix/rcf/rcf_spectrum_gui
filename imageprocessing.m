function varargout = imageprocessing(varargin)
% IMAGEPROCESSING M-file for imageprocessing.fig
%      IMAGEPROCESSING, by itself, creates a new IMAGEPROCESSING or raises the existing
%      singleton*.
%
%      H = IMAGEPROCESSING returns the handle to a new IMAGEPROCESSING or the handle to
%      the existing singleton*.
%
%      IMAGEPROCESSING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IMAGEPROCESSING.M with the given input arguments.
%
%      IMAGEPROCESSING('Property','Value',...) creates a new IMAGEPROCESSING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before imageprocessing_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to imageprocessing_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright 2002-2003 The MathWorks, Inc.

% Edit the above text to modify the response to help imageprocessing

% Last Modified by GUIDE v2.5 11-Oct-2013 12:35:55

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @imageprocessing_OpeningFcn, ...
                   'gui_OutputFcn',  @imageprocessing_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before imageprocessing is made visible.
function imageprocessing_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to imageprocessing (see VARARGIN)

% Choose default command line output for imageprocessing
handles.output = hObject;

data = getappdata(0, 'data');

%main image axes
set(handles.axes1, 'Visible', 'off', 'Units', 'normalized');
axis square;
colormap(handles.axes1,rcf);
%end set axes handles

%default filter options
set(handles.boundary_val, 'Value', 500);
set(handles.threshold_val, 'Value', 45000);
set(handles.threshold_expand, 'Value', 1);

set(handles.contrast_slider_high,'Min',0);
set(handles.contrast_slider_high,'Max', max(max(max(data.data.ImageArray))));
set(handles.contrast_slider_low,'Min',0);
set(handles.contrast_slider_low,'Max', max(max(max(data.data.ImageArray))));

set(handles.min_slider_pos_text,'String', 'Slider val:');
set(handles.max_slider_pos_text,'String', 'Slider val:');
set(handles.OD_cursor_text,'String', 'Value:');
set(handles.min_OD_text,'String', 'Minimum val:');
set(handles.max_OD_text,'String', 'Maximum val:');

%default is "normal" imagesc; no image loaded
handles.ImageLoaded = 0;

% reuse data or init
if ~isfield(data.data, 'edited') || isempty(data.data.edited)
    data.data.edited = zeros(size(data.data.ImageArray,3),1);
end
if ~isfield(data.data, 'meanBG') || isempty(data.data.meanBG)
    data.data.meanBG = zeros(size(data.data.ImageArray,3),1);
end
% CB 11.09.2013 add new mask to block out dust removed areas in deconvplots
if ~isfield(data.data, 'ImageDustMask') || isempty(data.data.ImageDustMask)
    data.data.ImageDustMask = false(size(data.data.ImageMask));
end

% and finaly set values to GUI
handles.currentRCFframe = 1;
handles.data.ImageArray2 = data.data.ImageArray(:,:,1);
handles.data.ImageMask = true(size(handles.data.ImageArray2));
handles.data.ImageDustMask = false(size(handles.data.ImageMask));
handles.data.meanBG = 0;
handles.data.errMeVpx2 = data.data.errMeVpx(:,:,1);

handles.data.meanerrMeVpx = 0;

handles = undo(handles, 'set');

% smarter list
set(handles.rcflistbox, 'String', strcat(data.parameters.rcfconfig_numb,'-', ...
    cellfun(@(x) x(1:end-4),data.parameters.rcffiles,'UniformOutput',false)));

setappdata(0, 'data', data);
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = imageprocessing_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
varargout{1} = handles.output;

function closebutton_Callback(hObject, eventdata, handles)
close imageprocessing

function roipolybutton_Callback(hObject, eventdata, handles)
%reload image first to "remove" old ROI polygon
disp_image(handles);

set(handles.statustext,'String',['Use mouse button to add vertices. Pressing Backspace',...
                                 'or Delete removes the previously selected vertex. A ',...
                                 'shift-click, right-click, or double-click adds a final',...
                                 'vertex to the selection and then starts the fill;',...
                                 'pressing Return finishes the selection without adding a vertex.'],'ForegroundColor','black');
[handles.ROI,handles.ROIxi,handles.ROIyi] = roipoly;
hold on
plot(handles.axes1, handles.ROIxi,handles.ROIyi,'-g','LineWidth',1.5)
hold off
guidata(hObject,handles);

function removeBG_Callback(hObject, eventdata, handles)
handles = undo(handles, 'set');

handles.data.ImageArray2 = handles.data.ImageArray2.*(~handles.ROI);
handles.data.ImageMask = handles.data.ImageMask.*(~handles.ROI);
handles.data.errMeVpx2 = handles.data.errMeVpx2.*(~handles.ROI);

disp_image(handles);
guidata(hObject,handles);

function removeBGinv_Callback(hObject, eventdata, handles)
handles = undo(handles, 'set');

handles.data.ImageArray2 = handles.data.ImageArray2.*handles.ROI;
handles.data.ImageMask = handles.data.ImageMask.*handles.ROI;
handles.data.errMeVpx2 = handles.data.errMeVpx2.*handles.ROI;

disp_image(handles);
guidata(hObject,handles);

function dustremover_Callback(hObject, eventdata, handles)
handles = undo(handles, 'set');

data = getappdata(0,'data');
% save only "large" dust ... dont remember every little ;)
if sum(sum(handles.ROI)) > 5*data.data.fileinfo(1).XResolution
    % |(OR) if you remove more dust ;)
    handles.data.ImageDustMask = handles.data.ImageDustMask | handles.ROI;
end

handles.data.ImageArray2 = roifill(handles.data.ImageArray2, handles.ROI);
handles.data.errMeVpx2 = roifill(handles.data.errMeVpx2, handles.ROI);

disp_image(handles);
guidata(hObject,handles);

function loadbutton_Callback(hObject, eventdata, handles)
data = getappdata(0, 'data');

handles.currentRCFframe=get(handles.rcflistbox,'Value');

handles.data.ImageArray2 = data.data.ImageArray(:,:,handles.currentRCFframe);
handles.data.ImageMask = data.data.ImageMask(:,:,handles.currentRCFframe);
handles.data.ImageDustMask = data.data.ImageDustMask(:,:,handles.currentRCFframe);
handles.data.meanBG = data.data.meanBG(handles.currentRCFframe);
handles.data.errMeVpx2 = data.data.errMeVpx(:,:,handles.currentRCFframe);

handles = undo(handles, 'set');
handles = set_values(handles);

if data.data.edited(handles.currentRCFframe)
    set(handles.text16,'String','edited','ForegroundColor','red');
else
    set(handles.text16,'String','not edited','ForegroundColor','black');
end

disp_image(handles);
handles.ImageLoaded=1;
guidata(hObject,handles)

function savebutton_Callback(hObject, eventdata, handles)
data = getappdata(0, 'data');

data.data.ImageArray(:,:,handles.currentRCFframe) = handles.data.ImageArray2;
data.data.ImageMask(:,:,handles.currentRCFframe) = handles.data.ImageMask;
data.data.ImageDustMask(:,:,handles.currentRCFframe) = handles.data.ImageDustMask;
data.data.meanBG(handles.currentRCFframe) = handles.data.meanBG;
data.data.errMeVpx(:,:,handles.currentRCFframe) = handles.data.errMeVpx2;

handles = undo(handles, 'set');

data.data.edited(handles.currentRCFframe) = 1;
clear handles.maximum
setappdata(0, 'data', data);
set(handles.statustext,'String','Saved.','ForegroundColor','black');  
guidata(hObject,handles);

function contrast_slider_high_Callback(hObject, eventdata, handles)
handles.maximum = get(hObject,'Value');
%case min contrast is "higher" maximum
if handles.maximum <= handles.minimum
    if handles.maximum == 0
        handles.maximum = 1e-6;
        set(handles.contrast_slider_high,'Value',handles.maximum);
    end
    handles.minimum = handles.maximum*0.9999;
    set(handles.contrast_slider_low,'Value',handles.minimum);
    
end
set(handles.max_slider_pos_text,'String',['Slider val: ' num2str(handles.maximum,'%5.2f')]);
set(handles.min_slider_pos_text,'String',['Slider val: ',num2str(handles.minimum,'%5.2f')]);
set(handles.threshold_val, 'String', handles.maximum);

disp_image(handles);
guidata(hObject,handles);

function contrast_slider_low_Callback(hObject, eventdata, handles)
handles.minimum = get(hObject,'Value');
%case max contrast is "lower" minimum
if handles.minimum >= handles.maximum
    if handles.minimum == handles.maximum;
        handles.minimum = handles.maximum*0.9999;
        set(handles.contrast_slider_high,'Value',handles.minimum);
    end
    handles.maximum = handles.minimum/0.9999;
    set(handles.contrast_slider_high,'Value',handles.maximum);
end
set(handles.max_slider_pos_text,'String',['Slider val: ' num2str(handles.maximum,'%5.2f')]);
set(handles.min_slider_pos_text,'String',['Slider val: ',num2str(handles.minimum,'%5.2f')]);
set(handles.boundary_val, 'String', handles.minimum);

disp_image(handles);
guidata(hObject,handles);

function show_OD_button_Callback(hObject, eventdata, handles)
button=1;
while button == 1
	set(handles.statustext,'String','Left mouse click shows OD. Press any key to abort.','ForegroundColor','red');  
	[x,y,button]=ginput(1);
	x=round(x); y=round(y);
    if button == 1
        set(handles.OD_cursor_text,'String',['Value: ' num2str(handles.data.ImageArray2(y,x),'%5.2f')])
    end
end
set(handles.statustext,'String',' "Show OD" aborted.','ForegroundColor','black');  

function sel_bg_points_Callback(hObject, eventdata, handles)
%be careful; values from impixel depend on slider value
%therefore take only pixel position and grep values from matrix
[c,r,~] = impixel;
killidx = find(c<0 | r<0 | r>size(handles.data.ImageArray2,1) | c>size(handles.data.ImageArray2,2));
r(killidx) = []; c(killidx) = [];
clear killidx
idx = sub2ind(size(handles.data.ImageArray2), r,c);

%grep values from position of impixel
A = handles.data.ImageArray2(idx);
%B = handles.data.errMeVpx2(idx);

%calc mean and gauss-error of background selection
handles.data.meanBG = mean(A);
%handles.data.meanerrMeVpx = mean(B)/numel(B);
handles.data.meanerrMeVpx = std(A)/sqrt(numel(A));

set(handles.statustext,'String',['Mean BG val: ' num2str(handles.data.meanBG,'%2.3f') ...
    ', mean errMeVpx/N val: ' num2str(handles.data.meanerrMeVpx,'%2.3f')]);
guidata(hObject,handles);

function backgroundbutton_Callback(hObject, eventdata, handles)
handles = undo(handles, 'set');

if handles.data.meanBG > 0
    handles.data.ImageArray2 = handles.data.ImageArray2 - handles.data.meanBG;
    handles.data.ImageArray2(handles.data.ImageArray2 < 0) = 0;
    handles.data.errMeVpx2 = sqrt(handles.data.errMeVpx2.^2 + handles.data.meanerrMeVpx^2);
    handles.data.errMeVpx2(handles.data.errMeVpx2 < 0) = 0;
    
    handles = set_values(handles);
    
    set(handles.statustext,'String',['Background subtraction successful. BG val was ',num2str(handles.data.meanBG,'%2.3f')],'ForegroundColor','black');
    
    disp_image(handles);
else
    set(handles.statustext,'String','BG has to be at least 0!','ForegroundColor','red');
    set(handles.statustext,'ForegroundColor','black');
end
guidata(hObject,handles);

function undobutton_Callback(hObject, eventdata, handles)
handles = undo(handles, 'get');

handles = set_values(handles);

disp_image(handles);

set(handles.statustext,'String','Undo','ForegroundColor','black');
guidata(hObject,handles);

function boundary_val_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if (isnan(input))
    set(handles.statustext, 'String', 'Only Numeric input');
    set(hObject, 'String', 1);
else
    set(handles.statustext, 'String', '');
    disp_image(handles);
end

function boundary_val_KeyPressFcn(hObject, eventdata, handles)
thres = str2double(get(hObject,'String'));
steps = 100;
if strcmp(eventdata.Key,'uparrow')
    set(hObject, 'String', thres+steps);
elseif strcmp(eventdata.Key,'downarrow')
    set(hObject, 'String', thres-steps);
end
disp_image(handles);

function boundary_show_Callback(hObject, eventdata, handles)
disp_image(handles);

function boundary_cut_Callback(hObject, eventdata, handles)
handles = undo(handles, 'set');

[BW_bound, ~] = get_bound(handles);

% cut
handles.data.ImageArray2 = handles.data.ImageArray2 .* BW_bound;
handles.data.ImageMask = handles.data.ImageMask .* BW_bound;
handles.data.errMeVpx2 = handles.data.errMeVpx2 .* BW_bound;

disp_image(handles);
set(handles.statustext,'String','Cut','ForegroundColor','black');
guidata(hObject,handles);

function threshold_val_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if (isnan(input))
    set(handles.statustext, 'String', 'Only Numeric input');
    set(hObject, 'String', 1);
else
    set(handles.statustext, 'String', '');
    disp_image(handles);
end

function threshold_val_KeyPressFcn(hObject, eventdata, handles)
% --- Executes on key press with focus on threshold_val and none of its controls.
% increase by steps if up or down arrow keys are pressed while in field
thres = str2double(get(hObject,'String'));
steps = 100;
if strcmp(eventdata.Key,'uparrow')
    set(hObject, 'String', thres+steps);
elseif strcmp(eventdata.Key,'downarrow')
    set(hObject, 'String', thres-steps);
end
disp_image(handles);

function threshold_show_Callback(hObject, eventdata, handles)
disp_image(handles);

function threshold_marker_Callback(hObject, eventdata, handles)
disp_image(handles);

function threshold_expand_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if (isnan(input))
    set(handles.statustext, 'String', 'Only Numeric input');
    set(hObject, 'String', 1);
else
    set(handles.statustext, 'String', '');
    
    if input > 6
        set(hObject, 'String', 6);
        set(handles.statustext, 'String', 'Expand beyond 6 not supported :)');
    elseif input < 0
        set(hObject, 'String', 0);
        set(handles.statustext, 'String', 'negative Expand not supported :)');
    else
        set(hObject, 'String', input);
        set(handles.statustext, 'String', '');
    end
end

function threshold_remove_Callback(hObject, eventdata, handles)
handles = undo(handles, 'set');
expand = str2double(get(handles.threshold_expand, 'String'));
thres = str2double(get(handles.threshold_val, 'String'));
level = thres / max(max(handles.data.ImageArray2));

mask = im2bw(handles.data.ImageArray2/max(max(handles.data.ImageArray2)),level);
mask = imfill(mask,'holes');

mask_exp = padarray(mask,[expand expand], 0);
M = size(mask_exp,1);
[abw_c, abw_r] = find(mask);
ind = sub2ind(size(mask_exp), abw_c+expand, abw_r+expand);
neighbor_offsets = [M M+1 1 -M+1 -M -M-1 -1 M-1];
for i = 1:expand
    tmp2 = bsxfun(@plus, ind, neighbor_offsets);
    tmp2 = tmp2(:);
    ind = [ind; tmp2];
    ind = unique(ind);
    ind(ind<1 | ind>(numel(mask_exp))) = [];
end
mask_exp(ind) = true;

tmpIA2 = handles.data.ImageArray2;
tmpIA2 = padarray(tmpIA2,[expand expand], 0);
tmpIA2 = roifill(tmpIA2, mask_exp);
handles.data.ImageArray2 = tmpIA2(1+expand:end-expand, 1+expand:end-expand);

tmpEM2 = handles.data.errMeVpx2;
tmpEM2 = padarray(tmpEM2,[expand expand], 0);
tmpEM2 = roifill(tmpEM2, mask_exp);
handles.data.errMeVpx2 = tmpEM2(1+expand:end-expand, 1+expand:end-expand);

level = thres / max(max(handles.data.ImageArray2));
if level > 1
    set(handles.threshold_val, 'String', max(max(handles.data.ImageArray2))*0.99);
elseif level < 0
    set(handles.threshold_val, 'String', 1);
end

disp_image(handles);
set(handles.statustext,'String','Removed','ForegroundColor','black');
guidata(hObject,handles);

% helper function
function handles = undo(handles, command)
switch(command)
    case 'set'
        handles.data.Undo = handles.data.ImageArray2;
        handles.data.UndoMask = handles.data.ImageMask;
        handles.data.UndoDustMask = handles.data.ImageDustMask;
        handles.data.UndomeanBG = handles.data.meanBG;
        handles.data.UndoerrMeVpx = handles.data.errMeVpx2;
    case 'get'
        handles.data.ImageArray2 = handles.data.Undo;
        handles.data.ImageMask = handles.data.UndoMask;
        handles.data.ImageDustMask = handles.data.UndoDustMask;
        handles.data.meanBG = handles.data.UndomeanBG;
        handles.data.errMeVpx2 = handles.data.UndoerrMeVpx;
    case 'clear'
        handles.data.Undo = zeros(size(handles.data.Undo));
        handles.data.UndoMask = true(size(handles.data.UndoMask));
        handles.data.UndoDustMask = false(size(handles.data.UndoDustMask));
        handles.data.UndomeanBG = zeros(size(handles.data.UndomeanBG));
        handles.data.errMeVpx2 = zeros(size(handles.data.errMeVpx2));
    otherwise
end

function [BW_bound, boundary] = get_bound(handles)
%%% find largest Area and use it ;)
bound = str2double(get(handles.boundary_val, 'String'));
level_bound = bound / max(max(handles.data.ImageArray2));

BW_bound = im2bw(handles.data.ImageArray2/max(max(handles.data.ImageArray2)),level_bound);
BW_bound = imfill(BW_bound,'holes');

stats = regionprops(BW_bound, 'Area', 'PixelIdxList' );
area = [stats.Area];
[~, largestBlobIndex] = max(area);
blobIdx = stats(largestBlobIndex).PixelIdxList;
BW_bound(:) = false;
BW_bound(blobIdx) = true;

BW_bound = bwconvhull(BW_bound);

% find where BW is not 0, return linear index, translate first element
% to row and col with ind2sub ....
tmp3 = find(BW_bound);
[row, col] = ind2sub(size(BW_bound), tmp3(1));
clear tmp3;
% traces the beam edge 
boundary = bwtraceboundary(BW_bound,[row, col],'SW');

function disp_image(handles)
imagesc(handles.data.ImageArray2);

caxis([handles.minimum, handles.maximum]);
set(handles.axes1, 'Visible', 'off', 'Units', 'normalized');
colormap(handles.axes1,rcf);
hold on
if get(handles.threshold_show, 'Value');
    thres = str2double(get(handles.threshold_val, 'String'));
    max_inImg = max(max(handles.data.ImageArray2));
    level_thres = thres / max_inImg;
    if level_thres > 1
        level_thres = 0.99;
        set(handles.threshold_val, 'String', level_thres*max_inImg);
    elseif level_thres < 0
        level_thres = 0.01;
        set(handles.threshold_val, 'String', level_thres*max_inImg);
    end
    BW_thres = im2bw(handles.data.ImageArray2/max_inImg,level_thres);
    BW_thres = imfill(BW_thres,'holes');
    [bw_r, bw_c] = find(BW_thres);
    if get(handles.threshold_marker, 'Value');
        marker = 7;
    else
        marker = 3;
    end
    plot(bw_c, bw_r,'.m','MarkerSize',marker);
end
if get(handles.boundary_show, 'Value');
    bound = str2double(get(handles.boundary_val, 'String'));
    max_inImg = max(max(handles.data.ImageArray2));
    level_bound = bound / max(max(handles.data.ImageArray2));
    if level_bound > 1
        level_bound = 0.99;
        set(handles.boundary_val, 'String', level_bound*max_inImg);
    elseif level_bound < 0
        level_bound = 0.01;
        set(handles.boundary_val, 'String', level_bound*max_inImg);
    end

    [~, boundary] = get_bound(handles);

    plot(boundary(:,2),boundary(:,1),'g','LineWidth',2); % plot boundary
end
hold off

function handles = set_values(handles)
%for better contrast and contrast slider; if max=0, take very small value
handles.maximum = max([max(max(handles.data.ImageArray2)), 1e-16]);
handles.minimum = 0;
set(handles.contrast_slider_high,'Value',handles.maximum);
set(handles.contrast_slider_low,'Value',handles.minimum);
set(handles.min_OD_text,'String',['Minimum val: ',num2str(handles.minimum,'%5.2f')]);
set(handles.max_OD_text,'String',['Maximum val: ',num2str(handles.maximum,'%5.2f')]);
set(handles.min_slider_pos_text,'String',['Slider val: ',num2str(handles.minimum,'%5.2f')]);
set(handles.max_slider_pos_text,'String',['Slider val: ',num2str(handles.maximum,'%5.2f')]);
set(handles.threshold_val, 'String', handles.maximum);
set(handles.boundary_val, 'String', handles.minimum);


function avrg_filt_Callback(hObject, eventdata, handles)
% 5 circ average 2D filter for "smoothing" pixels ...
handles = undo(handles, 'set');

% cut
% handles.data.ImageArray2 = imfilter(handles.data.ImageArray2, fspecial('log'));
% handles.data.errMeVpx2 = imfilter(handles.data.errMeVpx2, fspecial('log'));
handles.data.ImageArray2 = medfilt2(handles.data.ImageArray2);
handles.data.errMeVpx2 = medfilt2(handles.data.errMeVpx2);


disp_image(handles);
set(handles.statustext,'String','Filtered','ForegroundColor','black');
guidata(hObject,handles);
