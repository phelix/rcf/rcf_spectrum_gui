function [ImageArray, fail, npixels, stderror] = counts2OD_coolscan_lamp0RGB(ImageArray, rcftype)

%NIKON CoolScan RGB+IR images

fail = 0;
npixels = 0; %percentage of pixels saturated

if ~(size(ImageArray,3) == 3) % back RGB
    fail = 3;
else
    % 65535 must be dark for calibration
    ImageArray = imcomplement(ImageArray);
    if ~isempty(ImageArray(ImageArray > 65535))
        npixels = sum(ImageArray(ImageArray > 65535))/sum(sum(ImageArray));
        fail = 1;
    end
    % Background correction! - blank film
    % cat(dim, R, G, B, IR)
    switch(upper(char(rcftype)))
        case 'H2'
            % new kalib with blank films
%                 clean = cat(3, 12323, 9199, 23843, 12073);
%                 stderror = [737 782 684 1209];
            % Olli kalib 
%                 clean = cat(3, 13530, 10125, 24487, 13365);
%                 stderror = [123 141 126 635];
            clean = cat(3, 13530, 10125, 24487);
            stderror = [123 141 126];
        case 'HD'
            % new kalib with blank films
%                 clean = cat(3, 10740, 5876, 4814, 9462);
%                 stderror = [696 816 815 1299];
            % Olli kalib 
%                 clean = cat(3, 9278, 5246, 4431, 7652);
%                 stderror = [413 185 92 1421];
            clean = cat(3, 9278, 5246, 4431);
            stderror = [413 185 92];
        case {'E3','ES'}
            % new kalib with blank films
%                 clean = cat(3, 21277, 19105, 36551, 18068);
%                 stderror = [974 1135 769 2018];
            % Olli kalib 
%                 clean = cat(3, 24088, 21280, 39557, 24008);
%                 stderror = [447 183 160 482];
            clean = cat(3, 24088, 21280, 39557);
            stderror = [447 183 160];
        case 'M2'
            % new kalib with blank films
%                 clean = cat(3, 15615, 11799, 11553, 16203);
%                 stderror = [722 857 919 1071];
            % Olli kalib 
%                 clean = cat(3, 15658, 11961, 11737, 15094);
%                 stderror = [375 206 139 1317];
            clean = cat(3, 15658, 11961, 11737);
            stderror = [375 206 139];
        case {'H1B','HD11171501'}
            % CAL-2019_1-HD#11171501_4
            clean = 65534-cat(3, 44561, 46526, 35451);
            stderror = [454 440 320]; 
        case {'H1C','HD01091801'}
            % CAL-2019_1-HD#01091801_7
            clean = 65534-cat(3, 47742, 51081, 46386); 
            stderror = [640 304 266];
        case {'H1A','HD12151402'}
            % CAL-2019_1-HD#12151402_3
            clean = 65534-cat(3, 44254, 48736, 34950); 
            stderror = [654 621 438];
        case {'E1E','EBT09121802'}
            % CAL-2019_1-EBT#09121802_8
            clean = 65534-cat(3, 40608, 42488, 32077); 
            stderror = [478 147 201];
        case {'E1D','EBT07291902'}
            % CAL-2019_1-EBT#07291902_6
            clean = 65534-cat(3, 43600, 44082, 33629); 
            stderror = [677 146 229];
        case {'E1A','EBT05191502'}
            % CAL-2019_1-EBT#05191502_1
            clean = 65534-cat(3, 35741, 39932, 24170); 
            stderror = [300 584 356];
        case {'E1B','EBT03161503'}
            % CAL-2019_1-EBT#03161503_2
            clean = 65534-cat(3, 36713, 40417, 24279); 
            stderror = [325 576 327];
        otherwise
            fail = 4;
            clean = cat(3, 0, 0, 0);
            stderror = [0 0 0];
    end
    if isa(ImageArray, 'uint16')
        bg = uint16(clean);
    else
        bg = clean;
    end
    ImageArray = bsxfun(@minus, ImageArray, bg);
end