function varargout = shotparam_fig(varargin)
% SHOTPARAM_FIG M-file for shotparam_fig.fig
%      SHOTPARAM_FIG, by itself, creates a new SHOTPARAM_FIG or raises the existing
%      singleton*.
%
%      H = SHOTPARAM_FIG returns the handle to a new SHOTPARAM_FIG or the handle to
%      the existing singleton*.
%
%      SHOTPARAM_FIG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SHOTPARAM_FIG.M with the given input arguments.
%
%      SHOTPARAM_FIG('Property','Value',...) creates a new SHOTPARAM_FIG or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before shotparam_fig_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to shotparam_fig_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright 2002-2003 The MathWorks, Inc.

% Edit the above text to modify the response to help shotparam_fig

% Last Modified by GUIDE v2.5 26-May-2020 13:55:33

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @shotparam_fig_OpeningFcn, ...
                   'gui_OutputFcn',  @shotparam_fig_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before shotparam_fig is made visible.
function shotparam_fig_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to shotparam_fig (see VARARGIN)

% Choose default command line output for shotparam_fig
handles.output = hObject;

% reuse data or init
data = getappdata(0, 'data');
if ~isfield(data, 'parameters') || isempty(data.parameters)
    % complete init ... easy
    data.parameters.rcffiles = [];
	data.parameters.elossfiles = [];
	c = textscan('HD;HD;M2;' ,'%s','Delimiter',';');
    data.parameters.rcfconfig = c{1}';
	data.parameters.laser.energy = 75;
	data.parameters.laser.pulseduration = 600;
	data.parameters.laser.focusdia = 20;
	data.parameters.laser.wavelength = 1.053;
	data.parameters.target.thickness = 10;
	data.parameters.target.distance = 40;
	data.parameters.scannerpop = 'CoolScan 9000ED, Lamp 0, RGB';
    data.parameters.rcfpath = [];
    data.parameters.elosspath = [];
    data.parameters.savepath = [];
else
    % partial init ... bad, but musthave for down-compability ... better ?!?
    if ~isfield(data.parameters, 'rcffiles'); data.parameters.rcffiles = []; end
    if ~isfield(data.parameters, 'elossfiles'); data.parameters.elossfiles = []; end
    if ~isfield(data.parameters, 'rcfconfig'); c = textscan('HD;HD;M2;' ,'%s','Delimiter',';'); data.parameters.rcfconfig = c{1}'; end
    if ~isfield(data.parameters, 'laser') || isempty(data.parameters.laser)
        data.parameters.laser.energy = 75;
        data.parameters.laser.pulseduration = 600;
        data.parameters.laser.focusdia = 20;
        data.parameters.laser.wavelength = 1.053;
    else
        if ~isfield(data.parameters.laser, 'energy'); data.parameters.laser.energy = 75; end
        if ~isfield(data.parameters.laser, 'pulseduration'); data.parameters.laser.pulseduration = 600; end
        if ~isfield(data.parameters.laser, 'focusdia'); data.parameters.laser.focusdia = 20; end
        if ~isfield(data.parameters.laser, 'wavelength'); data.parameters.laser.wavelength = 1053; end
    end
    if ~isfield(data.parameters, 'target') || isempty(data.parameters.target)
        data.parameters.target.thickness = 10;
        data.parameters.target.distance = 40;
    else
        if ~isfield(data.parameters.target, 'thickness'); data.parameters.target.thickness = 10; end
        if ~isfield(data.parameters.target, 'distance'); data.parameters.target.distance = 40; end
    end
    if ~isfield(data.parameters, 'scannerpop')
        if isfield(data.parameters, 'scanner')
            switch char(data.parameters.scanner)
                case 'microtek_lamp0RGB'
                    data.parameters.scannerpop = 'ArtixScan 1800F, Lamp 0, RGB';
                case 'coolscan_lamp0RGB'
                    data.parameters.scannerpop = 'CoolScan 9000ED, Lamp 0, RGB';
                case 'dresdenScan'
                    data.parameters.scannerpop = 'Dresden Scanner';
                otherwise
                    data.parameters.scannerpop = 'CoolScan 9000ED, Lamp 0, RGB';
            end
            try data.parameters = rmfield(data.parameters, 'scanner'); end
        else
            data.parameters.scannerpop = 'CoolScan 9000ED, Lamp 0, RGB';
        end
    end
    if ~isfield(data.parameters, 'rcfpath'); data.parameters.rcfpath = []; end
    if ~isfield(data.parameters, 'elosspath'); data.parameters.elosspath = []; end
    if ~isfield(data.parameters, 'savepath'); data.parameters.savepath = []; end
end
    
% and finaly set values to GUI
set(handles.rcflistbox,'String', data.parameters.rcffiles);
set(handles.elosslistbox,'String', data.parameters.elossfiles);
set(handles.rcf_configbox,'String', cell2mat(strcat(data.parameters.rcfconfig,';')));
set(handles.energyedit,'String', data.parameters.laser.energy);
set(handles.pulseduredit,'String', data.parameters.laser.pulseduration);
set(handles.focusedit,'String', data.parameters.laser.focusdia);
set(handles.wavelengthedit,'String', data.parameters.laser.wavelength);
set(handles.thicknessedit,'String', data.parameters.target.thickness);
set(handles.distanceedit,'String', data.parameters.target.distance);
contents = cellstr(get(handles.Scanner,'String'));
set(handles.Scanner,'Value',find(not(cellfun('isempty',strfind(contents,data.parameters.scannerpop)))));

% and set the data
setappdata(0, 'data', data);
guidata(hObject, handles);

function closebutton_Callback(hObject, eventdata, handles)
data = getappdata(0,'data');
if isempty(data.parameters.elossfiles) || isempty(data.parameters.rcffiles) || isempty(data.parameters.rcfconfig)
    set(handles.status_tag,'String','#RCF, #ELO, #RCFConfig should be at least 1!','ForegroundColor','red');
elseif (length(data.parameters.elossfiles) ~= length(data.parameters.rcffiles)) || ...
        (length(data.parameters.elossfiles) ~= length(data.parameters.rcfconfig)) || ...
        (length(data.parameters.rcffiles) ~= length(data.parameters.rcfconfig))
    set(handles.status_tag,'String','#RCF, #ELO, #RCFConfig missmatch!','ForegroundColor','red');
elseif any([isnan(str2double(get(handles.wavelengthedit,'String'))) ...
        isnan(str2double(get(handles.focusedit,'String'))) ...
        isnan(str2double(get(handles.pulseduredit,'String'))) ...
        isnan(str2double(get(handles.energyedit,'String'))) ...
        isnan(str2double(get(handles.thicknessedit,'String'))) ...
        isnan(str2double(get(handles.distanceedit,'String')))])
    set(handles.status_tag,'String', 'Check your numeric input', 'ForegroundColor','red');
else
    % gather all data and save it :)
    data.parameters.laser.wavelength = str2double(get(handles.wavelengthedit,'String'));
    data.parameters.laser.focusdia = str2double(get(handles.focusedit,'String'));
    data.parameters.laser.pulseduration = str2double(get(handles.pulseduredit,'String'));
    data.parameters.laser.energy = str2double(get(handles.energyedit,'String'));
    data.parameters.target.thickness = str2double(get(handles.thicknessedit,'String'));
    data.parameters.target.distance = str2double(get(handles.distanceedit,'String'));
    contents = cellstr(get(handles.Scanner,'String'));
    data.parameters.scannerpop = contents{get(handles.Scanner,'Value')};
    if data.process == 0
        data.process = 10;
    end
    setappdata(0,'data',data);
    close shotparam_fig
end

% --- Outputs from this function are returned to the command line.
function varargout = shotparam_fig_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
varargout{1} = handles.output;

function openscans_Callback(hObject, eventdata, handles)
data = getappdata(0, 'data');

[rcffiles, data.parameters.rcfpath] = uigetfile(...
    {'*.tif','TIF-files (*.tif)'; '*.jpg','JPG-files (*.jpg)'; ...
    '*.*',  'All Files (*.*)'}, 'Pick a file', ...
    'MultiSelect', 'on', data.parameters.rcfpath);

if ~isequal(rcffiles, 0)
    if iscell(rcffiles)
        data.parameters.rcffiles = sort(rcffiles);
    else
        data.parameters.rcffiles = cell(1);
        data.parameters.rcffiles{1} = rcffiles;
    end
    
    setappdata(0, 'data', data);
    set(handles.rcflistbox, 'String', data.parameters.rcffiles);
    guidata(hObject, handles);
end

function openEloss_Callback(hObject, eventdata, handles)
data = getappdata(0, 'data');

[elossfiles, data.parameters.elosspath] = uigetfile( ...
    {'*.dat','DAT-files (*.dat)'; '*.txt','TXT-files (*.txt)'; ...
    '*.*',  'All Files (*.*)'}, 'Pick a file', ...
    'MultiSelect', 'on', data.parameters.elosspath);

if ~isequal(elossfiles, 0)
    if iscell(elossfiles)
        data.parameters.elossfiles = sort(elossfiles);
    else
        data.parameters.elossfiles = cell(1);
        data.parameters.elossfiles{1} = elossfiles;
    end
    setappdata(0, 'data', data);
    set(handles.elosslistbox, 'String', data.parameters.elossfiles);
    guidata(hObject, handles);
end

function rcf_configbox_Callback(hObject, eventdata, handles)
% CB 20.12.2019
% removed the check for configbox number of letter.
% new RCF calibs will have a longer identifier

% if rem(length(get(handles.rcf_configbox,'String')),3) == 0
    data = getappdata(0, 'data');
    c = textscan(get(handles.rcf_configbox,'String') ,'%s','Delimiter',';');
    data.parameters.rcfconfig = c{1}';
    
    data.parameters.rcfconfig_numb = cell(1,length(data.parameters.rcfconfig));
    for i = 1:length(data.parameters.rcfconfig)
        data.parameters.rcfconfig_numb{i} = [num2str(i,'%02i'), ' ', data.parameters.rcfconfig{i}];
    end

    setappdata(0, 'data', data);
% end
guidata(hObject, handles);

% --- Executes on key press with focus on rcflistbox and none of its controls.
function rcflistbox_KeyPressFcn(hObject, eventdata, handles)
% deletes with del key elements
data = getappdata(0, 'data');
if strcmp(eventdata.Key,'delete')
    SVV = data.parameters.rcffiles;
    SV = get(hObject,{'string','value'});
    if ~cellfun('isempty',SV{1})
        SV{1}(SV{2}) = [];
        SVV(SV{2}) = [];
        set(hObject,'string',SV{1},'value',1)
    end
    data.parameters.rcffiles = SVV;
    setappdata(0,'data',data);
    guidata(hObject,handles);
end

% --- Executes on key press with focus on elosslistbox and none of its controls.
function elosslistbox_KeyPressFcn(hObject, eventdata, handles)
% deletes with del key elements
data = getappdata(0, 'data');
if strcmp(eventdata.Key,'delete')
    SVV = data.parameters.elossfiles;
    SV = get(hObject,{'string','value'});
    if ~cellfun('isempty',SV{1})
        SV{1}(SV{2}) = [];
        SVV(SV{2}) = [];
        set(hObject,'string',SV{1},'value',1)
    end
    data.parameters.elossfiles = SVV;
    setappdata(0,'data',data);
    guidata(hObject,handles);
end

% --- Executes during object creation, after setting all properties.
function Scanner_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Scanner (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
data = getappdata(0,'data');
set(hObject,'Value',1)
setappdata(0,'data',data);
