function [tmpImageArray, tmpErrMeVpx] = OD2MeV_microtek_lamp0RGB(data, handles, frame)

tmpImageArray = data.data.ImageArray(:,:,:,frame);
stdabwBG = data.data.stdabwBG(frame,:);
rcf_config = upper(char(data.parameters.rcfconfig(frame)));
res = data.data.fileinfo.XResolution;
mop = false;

%we need this, cause calibration is done from 0 to 1 color-values
%plus downward compatiblity
if isa(tmpImageArray,'uint16')
    tmpImageArray=single(tmpImageArray)/65535;
    stdabwBG=stdabwBG/65535;
end

% Just to be sure that there are no negative values and values > 1
tmpImageArray(tmpImageArray < 0) = 0;
tmpImageArray(tmpImageArray > 1) = 1.0;

switch rcf_config
    case 'HD'
        %scale from dose to keV/mm^2
        %HD-810 => 6.5µm
        scale = 4.381539772278532e7;

        %RGB calibration 2012; LOT-Nummer S26A41H810
        %fitting parameters sorted for each channel [R G B]
        a =[0; 0; 4556687677507/100000000000];
        b = [939648965743/1000000000000; 212176330783/200000000000; 53399806923/50000000000];
        c = [759459383474267/500000000000; 8959449552099823/1000000000000; 6272989256492953/62500000000];
        d = [5627251154081/125000000000; 64991094346101/500000000000; 480701737394151/1000000000000];
        %errors from fitting [R G B]
        deltaa = [0; 0; 35337443257681/1000000000000];
        deltab = [9616902037/1000000000000; 22918832721/1000000000000; 31436849353/1000000000000];
        deltac = [82984526736137/500000000000; 82100401206493/50000000000; 8818733012506891/250000000000];
        deltad = [3747865309203/1000000000000; 4385390857487/250000000000; 47984456578701/1000000000000];

        %estimated asymtotic covariance from fit 
        %cov = {cov_ab; cov_ac; cov_ad; cov_bc; cov_bd; cov_cd}
        %needs to be cell-array
        covR = {0; 0; 0; -0.6657630634238125; 0.028843203823644192; -468.0534822335143};
        covG = {0; 0; 0; -18.102943963602673; 0.3383694619837436; -22787.394101744765};
        covB = {-0.6865079911401293; 1.22012684218108e6; -638.8399413570173; -776.1846935472086; 1.3799578090212492; -871586.6491954351};

        %limes for derivatives[0=dose]
        limesDfit=[0; 0; 0.0004539988324536206];

        %satturation color values defined by calibration [R G B]
        %dose satt is 2201Gy * 0.95=2092Gy, so 95% of max usable dose
        %from calib => about 2 times higher then spec of HD;
        %error for max dose approx 14% (real) and 13% (including fit errors)!
        %mean real error dose [0,dosemax]=6.6%
        satt=[0.9195396178886487; 0.9968927659005754; 0.8699114960139444];
        start=[1e-6; 1e-6]; % for 2013 calib
        stop=[inf,inf,inf];
    case 'H2'
        %scale from dose to keV/mm^2
        %HDv2 => 8µm
        scale = 5.392664335112039e7;

        %RGB calibration 2012; LOT-Nummer A01141102
        %fitting parameters sorted for each channel [R G B]
        a = [0; 0; 0];
        b = [167272544787/200000000000; 37294399887/40000000000; 20887451259/40000000000];
        c = [0; 39566651184559/40000000000; 0];
        d = [25195630652597/250000000000; 146251967010519/500000000000; 377273531569493/500000000000];
        %errors from fitting [R G B]
        deltaa = [0; 0; 0];
        deltab = [8969037323/1000000000000; 39982823/6250000000; 2125352723/250000000000];
        deltac = [0; 91038081041077/200000000000; 0];
        deltad = [2062250418437/500000000000; 2094023759091/250000000000; 34026388357151/1000000000000];

        %estimated asymtotic covariance from fit 
        %cov = {cov_ab; cov_ac; cov_ad; cov_bc; cov_bd; cov_cd}
        %needs to be cell-array
        covR = {0; 0; 0; 0; 0.024660998032886298; 0};
        covG = {0; 0; 0; -1.088821231446897; 0.042824702716187574; -2357.324562290071};
        covB = {0; 0; 0; 0; 0.2544189282895268; 0};

        %limes for derivatives[0=dose]
        limesDfit=[0.00829868812838166; 0; 0.000692052632612347];

        %satturation color values defined by calibration [R G B]
        %dose satt is 4224Gy * 0.95=4013Gy, so 95% of max usable dose
        %from calib => about 4 times higher then spec of H2;
        %error for max dose approx 18% (real) and 12% (including fit errors)!
        %mean real error dose [0,dosemax]=6.6%
        satt=[0.8158705566740958; 0.8689614245225881; 0.4395331398795763];
        start=[1e-6; 1e-6]; % for 2013 calib
        stop=[inf,inf,inf];
    case 'M2'
        %scale from dose to keV/mm^2
        %M2 => 35µm
        scale = 2.359290646611517e8;

        %RGB calibration 2012; LOT-Nummer S0628MDV2 ("M2new")
        %also verified for M2old (LOT-Nummer R0551MDV2, before 2012)
        %=> Deviations for complete dose space M2new/M2old ~ 3.7 %
        %typicall uncertainty of M2-films ~6.5% (whole dose space)
        %=> M2new and M2old are identical films!
        %fitting parameters sorted for each channel [R G B]
        a = [23752037986013/1000000000000; 42512462392083/200000000000; 0];
        b = [739628623659/1000000000000; 137352051451/200000000000; 1133849531741/1000000000000];
        c = [256985954494073/500000000000; 5779433357905509/500000000000; 0];
        d = [135712768879/5000000000; 7308571278331/40000000000; 95053615377571/500000000000];
        %errors from fitting [R G B]
        deltaa = [5546748709211/1000000000000; 55896997708437/500000000000; 0];
        deltab = [11918912489/1000000000000; 111559252417/1000000000000; 9983210867/500000000000];
        deltac = [27262488025563/250000000000; 2956443334959017/500000000000; 0];
        deltad = [4973455559237/1000000000000; 82844301722741/1000000000000; 2658537833649/500000000000];

        %estimated asymtotic covariance from fit 
        %cov = {cov_ab; cov_ac; cov_ad; cov_bc; cov_bd; cov_cd}
        %needs to be cell-array
        covR = {-0.05369683553916271; 603.2950146161161; 27.299730004608712; -1.0796221737432825; -0.043414076808257866; 531.1375664302012};
        covG = {-12.23193539631371; 660816.9636269745; 9243.700787091528; -648.8082222240454; -8.944508838779896; 488126.9256501742};
        covB = {0; 0; 0; 0; 0.10275934643324613; 0};

        %limes for derivatives[0=dose]
        limesDfit=[0.04621271624119131; 0.018389546067665744; 0.00596426304900106];

        %satturation color values defined by calibration [R G B]
        %dose satt is 222Gy * 0.95=211Gy, so 95% of max usable dose
        %from calib => about 2 times higher then spec of M2;
        %error for max dose approx 2% (real) and 4% (including fit errors)!
        %mean real error dose [0,dosemax]=2.8%
        satt=[0.7474211026158994; 0.7969694283932233; 0.5960204879932623];
        start=[1e-6; 1e-6]; % for 2013 calib
        stop=[inf,inf,inf];
    case 'E3'
        %scale from dose to keV/mm^2
        %E3 => 27µm
        %scaleE3 = 1.820024213100313e8; % with wrong density 1.08
        %correction for wrong IC calib-factor in calibration
        %=> old=0.891; new=1.970;
        scale = 1.2/1.08*1.820024213100313e8*0.891/1.970; % with denisty 1.2

        %RGB calibration 2012; LOT-Nummer A12151101
        %fitting parameters sorted for each channel [R G B]
        a = [150653651851/250000000000; 2512739042741/500000000000; 0];
        b = [263876522401/500000000000; 610904219953/1000000000000; 114496881131/500000000000];
        c = [97833484761/15625000000; 2664341957039/25000000000; 0];
        d = [1320292550263/200000000000; 551125253693/31250000000; 5081689005061/200000000000];
        %errors from fitting [R G B]
        deltaa =[158134042683823/1000000000000; 7468707924037/1000000000000; 0];
        deltab = [7191352271/500000000000; 5234405351/250000000000; 58308559/7812500000];
        deltac = [1639979283372997/1000000000000; 18949229564343/125000000000; 0];
        deltad = [11956840748659/40000000000; 2014232652831/200000000000; 762776222463/500000000000];

        %estimated asymtotic covariance from fit 
        %cov = {cov_ab; cov_ac; cov_ad; cov_bc; cov_bd; cov_cd}
        %needs to be cell-array
        covR = {-1.5158966922233688; 259336.4958928451; 47269.52450148514; -15.728216770788764; -2.8607449863063317; 490223.0606211633};
        covG = {-0.13278984546737638; 1131.9533429487435; 75.00733206730816; -2.7176581917399334; -0.1706883897766888; 1520.143476397272};
        covB = {0; 0; 0; 0; 0.010909761814999277; 0};

        %limes for derivatives[0=dose]
        limesDfit=[0.09624366609955462; 0.047154965151951146; 0.00901250596146846];

        %satturation color values defined by calibration [R G B]
        %dose satt is 55Gy * 0.95=52Gy, so 95% of max usable dose
        %from calib => about 6.5 times higher then spec of E3;
        %note: calibration focused on "higher" doses
        %error for max dose approx 8.5% (real) and 8% (including fit errors)!
        %mean real error dose [0,dosemax]=5%
        satt=[0.47785903894420345; 0.5137303091266063; 0.15411379065068262];
        start=[1e-6; 1e-6]; % for 2013 calib
        stop=[inf,inf,inf];
    otherwise
        mop = true;
        disp('Unknown layer.')
end

if ~mop
    [tmpImageArray, tmpErrMeVpx] = RGBcalib2013_3(tmpImageArray,data.data.ImageMask(:,:,frame),satt,start,stop,stdabwBG,a,b,c,d,deltaa,deltab,deltac,deltad,covR,covG,covB,limesDfit,handles,frame);
    tmpImageArray = tmpImageArray*scale;
    tmpErrMeVpx = tmpErrMeVpx*scale;

    %just for debugging
    %setappdata(0, strcat('Image', num2str(frame)), tmpImageArray);
    %setappdata(0, strcat('err', num2str(frame)), tmpErrMeVpx);

    %convert from dose in MeV/px
    tmpImageArray = 25.4^2/res^2*1e-3 .*tmpImageArray;
    tmpErrMeVpx = 25.4^2/res^2*1e-3 .*tmpErrMeVpx;
else
    tmpErrMeVpx = zeros(size(tmpImageArray,1),size(tmpImageArray,2));
end

clear stdabwBG data rcfconfig scanner fileinfo
end