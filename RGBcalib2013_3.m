function [ImageArray, errdose] = RGBcalib2013_3(ImageArray,ImageMask,satt,start,stop,stdabwBG,a,b,c,d,deltaa,deltab,deltac,deltad,covR,covG,covB,limesDfit,handles,frame)
% CB 21.10.2013
% Killed IR part of calib and merged 2012 & 2013 calib

% CB 07.10.2013
% assign here, costs some memory, but increase speed by 50%.
Ic1 = ImageArray(:,:,1);
Ic2 = ImageArray(:,:,2);
Ic3 = ImageArray(:,:,3);
Is = size(ImageArray);

%check for saturations
px=[sum(sum(Ic1 > satt(1))), sum(sum(Ic2 > satt(2))), sum(sum(Ic3 > satt(3)))];
%average saturation over all channels
npixels=sum(px)/(3*Is(1)*Is(2));
npixelR=px(1)/(Is(1)*Is(2));
npixelG=px(2)/(Is(1)*Is(2));
npixelB=px(3)/(Is(1)*Is(2));

%color coding for saturation warning; less than 5% => green, between
%5 and 15% => orange, over 15% => red
if npixels > 0.01
    if npixels < 0.05
        colorc = 'blue';
    elseif npixels <= 0.15 
        colorc = [1 .5 0]; %orange
    elseif npixels > 0.15
        colorc = 'red';
    end
    set(handles.statustext,'String', ['(',num2str(npixelR*100,'%3.1f'),'; ',num2str(npixelG*100,'%3.1f'),'; ',num2str(npixelB*100,'%3.1f'), ') % of RGB px from layer ',num2str(frame),' are saturated! Result may be wrong, maybe delete it?!'], 'ForegroundColor', colorc);
    pause(0.5)
end

%force values to maximum color value for max. dose usable in calibration
Ic1(Ic1 > satt(1)) = satt(1);
Ic2(Ic2 > satt(2)) = satt(2);
Ic3(Ic3 > satt(3)) = satt(3);

%errors in color-channel are defined by stdabw of backgroud for each pixel

%pre-calculate dose error from color-values; partial derivatives
partFWR=(-a(1)+d(1).*Ic1+sqrt(4*c(1).*(b(1)-Ic1).*Ic1+(a(1)-d(1).*Ic1).^2)+(b(1)-Ic1).*(d(1)+(2*b(1)*c(1)-a(1)*d(1)+(-4*c(1)+d(1)^2).*Ic1)./sqrt(4*c(1)*(b(1)-Ic1).*Ic1+(a(1)-d(1)*Ic1).^2)))./(2*(b(1)-Ic1).^2);
partFWG=(-a(2)+d(2).*Ic2+sqrt(4*c(2).*(b(2)-Ic2).*Ic2+(a(2)-d(2).*Ic2).^2)+(b(2)-Ic2).*(d(2)+(2*b(2)*c(2)-a(2)*d(2)+(-4*c(2)+d(2)^2).*Ic2)./sqrt(4*c(2)*(b(2)-Ic2).*Ic2+(a(2)-d(2)*Ic2).^2)))./(2*(b(2)-Ic2).^2);
partFWB=(-a(3)+d(3).*Ic3+sqrt(4*c(3).*(b(3)-Ic3).*Ic3+(a(3)-d(3).*Ic3).^2)+(b(3)-Ic3).*(d(3)+(2*b(3)*c(3)-a(3)*d(3)+(-4*c(3)+d(3)^2).*Ic3)./sqrt(4*c(3)*(b(3)-Ic3).*Ic3+(a(3)-d(3)*Ic3).^2)))./(2*(b(3)-Ic3).^2);

partaR=(-1+(a(1)-d(1)*Ic1)./sqrt(4*c(1)*(b(1)-Ic1).*Ic1+(a(1)-d(1)*Ic1).^2))./(2*(b(1)-Ic1));
partaG=(-1+(a(2)-d(2)*Ic2)./sqrt(4*c(2)*(b(2)-Ic2).*Ic2+(a(2)-d(2)*Ic2).^2))./(2*(b(2)-Ic2));
partaB=(-1+(a(3)-d(3)*Ic3)./sqrt(4*c(3)*(b(3)-Ic3).*Ic3+(a(3)-d(3)*Ic3).^2))./(2*(b(3)-Ic3));
                    
partbR=(a(1)-d(1)*Ic1+(2*c(1)*(b(1)-Ic1).*Ic1)./sqrt(4*c(1)*(b(1)-Ic1).*Ic1+(a(1)-d(1)*Ic1).^2)-sqrt(4*c(1)*(b(1)-Ic1).*Ic1+(a(1)-d(1)*Ic1).^2))./(2*(b(1)-Ic1).^2);
partbG=(a(2)-d(2)*Ic2+(2*c(2)*(b(2)-Ic2).*Ic2)./sqrt(4*c(2)*(b(2)-Ic2).*Ic2+(a(2)-d(2)*Ic2).^2)-sqrt(4*c(2)*(b(2)-Ic2).*Ic2+(a(2)-d(2)*Ic2).^2))./(2*(b(2)-Ic2).^2);
partbB=(a(3)-d(3)*Ic3+(2*c(3)*(b(3)-Ic3).*Ic3)./sqrt(4*c(3)*(b(3)-Ic3).*Ic3+(a(3)-d(3)*Ic3).^2)-sqrt(4*c(3)*(b(3)-Ic3).*Ic3+(a(3)-d(3)*Ic3).^2))./(2*(b(3)-Ic3).^2);

partcR=Ic1./sqrt(4*c(1)*(b(1)-Ic1).*Ic1+(a(1)-d(1)*Ic1).^2);
partcG=Ic2./sqrt(4*c(2)*(b(2)-Ic2).*Ic2+(a(2)-d(2)*Ic2).^2);
partcB=Ic3./sqrt(4*c(3)*(b(3)-Ic3).*Ic3+(a(3)-d(3)*Ic3).^2);

partdR=(Ic1+(Ic1.*(-a(1)+d(1)*Ic1))./sqrt(4*c(1)*(b(1)-Ic1).*Ic1+(a(1)-d(1)*Ic1).^2))./(2*(b(1)-Ic1));
partdG=(Ic2+(Ic2.*(-a(2)+d(2)*Ic2))./sqrt(4*c(2)*(b(2)-Ic2).*Ic2+(a(2)-d(2)*Ic2).^2))./(2*(b(2)-Ic2));
partdB=(Ic3+(Ic3.*(-a(3)+d(3)*Ic3))./sqrt(4*c(3)*(b(3)-Ic3).*Ic3+(a(3)-d(3)*Ic3).^2))./(2*(b(3)-Ic3));

%remove inf and NaN from partial derivatives above:
%no limes needed, cause we define 
%if FW=0 => dose=0 =>deltaDose=0
partFWR(isnan(partFWR) | isinf(partFWR))=0;
partFWG(isnan(partFWG) | isinf(partFWG))=0;
partFWB(isnan(partFWB) | isinf(partFWB))=0;
partaR(isnan(partaR) | isinf(partaR))=0;
partaG(isnan(partaG) | isinf(partaG))=0;
partaB(isnan(partaB) | isinf(partaB))=0;
partbR(isnan(partbR) | isinf(partbR))=0;
partbG(isnan(partbG) | isinf(partbG))=0;
partbB(isnan(partbB) | isinf(partbB))=0;
partcR(isnan(partcR) | isinf(partcR))=0;
partcG(isnan(partcG) | isinf(partcG))=0;
partcB(isnan(partcB) | isinf(partcB))=0;
partdR(isnan(partdR) | isinf(partdR))=0;
partdG(isnan(partdG) | isinf(partdG))=0;
partdB(isnan(partdB) | isinf(partdB))=0;

%just a temporay array
errdoseRtmp=cellfun(@(x,y) x.*y, {partaR.*partbR; partaR.*partcR; partaR.*partdR; partbR.*partcR; partbR.*partdR; partcR.*partdR}, covR, 'UniformOutput', false);
errdoseGtmp=cellfun(@(x,y) x.*y, {partaG.*partbG; partaG.*partcG; partaG.*partdG; partbG.*partcG; partbG.*partdG; partcG.*partdG}, covG, 'UniformOutput', false);
errdoseBtmp=cellfun(@(x,y) x.*y, {partaB.*partbB; partaB.*partcB; partaB.*partdB; partbB.*partcB; partbB.*partdB; partcB.*partdB}, covB, 'UniformOutput', false);

errdoseR=sqrt((partFWR.*stdabwBG(1)).^2+(partaR.*deltaa(1)).^2+(partbR.*deltab(1)).^2+(partcR.*deltac(1)).^2+(partdR.*deltad(1)).^2+2*sum(cat(3,errdoseRtmp{:}),3));
errdoseG=sqrt((partFWG.*stdabwBG(2)).^2+(partaG.*deltaa(2)).^2+(partbG.*deltab(2)).^2+(partcG.*deltac(2)).^2+(partdG.*deltad(2)).^2+2*sum(cat(3,errdoseGtmp{:}),3));
errdoseB=sqrt((partFWB.*stdabwBG(3)).^2+(partaB.*deltaa(3)).^2+(partbB.*deltab(3)).^2+(partcB.*deltac(3)).^2+(partdB.*deltad(3)).^2+2*sum(cat(3,errdoseBtmp{:}),3));

%find color-value-index for color-value == 0
indxR=find(Ic1==0);
indxG=find(Ic2==0);
indxB=find(Ic3==0);

%convert from color-value to dose
Ic1=(-a(1)+d(1)*Ic1+sqrt(4*c(1)*(b(1)-Ic1).*Ic1+(a(1)-d(1)*Ic1).^2))./(2*(b(1)-Ic1));
Ic2=(-a(2)+d(2)*Ic2+sqrt(4*c(2)*(b(2)-Ic2).*Ic2+(a(2)-d(2)*Ic2).^2))./(2*(b(2)-Ic2));
Ic3=(-a(3)+d(3)*Ic3+sqrt(4*c(3)*(b(3)-Ic3).*Ic3+(a(3)-d(3)*Ic3).^2))./(2*(b(3)-Ic3));

%where color-value is zero, dose has to be zero!
Ic1(indxR)=0;
Ic2(indxG)=0;
Ic3(indxB)=0;

clear indxR indxG indxB

%set NaN in dose matrix to zero; needs to be done
%this way cause sometimes FW=0 gives dose=NaN due to 1/0 in calibration 
Ic1(isnan(Ic1)) = 0;
Ic2(isnan(Ic2)) = 0;
Ic3(isnan(Ic3)) = 0;

%calculate weighting matrix for each color channel
%weighting will be done related to steepness of FW vs dose curve; Dfit is
%derivative of fitted model and needs a dose value; needs to be done this
%way, cause of different doses
DfitRdose1=(b(1)*Ic1.*(2*c(1)+d(1)*Ic1)+a(1)*(c(1)-Ic1.^2))./(c(1)+Ic1.*(d(1)+Ic1)).^2;
DfitRdose2=(b(1)*Ic2.*(2*c(1)+d(1)*Ic2)+a(1)*(c(1)-Ic2.^2))./(c(1)+Ic2.*(d(1)+Ic2)).^2;
DfitRdose3=(b(1)*Ic3.*(2*c(1)+d(1)*Ic3)+a(1)*(c(1)-Ic3.^2))./(c(1)+Ic3.*(d(1)+Ic3)).^2;

DfitGdose1=(b(2)*Ic1.*(2*c(2)+d(2)*Ic1)+a(2)*(c(2)-Ic1.^2))./(c(2)+Ic1.*(d(2)+Ic1)).^2;
DfitGdose2=(b(2)*Ic2.*(2*c(2)+d(2)*Ic2)+a(2)*(c(2)-Ic2.^2))./(c(2)+Ic2.*(d(2)+Ic2)).^2;
DfitGdose3=(b(2)*Ic3.*(2*c(2)+d(2)*Ic3)+a(2)*(c(2)-Ic3.^2))./(c(2)+Ic3.*(d(2)+Ic3)).^2;

DfitBdose1=(b(3)*Ic1.*(2*c(3)+d(3)*Ic1)+a(3)*(c(3)-Ic1.^2))./(c(3)+Ic1.*(d(3)+Ic1)).^2;
DfitBdose2=(b(3)*Ic2.*(2*c(3)+d(3)*Ic2)+a(3)*(c(3)-Ic2.^2))./(c(3)+Ic2.*(d(3)+Ic2)).^2;
DfitBdose3=(b(3)*Ic3.*(2*c(3)+d(3)*Ic3)+a(3)*(c(3)-Ic3.^2))./(c(3)+Ic3.*(d(3)+Ic3)).^2;

%if Dfits are NaN (sometimes if dose is zero; then replace NaN with
%limes Dfit->0
DfitRdose1(isnan(DfitRdose1))=limesDfit(1);
DfitRdose2(isnan(DfitRdose2))=limesDfit(1);
DfitRdose3(isnan(DfitRdose3))=limesDfit(1);

DfitGdose1(isnan(DfitGdose1))=limesDfit(2);
DfitGdose2(isnan(DfitGdose2))=limesDfit(2);
DfitGdose3(isnan(DfitGdose3))=limesDfit(2);

DfitBdose1(isnan(DfitBdose1))=limesDfit(3);
DfitBdose2(isnan(DfitBdose2))=limesDfit(3);
DfitBdose3(isnan(DfitBdose3))=limesDfit(3);

%now look for starting range of values
%
%concerning calculated dose, decide if channel is taken into account
%now look into green, decide if fullfills condition
%start(1)=> green starts; start(2)=> blue starts; start(3)=> IR starts
%if red-dose smaller start(1), don't calculate other channels
ImageArrayDose=Ic1;

DfitRdose1(ImageArrayDose>stop(1))=0;
DfitRdose2(ImageArrayDose>stop(1))=0;
DfitRdose3(ImageArrayDose>stop(1))=0;

DfitGdose1(ImageArrayDose<start(1))=0;
DfitGdose2(ImageArrayDose<start(1))=0;
DfitGdose3(ImageArrayDose<start(1))=0;

%calculate normalized weightings wR wG wB wIR
wsteigRdose1=DfitRdose1./(DfitRdose1+DfitGdose1);
wsteigGdose2=DfitGdose2./(DfitRdose2+DfitGdose2);
%normalized
wR=(wsteigRdose1)./(wsteigRdose1+wsteigGdose2);
wG=(wsteigGdose2)./(wsteigRdose1+wsteigGdose2);
%calculate preliminary dose
ImageArrayDose=wR.*Ic1+wG.*Ic2;

%now look into blue, decide if fullfills condition
DfitBdose1(ImageArrayDose<start(2))=0;
DfitBdose2(ImageArrayDose<start(2))=0;
DfitBdose3(ImageArrayDose<start(2))=0;
%calculate normalized weightings wR wG wB wIR
wsteigRdose1=DfitRdose1./(DfitRdose1+DfitGdose1+DfitBdose1);
wsteigGdose2=DfitGdose2./(DfitRdose2+DfitGdose2+DfitBdose2);
wsteigBdose3=DfitBdose3./(DfitRdose3+DfitGdose3+DfitBdose3);

%normalized
wR=(wsteigRdose1)./(wsteigRdose1+wsteigGdose2+wsteigBdose3);
wG=(wsteigGdose2)./(wsteigRdose1+wsteigGdose2+wsteigBdose3);
wB=(wsteigBdose3)./(wsteigRdose1+wsteigGdose2+wsteigBdose3);

%calculate end dose
ImageArray=wR.*Ic1+wG.*Ic2+wB.*Ic3;

%weighted error dose
errdose=wR.*errdoseR+wG.*errdoseG+wB.*errdoseB;

%set values outside mask to zero; both error and dose; therefore for dose=0 inside
%mask an error-value exists
ImageArray(~ImageMask)=0;
errdose(~ImageMask)=0;

% CB 13.09.2013
% Why are still NaN in ImageArray and errdose ?! kill it here ...
ImageArray(isnan(ImageArray)) = 0;
errdose(isnan(errdose)) = 0;

%can be done better? maybe less reasignment above?!
clear ImageArrayDose partFWR partFWG partFWB partFWIR partaR partaG partaB partaIR partbR partbG partbB partbIR partcR partcG partcB partcIR partdR partdG partdB partdIR... 
errdoseR errdoseG errdoseB errdoseIR DfitRdose1 DfitRdose2 DfitRdose3 DfitRdose4 DfitGdose1 DfitGdose2 DfitGdose3 DfitGdose4 DfitBdose1 DfitBdose2 DfitBdose3 DfitBdose4...
DfitIRdose1 DfitIRdose2 DfitIRdose3 DfitIRdose4 wsteigRdose1 wsteigGdose2 wsteigBdose3 wsteigIRdose4 wR wG wB wIR errdoseRtmp errdoseGtmp errdoseBtmp errdoseIRtmp...
npixels px npixelR npixelG npixelB npixelIR;

end