function varargout = choosefit(varargin)
% CHOOSEFIT M-file for choosefit.fig
%      CHOOSEFIT, by itself, creates a new CHOOSEFIT or raises the existing
%      singleton*.
%
%      H = CHOOSEFIT returns the handle to a new CHOOSEFIT or the handle to
%      the existing singleton*.
%
%      CHOOSEFIT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CHOOSEFIT.M with the given input arguments.
%
%      CHOOSEFIT('Property','Value',...) creates a new CHOOSEFIT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before choosefit_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to choosefit_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright 2002-2003 The MathWorks, Inc.

% Edit the above text to modify the response to help choosefit

% Last Modified by GUIDE v2.5 09-Aug-2013 10:22:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @choosefit_OpeningFcn, ...
                   'gui_OutputFcn',  @choosefit_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before choosefit is made visible.
function choosefit_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to choosefit (see VARARGIN)

handles.output = hObject;

% set selection change fcn
set(handles.uipanel4,'SelectionChangeFcn',{@selcbk, handles});

% reuse data or init
data = getappdata(0, 'data');

%find E_max_bnd
%peaks_position = peaks(data.data.ELOarray(:,4,end));
[a, peaks_position] = max(data.data.ELOarray(:,4,end));
E = mean(data.data.ELOarray(peaks_position,1,end));
E_error = max(abs(data.data.ELOarray(peaks_position,1,end)-E));

if ~isfield(data, 'fitparam') || isempty(data.fitparam)
    % complete init ... easy
    data.fitparam.fitfunction='exponential';
    data.fitparam.N_start=1e10;
    data.fitparam.T_start=1;
    data.fitparam.maxiter=1000;
    data.fitparam.maxeval=1000;
    data.fitparam.div_a=20.0;
    data.fitparam.div_b=25.0;
    data.fitparam.div_c=3.0;
    data.fitparam.div_d=75.0;
    data.fitparam.div_r=15.0;
    data.fitparam.yesno = 0;
    data.fitparam.N0main=1e13;
    data.fitparam.kTmain=4;
    data.fitparam.Emax_bnd=E+E_error;
else
    % partial init ... bad, but musthave for down-compability ... better ?!?
    if ~isfield(data.fitparam, 'fitfunction'); data.fitparam.fitfunction = 'exponential'; end
    if ~isfield(data.fitparam, 'N_start'); data.fitparam.N_start = 1e10; end
    if ~isfield(data.fitparam, 'T_start'); data.fitparam.T_start = 1; end
    if ~isfield(data.fitparam, 'maxiter'); data.fitparam.maxiter = 1000; end
    if ~isfield(data.fitparam, 'maxeval'); data.fitparam.maxeval = 1000; end
    if ~isfield(data.fitparam, 'div_a'); data.fitparam.div_a = 20; end
    if ~isfield(data.fitparam, 'div_b'); data.fitparam.div_b = 25; end
    if ~isfield(data.fitparam, 'div_c'); data.fitparam.div_c = 3; end
    if ~isfield(data.fitparam, 'div_d'); data.fitparam.div_d = 75; end
    if ~isfield(data.fitparam, 'div_r'); data.fitparam.div_r = 15; end
    if ~isfield(data.fitparam, 'yesno'); data.fitparam.yesno = 0; end
    if ~isfield(data.fitparam, 'N0main'); data.fitparam.N0main = 1e13; end
    if ~isfield(data.fitparam, 'kTmain'); data.fitparam.kTmain = 4; end
    if ~isfield(data.fitparam, 'Emax_bnd'); data.fitparam.Emax_bnd = E+E_error; end
end
% and finaly set values to GUI
set(handles.uipanel4,'SelectedObject',findobj('tag', data.fitparam.fitfunction));
set(handles.N_start,'String', data.fitparam.N_start);
set(handles.T_start,'String',data.fitparam.T_start);
set(handles.maxfunedit,'String',data.fitparam.maxiter);
set(handles.maxevaledit,'String',data.fitparam.maxeval);
set(handles.edita,'String',data.fitparam.div_a);
set(handles.editb,'String',data.fitparam.div_b);
set(handles.editc,'String',data.fitparam.div_c);
set(handles.editd,'String',data.fitparam.div_d);
set(handles.editr,'String',data.fitparam.div_r);
set(handles.editN0main,'String',data.fitparam.N0main);
set(handles.editkTmain,'String',data.fitparam.kTmain);
set(handles.Emax_bnd,'String',data.fitparam.Emax_bnd);

% and set the data
setappdata(0, 'data', data);
% END reuse data or init

switch_fitfun(data.fitparam.fitfunction, handles);

% Update handles structure
guidata(hObject, handles);

function switch_fitfun(fitfun, handles)
switch fitfun
    case {'exponential', 'exponential2', 'exponentialn', 'nature'}
        set(handles.edita,'Enable','Off');
        set(handles.editb,'Enable','Off');
        set(handles.editc,'Enable','Off');
        set(handles.editd,'Enable','Off');
        set(handles.editr,'Enable','Off');
        set(handles.editN0main,'Enable','Off');
        set(handles.editkTmain,'Enable','Off');
    case 'exponential_with_solidangle'
        set(handles.edita,'Enable','On');
        set(handles.editb,'Enable','On');
        set(handles.editc,'Enable','On');
        set(handles.editd,'Enable','On');
        set(handles.editr,'Enable','On');
        set(handles.editN0main,'Enable','Off');
        set(handles.editkTmain,'Enable','Off');
    case 'transportfun'
        set(handles.edita,'Enable','On');
        set(handles.editb,'Enable','On');
        set(handles.editc,'Enable','On');
        set(handles.editd,'Enable','On');
        set(handles.editr,'Enable','On');
        set(handles.editN0main,'Enable','On');
        set(handles.editkTmain,'Enable','On');
    otherwise
        set(handles.edita,'Enable','Off');
        set(handles.editb,'Enable','Off');
        set(handles.editc,'Enable','Off');
        set(handles.editd,'Enable','Off');
        set(handles.editr,'Enable','Off');
        set(handles.editN0main,'Enable','Off');
        set(handles.editkTmain,'Enable','Off');
end

% --- Outputs from this function are returned to the command line.
function varargout = choosefit_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
varargout{1} = handles.output;

% --- Executes on button press in closebutton.
function closebutton_Callback(hObject, eventdata, handles)
% hObject    handle to closebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% gather all data and save it :)
data = getappdata(0, 'data');
data.fitparam.fitfunction = get(get(handles.uipanel4,'SelectedObject'),'tag');
data.fitparam.N_start = str2double(get(handles.N_start,'String'));
data.fitparam.T_start = str2double(get(handles.T_start,'String'));
data.fitparam.maxiter = str2double(get(handles.maxfunedit,'String'));
data.fitparam.maxeval = str2double(get(handles.maxevaledit,'String'));
data.fitparam.div_a = str2double(get(handles.edita,'String'));
data.fitparam.div_b = str2double(get(handles.editb,'String'));
data.fitparam.div_c = str2double(get(handles.editc,'String'));
data.fitparam.div_d = str2double(get(handles.editd,'String'));
data.fitparam.div_r = str2double(get(handles.editr,'String'));
data.fitparam.N0main = str2double(get(handles.editN0main,'String'));
data.fitparam.kTmain = str2double(get(handles.editkTmain,'String'));
data.fitparam.Emax_bnd = str2double(get(handles.Emax_bnd,'String'));
% yesno = 0; dont start fit
data.fitparam.yesno = 0;
setappdata(0, 'data', data);
close choosefit;

% --- Executes on button press in startfit.
function startfit_Callback(hObject, eventdata, handles)
% hObject    handle to startfit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% gather all data and save it :)
data = getappdata(0, 'data');
data.fitparam.fitfunction = get(get(handles.uipanel4,'SelectedObject'),'tag');
data.fitparam.N_start = str2double(get(handles.N_start,'String'));
data.fitparam.T_start = str2double(get(handles.T_start,'String'));
data.fitparam.maxiter = str2double(get(handles.maxfunedit,'String'));
data.fitparam.maxeval = str2double(get(handles.maxevaledit,'String'));
data.fitparam.div_a = str2double(get(handles.edita,'String'));
data.fitparam.div_b = str2double(get(handles.editb,'String'));
data.fitparam.div_c = str2double(get(handles.editc,'String'));
data.fitparam.div_d = str2double(get(handles.editd,'String'));
data.fitparam.div_r = str2double(get(handles.editr,'String'));
data.fitparam.N0main = str2double(get(handles.editN0main,'String'));
data.fitparam.kTmain = str2double(get(handles.editkTmain,'String'));
data.fitparam.Emax_bnd = str2double(get(handles.Emax_bnd,'String'));
% yesno = 1; start fit
data.fitparam.yesno = 1;
setappdata(0, 'data', data);
close choosefit;

function selcbk(hObject, eventdata, handles)
data = getappdata(0, 'data');

%find E_max_bnd
%peaks_position = peaks(data.data.ELOarray(:,4,end));
[a, peaks_position] = max(data.data.ELOarray(:,4,end));
E = mean(data.data.ELOarray(peaks_position,1,end));
E_error = max(abs(data.data.ELOarray(peaks_position,1,end)-E));

data.fitparam.fitfunction = get(get(hObject,'SelectedObject'),'tag');
switch_fitfun(data.fitparam.fitfunction, handles);
% reset all parameters !
data.fitparam.N_start=1e10;
data.fitparam.T_start=1;
data.fitparam.maxiter=1000;
data.fitparam.maxeval=1000;
data.fitparam.div_a=20.0;
data.fitparam.div_b=25.0;
data.fitparam.div_c=3.0;
data.fitparam.div_d=75.0;
data.fitparam.div_r=15.0;
data.fitparam.yesno = 0;
data.fitparam.N0main=1e13;
data.fitparam.kTmain=4;
data.fitparam.Emax_bnd=E+E_error;

set(handles.N_start,'String', data.fitparam.N_start);
set(handles.T_start,'String',data.fitparam.T_start);
set(handles.maxfunedit,'String',data.fitparam.maxiter);
set(handles.maxevaledit,'String',data.fitparam.maxeval);
set(handles.edita,'String',data.fitparam.div_a);
set(handles.editb,'String',data.fitparam.div_b);
set(handles.editc,'String',data.fitparam.div_c);
set(handles.editd,'String',data.fitparam.div_d);
set(handles.editr,'String',data.fitparam.div_r);
set(handles.editN0main,'String',data.fitparam.N0main);
set(handles.editkTmain,'String',data.fitparam.kTmain);
set(handles.Emax_bnd,'String',data.fitparam.Emax_bnd);

setappdata(0, 'data', data);
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function Emax_bnd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Emax_bnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Emax_bnd_Callback(hObject, eventdata, handles)
% hObject    handle to Emax_bnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Emax_bnd as text
%        str2double(get(hObject,'String')) returns contents of Emax_bnd as a double


% --- Executes on button press in lastlayer.
function lastlayer_Callback(hObject, eventdata, handles)
% hObject    handle to lastlayer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = getappdata(0, 'data');
[data.parameters.lastrcffile, data.parameters.lastrcfpath] = uigetfile(...
    {'*.dat',  'dat files (*.dat)'}, 'Pick last Energyloss film dat-file (with no signal)', ...
    'MultiSelect', 'off');
data.data.ELOarraylast = [];

file_name = fullfile(data.parameters.lastrcfpath, data.parameters.lastrcffile);
data.data.ELOarraylast = importdata(file_name);

%find useful value for Emax boundary for fit, where in this layer is still
%no signal - take average between no signal an bragg-peak
%peak=peaks(data.data.ELOarraylast(:,2));
[a, peak] = max(data.data.ELOarraylast(:,2));
[~,indmin]=min(flipud(data.data.ELOarraylast(:,2)));
indmin=length(data.data.ELOarraylast)-indmin;
indEmax=floor((indmin+peak(1))/2);

data.fitparam.Emax_bnd=data.data.ELOarraylast(indEmax,1);
set(handles.Emax_bnd,'String',data.fitparam.Emax_bnd);

setappdata(0, 'data', data);
guidata(hObject,handles);

