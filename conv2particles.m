function conv2particles(varargin)
% semi graphical deconvolution

data = getappdata(0, 'data');
statustext = varargin{1};

set(statustext,'String', 'Deconvolution: Calculating ... ');
pause(0.1)

fun = @(x,N,T) N./x.*exp(-(x./T)); % dN/dE

N0 = data.results.fitdeconv{1}(1); % fitted N0
T0 = data.results.fitdeconv{1}(2); % fitted T0
E0 = data.results.fitdeconv{1}(3); % fitted Emax

imgArrayCorr = data.data.ImageArray;
imgArrayNP = data.data.ImageArray;
eloArray = data.data.ELOarray;

rcfconf = data.parameters.rcfconfig;

E_measured = data.results.fitdeconv{6}(:,1); % measured energies from RCF_EnergyLoss

len = length(E_measured);

% get image sizes for later gaus, Ampli, Power
x = -size(imgArrayCorr,2)/2 : size(imgArrayCorr,2)/2 - 1;
y = -size(imgArrayCorr,1)/2:size(imgArrayCorr,1)/2-1;
p = 1;
A = 1;

shrink = 0.95; % shrink value
remember = false; % init var for layers that are closer than 1 MeV

for ii = 2:len
    set(statustext,'String', ['Deconvoluting Film: ' num2str(len-ii+2)]);
    drawnow;
    
    %calculate particles in actual layer
    % film start, first index
    % plus/minus 0.5MeV around bragg-peak
    % only if [peak-0.5,peak+0.5]
    [a,peak] = max(eloArray(:,4,len-ii+2));
    peakrb = find(eloArray(:,1,len-ii+2) > eloArray(peak,1,len-ii+2)+0.5, 1, 'first');
    peaklb = find(eloArray(:,1,len-ii+2) > eloArray(peak,1,len-ii+2)-0.5, 1, 'first');
    %look for zero energyloss in interval
    shift=max(find(eloArray(peaklb:peakrb,4,len-ii+2)==0));
    if isempty(shift)
        shift=0;
    else
        shift=shift+1;
    end
    
    fi=peaklb+shift;
          
    % film end, last index (1MeV bin)
    if remember
        % if 2 layer overlap then end index should be max start of next layer
        %li2 = find(eloArray(:,4,len-ii+3) > 0, 1, 'first')-1;
        %li = find(eloArray(:,1,len-ii+2) > eloArray(fi,1,len-ii+2)+1, 1, 'first');
        [a,peak] = max(eloArray(:,4,len-ii+3));
        peakrb = find(eloArray(:,1,len-ii+3) > eloArray(peak,1,len-ii+3)+0.5, 1, 'first');
        peaklb = find(eloArray(:,1,len-ii+3) > eloArray(peak,1,len-ii+3)-0.5, 1, 'first');
        %look for zero energyloss in interval
        shift=max(find(eloArray(peaklb:peakrb,4,len-ii+3)==0));
        if isempty(shift)
          shift=0;
        else
          shift=shift+1;
        end
        li2=peakrb+shift;
        
        [a,peak] = max(eloArray(:,4,len-ii+2));
        peakrb = find(eloArray(:,1,len-ii+2) > eloArray(peak,1,len-ii+2)+0.5, 1, 'first');
        peaklb = find(eloArray(:,1,len-ii+2) > eloArray(peak,1,len-ii+2)-0.5, 1, 'first');
        %look for zero energyloss in interval
        shift=max(find(eloArray(peaklb:peakrb,4,len-ii+2)==0));
        if isempty(shift)
          shift=0;
        else
          shift=shift+1;
        end
        li=peakrb+shift;             
        li = min([li li2]);
        clear li2
        remember = false;
        scaleToMeV = eloArray(li,1,len-ii+2)-eloArray(fi,1,len-ii+2);
    else
        if ii == 2
            % 19.01.2014 CB
%         % for the last layer to fit the spectrum ... dunno
%         % else this routine yield to low particle numbers ....
            Ntest = 1e10;
            ENmax = 10;
            while Ntest > 1e8
                ENmax = ENmax+0.1;
                Ntest = integral(@(E2)N0./E2.*exp(-E2./T0),ENmax-1,ENmax);
            end
            Erange = ENmax-E0
            if Erange > 8
                Erange = 8;
            end
            li = find(eloArray(:,1,len-ii+2) > eloArray(fi,1,len-ii+2)+Erange, 1, 'first')
            if isempty(li)
                li=size(eloArray,1);
            end
        else
            li = find(eloArray(:,1,len-ii+2) > eloArray(fi,1,len-ii+2)+1, 1, 'first');
        end
        scaleToMeV = 1;
    end
    
%    eloArray(fi:li-1,1,len-ii+2)
    
    m_eloss = mean(eloArray(fi:li-1,4,len-ii+2));
    imgArrayNP(:,:,len-ii+2) = imgArrayCorr(:,:,len-ii+2)/m_eloss/scaleToMeV;
%     count_p = sum(sum(imgArrayCorr(:,:,len-ii+2)))/m_eloss;
%     imgArrayNP(:,:,len-ii+2) = imgArrayCorr(:,:,len-ii+2)/sum(sum(imgArrayCorr(:,:,len-ii+2))) * count_p;
    
    % same for actual metal layer, start of front layer +1MeV -> start
    % metal layer
    %fim = find(eloArray(:,4,len-ii+1) > 0, 1, 'first');
    [a,peak] = max(eloArray(:,4,len-ii+1));
    peakrb = find(eloArray(:,1,len-ii+1) > eloArray(peak,1,len-ii+1)+0.5, 1, 'first');
    peaklb = find(eloArray(:,1,len-ii+1) > eloArray(peak,1,len-ii+1)-0.5, 1, 'first');
    %look for zero energyloss in interval
    shift=max(find(eloArray(peaklb:peakrb,4,len-ii+1)==0));
    if isempty(shift)
        shift=0;
    else
        shift=shift+1;
    end
    
    fim=peaklb+shift;   
    lim = find(eloArray(:,1,len-ii+1) > eloArray(fim,1,len-ii+1)+1, 1, 'first');
    % gues NP in metal layer from dN/dE
    % if lim > fi then there is no metal layer ?!
    if lim >= fi
        NPm = 0;
        % remember that actual front layer is in actual layer ...
        remember = true;
    else
        NPm = integral(@(x)fun(x,N0,T0), eloArray(lim,1,len-ii+2), eloArray(fi-1,1,len-ii+2));
    end
    
    for i = 1:len-ii+1
        % actual dimension and sigma for layer
        SX = (find(sum(imgArrayCorr(:,:,len-i-ii+2),1),1, 'last') - find(sum(imgArrayCorr(:,:,len-i-ii+2),1),1, 'first'))/2;
        X0 = (find(sum(imgArrayCorr(:,:,len-i-ii+2),1),1, 'last') + find(sum(imgArrayCorr(:,:,len-i-ii+2),1),1, 'first'))/2 -size(imgArrayCorr,2)/2;
        SY = (find(sum(imgArrayCorr(:,:,len-i-ii+2),2),1, 'last') - find(sum(imgArrayCorr(:,:,len-i-ii+2),2),1, 'first'))/2;
        Y0 = (find(sum(imgArrayCorr(:,:,len-i-ii+2),2),1, 'last') + find(sum(imgArrayCorr(:,:,len-i-ii+2),2),1, 'first'))/2 -size(imgArrayCorr,1)/2;

        % letztes bild holen und verkleinern um bestimmten wert
        last_img = imgArrayCorr(:,:,len-ii+2);
        % verkleinern je nach layer
        last_img = imresize(last_img, shrink^i);
        % repad to size
        last_img = padarray(last_img, round(.5*[size(imgArrayCorr,1)-size(last_img,1) size(imgArrayCorr,2)-size(last_img,2)]));
        % check sizes ... sad
        if size(last_img,1) > size(imgArrayCorr,1)
            last_img(size(imgArrayCorr,1)+1:end,:) = [];
        elseif size(last_img,1) < size(imgArrayCorr,1)
            last_img(end+1:size(imgArrayCorr,1),:) = 0;
        end
        if size(last_img,2) > size(imgArrayCorr,2)
            last_img(:,size(imgArrayCorr,2)+1:end) = [];
        elseif size(last_img,2) < size(imgArrayCorr,2)
            last_img(:,end+1:size(imgArrayCorr,2)) = 0;
        end
        % attenuation_rcf scales in first order with active layer thickness
        % thicknesses from RCF_EnergyLoss_GUI
        if strcmpi(rcfconf{len-ii+2}, rcfconf{len-i-ii+2})
            % both are same, do nothing
            attenuation_rcf = 1;
        else
            % layer is diffrent so it should be weightend different
            % last layer
            switch rcfconf{len-ii+2}
                case 'HD'
                    alay = 6.50;
                case 'M2'
                    alay = 2*17.5;
                case {'H2', 'H1A', 'H1B', 'H1C'}
                    alay = 8;
                case {'E3', 'ES', 'E1A', 'E1B', 'E1D', 'E1E'}
                    alay = 28;
                otherwise
                    alay = 1;
            end
            llay = alay;
            % actual layer
            switch rcfconf{len-i-ii+2}
                case 'HD'
                    alay = 6.50;
                case 'M2'
                    alay = 2*17.5;
                case {'H2', 'H1A', 'H1B', 'H1C'}
                    alay = 8;
                case {'E3', 'ES', 'E1A', 'E1B', 'E1D', 'E1E'}
                    alay = 28;
                otherwise
                    alay = 1;
            end
            attenuation_rcf = alay / llay;
        end
        % mean eloss of particles in actual front layer of x layer
        m_eloss = mean(eloArray(fi:li-1,4,len-i-ii+2));
        % attenuation_dist scales with mean eloss and peak eloss
        attenuation_dist = m_eloss / max(eloArray(:,4,len-i-ii+2));

        % add last image to image len-i
        add_img = last_img * attenuation_rcf * attenuation_dist + imgArrayCorr(:,:,len-i-ii+2);
        % convolve with gaus ...
        add_img = add_img .* s2dGaussian(x, 2*SX, X0, A, y ,2*SY, Y0, p);
        % cut with original image
        add_img(imgArrayCorr(:,:,len-i-ii+2) == 0) = 0;
        % normalize integral 1
        add_img = add_img / sum(sum(add_img));
        
        % mean eloss of particles in actual front layer of x metal layer
        if remember
            m_eloss_m = 0;
        else
            m_eloss_m = mean(eloArray(lim:fi-1,4,len-i-ii+2));
        end
        
        substr_img = add_img * (m_eloss * sum(sum(imgArrayNP(:,:,len-ii+2))) + ...
            m_eloss_m * NPm);
        % substract
        imgArrayCorr(:,:,len-i-ii+2) = imgArrayCorr(:,:,len-i-ii+2) - substr_img;
    end
end

% do last film by hand.
% film start, first index
fi = find(eloArray(:,4,1) > 0, 1, 'first');
% film end, last index (1MeV bin)
if remember
    % if 2 layer overlap then end index should be max start of next layer
    li2 = find(eloArray(:,4,2) > 0, 1, 'first');
    li = find(eloArray(:,1,1) > eloArray(fi,1,1)+1, 1, 'first');
    li = min([li li2]);
    clear li2
else
    li = find(eloArray(:,1,1) > eloArray(fi,1,1)+1, 1, 'first');
end
m_eloss = mean(eloArray(fi:li-1,4,1));
imgArrayNP(:,:,1) = imgArrayCorr(:,:,1)/m_eloss;

data.data.imgArrayNP = imgArrayNP;
setappdata(0, 'data', data);
%imgArrayNP(imgArrayNP<0)=0;
squeeze(sum(sum(imgArrayNP,1),2))

figure()

semilogy(E_measured+1, squeeze(sum(sum(imgArrayNP,1),2))) % 1mev interval

imgArrayNP = log(imgArrayNP);
imgArrayNP(imgArrayNP < 0) =0;
h = figure();
sstack(imgArrayNP, h);

set(statustext,'String', 'Deconvolution: Finished ... ');
drawnow;
end

function [gaussian] = s2dGaussian(x, SX, X0, A, y ,SY, Y0, p)
[x, y]  = meshgrid(x,y);
gaussian = A * exp( -( (x-X0).^2/(2*SX^2) + (y-Y0).^2/(2*SY^2) ).^p );
end

% -------- Function to show 4D-Array in axes window
function sstack(ImageArray, fig)
%display all images in montage

%for plotrcf "small" images are quite good
%scale all to 250dpi for saving memory in montage-plot !
tmparray = single([]);

scale = 0.25;

for frame = 1:size(ImageArray,3);
    tmparray(:,:,1,frame) = imresize(ImageArray(:,:,frame), scale, 'bilinear');
end

tmparray = log(tmparray);
tmparray(tmparray < 0) = 0;

clim_min=double(min(min(min(tmparray(:,:,:,1)))));
clim_max=double(max(max(max(tmparray(:,:,:,1)))));
montage(tmparray, [clim_min clim_max]);
caxis([clim_min 1.2*clim_max]);

colormap rcf;
colorbar;
axis equal;
%Add text of energies
switch get(fig, 'Type')
    case 'figure'
        ax = get(fig,'CurrentAxes');
        set(ax, 'FontSize', 16);
    otherwise
        ax = fig;
        set(ax, 'FontSize', 14);
end
set(ax, 'Units', 'normalized', 'Visible', 'off');

clear tmparray
end