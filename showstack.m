% -------- Function to show 4D-Array in axes window
function showstack(data, handles, fig)
%display all images in montage

strarray=get(handles.rcfstack_linlog, 'String');
strindex=get(handles.rcfstack_linlog, 'Value');

%for plotrcf "small" images are quite good
%scale all to 250dpi for saving memory in montage-plot !
tmparray = single([]);

xres = data.data.fileinfo.XResolution;

if xres == 500
    scale = 0.5;
elseif xres == 1000;
    scale = 0.25;
else
    scale = 1;
end

for frame = 1:size(data.data.ImageArray,3);
    tmparray(:,:,1,frame) = imresize(data.data.ImageArray(:,:,frame), scale, 'bilinear');
end

switch char(strarray(strindex))
    case 'log'
            tmparray = log(0.001+tmparray); %0.001+ to avoid log(0)
    otherwise
end

clim_min=double(min(min(min(tmparray(:,:,:,1)))));
clim_max=double(max(max(max(tmparray(:,:,:,1)))));
montage(tmparray,'DisplayRange', [clim_min clim_max]);
caxis([clim_min 1.2*clim_max]);

colormap(fig,rcf);
h=colorbar;
axis equal;
%Add text of energies
switch get(fig, 'Type')
    case 'figure'
        ax = get(fig,'CurrentAxes');
        set(ax, 'FontSize', 16);
    otherwise
        ax = fig;
        set(ax, 'FontSize', 14);
end
set(ax, 'Units', 'normalized', 'Visible', 'off');
colormap(ax,rcf);

switch char(strarray(strindex))
    case 'log'
        ylabel(h,'ln(dep. Energie in MeV/px)');
    otherwise
        ylabel(h,'dep. Energie in MeV/px');
end

clear tmparray