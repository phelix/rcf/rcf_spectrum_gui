function varargout = deleteRCF(varargin)
% DELETERCF MATLAB code for deleteRCF.fig
%      DELETERCF, by itself, creates a new DELETERCF or raises the existing
%      singleton*.
%
%      H = DELETERCF returns the handle to a new DELETERCF or the handle to
%      the existing singleton*.
%
%      DELETERCF('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DELETERCF.M with the given input arguments.
%
%      DELETERCF('Property','Value',...) creates a new DELETERCF or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before deleteRCF_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to deleteRCF_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help deleteRCF

% Last Modified by GUIDE v2.5 10-Aug-2013 08:06:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @deleteRCF_OpeningFcn, ...
                   'gui_OutputFcn',  @deleteRCF_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before deleteRCF is made visible.
function deleteRCF_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to deleteRCF (see VARARGIN)

% Choose default command line output for deleteRCF
handles.output = hObject;
data = getappdata(0, 'data');
handles.backup = data.parameters;

% UIWAIT makes deleteRCF wait for user response (see UIRESUME)
%uiwait(handles.figure1);

% Update handles structure
setappdata(0,'yesno',0);
clear data
guidata(hObject, handles);



% --- Outputs from this function are returned to the command line.
function varargout = deleteRCF_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in dellistbox.
function dellistbox_Callback(hObject, eventdata, handles)
% hObject    handle to dellistbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns dellistbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from dellistbox
data = getappdata(0, 'data');
set(hObject, 'String', data.parameters.rcfconfig_numb);
clear data

% --- Executes during object creation, after setting all properties.
function dellistbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dellistbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
data = getappdata(0, 'data');
set(hObject, 'String', data.parameters.rcfconfig_numb);
clear data


% --- Executes on key press with focus on dellistbox and none of its controls.
function dellistbox_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to dellistbox (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
data = getappdata(0, 'data');
if strcmp(eventdata.Key,'delete')
    SVV = data.parameters.rcfconfig_numb;
    SVV2 = data.parameters.rcfconfig;
    SVV3 = data.parameters.rcffiles;
    SVV4 = data.parameters.elossfiles;
    SV = get(hObject,{'string','value'});
    if ~cellfun('isempty',SV{1})
        SV{1}(SV{2}) = [];
        SVV(SV{2}) = [];
        SVV2(SV{2}) = [];
        SVV3(SV{2}) = [];
        SVV4(SV{2}) = [];      
        
        set(hObject,'string',SV{1},'value',1)
    end
    data.parameters.rcfconfig_numb = SVV;
    data.parameters.rcfconfig = SVV2;
    data.parameters.rcffiles = SVV3;
    data.parameters.elossfiles = SVV4;
    
    
    setappdata(0,'data',data);
    guidata(hObject,handles);
end
clear data


% --- Executes on button press in undobutton.
function undobutton_Callback(hObject, eventdata, handles)
% hObject    handle to undobutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = getappdata(0, 'data');
data.parameters=handles.backup;
set(handles.dellistbox, 'String', data.parameters.rcfconfig_numb);
setappdata(0,'data',data);
clear data
guidata(hObject,handles);


% --- Executes on button press in applybutton.
function applybutton_Callback(hObject, eventdata, handles)
% hObject    handle to applybutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = getappdata(0, 'data');

%remove missing stuff from ImageArray etc
%find removed numbers from rcf-parameters
rcfconfig=data.parameters.rcfconfig_numb;
%original numbers of layers
numlay=size(data.data.ImageArray,3);
numlay_org=1:numlay;
numlay_new=[];

for i=1:length(rcfconfig)
    numlay_new(i)=str2double(strtok(data.parameters.rcfconfig_numb{i}));
end


toremove=setxor(numlay_new,numlay_org);
data.data.ImageArray(:,:,toremove)=[];
data.data.errMeVpx(:,:,toremove)=[];
data.data.ImageMask(:,:,toremove)=[];
data.data.ELOarray(:,:,toremove)=[];

rcfconf=cell(1,length(data.parameters.rcfconfig));
for i=1:length(data.parameters.rcfconfig)
      rcfconf{i}=[num2str(i,'%02i'),' ',data.parameters.rcfconfig{i}];
end
data.parameters.rcfconfig_numb=rcfconf;

setappdata(0,'data',data);
setappdata(0,'yesno',1);

guidata(hObject,handles);
clear data
close deleteRCF


% --- Executes on button press in discardbutton.
function discardbutton_Callback(hObject, eventdata, handles)
% hObject    handle to discardbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close deleteRCF
