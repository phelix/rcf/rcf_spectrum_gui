function [deconv_estimates, model, meanerror, exitflag, output, EvsDOSE_measured, EvsNP_fitted, ENmax] = fitdeconv_processor(data)

%second approximation, deconvolution
ELOarray = data.data.ELOarray;
last=size(data.data.ImageArray,3);

E = zeros(1,last);
E_error = zeros(1,last);
dose = zeros(1,last);

for frame = 1:last
   %peaks_position = peaks(ELOarray(:,4,frame));
   [~, peaks_position] = max(ELOarray(:,4,frame));
   E(frame) = mean(ELOarray(peaks_position,1,frame));
   E_error(frame) = max(abs(ELOarray(peaks_position,1,frame)-E(frame)));
   
   %%for testing and debugging each fit
   %dose(frame) = (0.8 + 0.3*rand(1))*sum(sum(data.data.ImageArray(:,:,:,frame)));
   %dose(frame) = (0.8 + 0.2*rand(1))*simpsum(1e12*1./ELOarray(:,1,frame).*exp(-ELOarray(:,1,frame).^2./12.^2).*ELOarray(:,4,frame), 34.8);
   %ELOarray2 = ELOarray((ELOarray(:,1,1)<=33.256),:,:);
   %dose(frame) = (0.7 + 0.3*rand(1))*simpsum(1e12*1./sqrt(2*ELOarray2(:,1,frame)*12).*exp(-sqrt(2*ELOarray2(:,1,frame)./12)).*ELOarray2(:,4,frame), 33.256);
   
%    fitopts = data.fitparam;
%    angle = fitopts.div_a*sqrt(-2.0*log(ELOarray(:,1,frame)./fitopts.div_b)).*(ELOarray(:,1,frame)./fitopts.div_b).^(1.0/fitopts.div_c); %half angle distribution of proton beam
%    r1=fitopts.div_d*tan(angle*pi/180.); %beam radius at entrance of apperture
%    solid=fitopts.div_r^2./r1.^2; %ratio of apperture to real beam -> defines ratio of protons through appeture; assumption: homogenious spacial beam profile
%    solid(solid>=1.0)=1.0;%ratio can max=1 -> all protons pass apperture; beam smaller apperture -> solid=1.0
%    
%    dose(frame) = (0.8 + 0.2*rand(1))*simpsum(1e12*solid./ELOarray(:,1,frame).*exp(-ELOarray(:,1,frame)./4).*ELOarray(:,4,frame), 33.256);
   dose(frame) = sum(sum(data.data.ImageArray(:,:,frame),'double'),'double');
end

dose_error = squeeze(sum(sum(data.data.errMeVpx,1,'double'),2,'double'))';

%find E_max -> will be parameter for fit
data.fitparam.E_max = E(last);
data.fitparam.E_max_low = E(last)-E_error(last);

%run best fit as long meanerror is smaller than before
flag=true;
meanerrorbefore=0;

while (flag)
%best fit
[model, text] = model_handler([data.fitparam.fitfunction '_dose']);
options = optimset('Display', 'none', ...
                   'MaxFunEvals', data.fitparam.maxeval, ...
                   'TolX', 1e-12, ...
                   'TolFun', 1e-12, ...
                   'MaxIter', data.fitparam.maxiter);
 datas = {E, ... % xdata
     log(dose), ... % ydata
     data.fitparam, ... %fitparams
     log(dose_error), ... % deltay
     E_error %E error
     };

%use of new fminsearchbnd which has option for parameter boundaries => nice
%for E_max parameter => should be between E_max we see in last colored
%layer and E_max_up of layer we don't see a signal
[deconv_estimates, ~, exitflag, output] = fminsearchbnd(@(x) model(x, datas), ...
    [data.fitparam.N_start*1e-12, data.fitparam.T_start, data.fitparam.E_max], ...
    [-inf, -inf, data.fitparam.E_max_low], [inf, inf, data.fitparam.Emax_bnd], options);

% [deconv_estimates, ~, exitflag, output] = fminsearch(@(x) model(x, datas), ...
%     [data.fitparam.N_start*1e-12, data.fitparam.T_start, data.fitparam.E_max], ...
%     options);

switch exitflag
    case 1
%         disp('fminsearch converged to a solution x')
    case 0
%         disp('Maximum number of function evaluations or iterations was reached.')
%         disp('Fit not possible!')
%        return
    case -1
%         disp('Algorithm was terminated by the output function.')
%         disp('Fit not possible!')
%        return
end

[~, dose_fitted, sdr] = model(deconv_estimates, datas);
meanerror = mean(abs(exp(dose_fitted)-dose)./dose)*100; %mean deviation in %

%repeat only up to percision 1e-6 of meanerror
meanerror = floor(meanerror*1e4)/1e4;

if meanerror>meanerrorbefore
    meanerrorbefore=meanerror;
    data.fitparam.N_start=deconv_estimates(:,1)*1e12;
    data.fitparam.T_start=deconv_estimates(:,2);
    data.fitparam.E_max=deconv_estimates(:,3);
else
    flag=false;
end

end

% fit values
N_deconv = deconv_estimates(:,1)*1e12;
T_deconv = deconv_estimates(:,2);
Emax_deconv = deconv_estimates(:,3);

%for jacobian error estimation we only need 2 of the fitted 3 parameters
deconv_estimatesJ = [deconv_estimates(:,1), deconv_estimates(:,2)];

% degrees of freedom in the problem; 2 accounts for two fit-parameters
dof = length(dose) - 2;

% standard deviation of the residuals
sdr = sqrt(sdr/dof);

%catch the pure model parameterised
[modelfunc, ~] = model_handler([data.fitparam.fitfunction '_func']);

% jacobian matrix
J = jacobianest(modelfunc(Emax_deconv,datas),deconv_estimatesJ);
J = J*1e-10;

Sigma = sdr^2./(J'*J)*1e-20;

% Parameter standard errors
se = sqrt(diag(Sigma))';

% which suggest rough confidence intervalues around
% the parameters might be...
deconv_estimates_upJ = deconv_estimatesJ + 2*se;
deconv_estimates_lowJ = deconv_estimatesJ - 2*se;

deconv_estimates_up = [deconv_estimates_upJ, Emax_deconv];
deconv_estimates_low = [deconv_estimates_lowJ, Emax_deconv];

[~, dose_fitted_up, ~] = model(deconv_estimates_up, datas);
[~, dose_fitted_low, ~] = model(deconv_estimates_low, datas);

% fit values conf-interval
N_deconvup = deconv_estimates_up(:,1)*1e12;
T_deconvup = deconv_estimates_up(:,2);
N_deconvlow = deconv_estimates_low(:,1)*1e12;
T_deconvlow = deconv_estimates_low(:,2);

N_deconvErr = ((N_deconv-N_deconvlow)+(N_deconvup-N_deconv))/2;
T_deconvErr = ((T_deconv-T_deconvlow)+(T_deconvup-T_deconv))/2;

%find the exponent of the fit parameters
N_exp=floor(log10(N_deconv));

N_deconv=N_deconv*10^(-N_exp);
N_deconvErr=N_deconvErr*10^(-N_exp);

deconv_estimates = [deconv_estimates(:,1)*1e12, deconv_estimates(:,2), deconv_estimates(:,3)];

figure();
subplot(2,2,[3 4]);
plot(E, exp(dose_fitted),'-*')
hold all
plot(E, exp(dose_fitted_low),'--k')
phandle=plot(E, exp(dose_fitted_up),'--k');

%hide 3 legend entry
hasbehavior(phandle,'legend',false);

I = data.parameters.laser.energy * 2*sqrt(log(2))/ ...
    (data.parameters.laser.pulseduration*1e-15*pi*sqrt(pi)*( ...
    data.parameters.laser.focusdia*1e-6/(2*sqrt(log(2))))^2)*1e-4; %Intensity in W/cm^2

subplot(2,2,[3 4]);
% plot(E, exp(dose_fittednature),'-o')
% errorbarxy(E, dose, E_error, dose_error, [],[],'ro','r')
errorbar(E, dose, dose_error, 'ro')
axis([min(E)-1 max(E)+2 min(dose)-0.8*min(dose) max(dose)+0.8*max(dose)])
set(gca,'YScale','log')
xlabel('E (MeV)');
ylabel('deposited energy E_{dep} (MeV)');
% legend('calculated dep. energy from fit','calculated dep. energy with J. Fuchs scaling law','measured dep. energy in RCF');
legend('calculated dep. energy from fit', '95% confidence band', 'measured dep. energy in RCF');
hold off

ll2 = ['N_0 = (',num2str(N_deconv,'%5.2f'), ' ', char(177), ' ', num2str(N_deconvErr,'%5.2f'),') \cdot 10^{', num2str(N_exp,'%1.0f'),'}'];
ll3 = ['kT = (',num2str(T_deconv,'%5.2f'), ' ', char(177), ' ', num2str(T_deconvErr,'%5.2f'), ') MeV'];
ll31=['E_{cut} = ',num2str(Emax_deconv,'%3.2f'),' MeV'];

%calculate conversion efficiency above 4 MeV
Elaser = data.parameters.laser.energy;
%summarize over all measured data points 

Etotal = 0;
Etotalup = 0;
Etotallow = 0;
N_deconv=N_deconv*10^(N_exp);

if last ~=1
    for i = last:-1:1
        switch char(data.fitparam.fitfunction)
            case {'exponential', 'transportfun'}
                number = N_deconv/E(i)*exp(-(E(i)./T_deconv));
                numberup = N_deconvup/E(i)*exp(-(E(i)./T_deconvup));
                numberlow = N_deconvlow/E(i)*exp(-(E(i)./T_deconvlow));
            case 'exponential2'
                number = N_deconv/E(i)*exp(-(E(i).^2/T_deconv^2));
                numberup = N_deconvup/E(i)*exp(-(E(i).^2/T_deconvup));
                numberlow = N_deconvlow/E(i)*exp(-(E(i).^2/T_deconvlow));
            case 'nature'
                number = N_deconv/sqrt(2*E(i)*T_deconv)*exp(-sqrt(2*E(i)/T_deconv));
                numberup = N_deconvup/sqrt(2*E(i)*T_deconvup)*exp(-sqrt(2*E(i)/T_deconvup));
                numberlow = N_deconvlow/sqrt(2*E(i)*T_deconvlow)*exp(-sqrt(2*E(i)/T_deconvlow));
            case 'exponential_with_solidangle'
                number = N_deconv/E(i)*exp(-(E(i)./T_deconv));
                numberup = N_deconvup/E(i)*exp(-(E(i)./T_deconvup));
                numberlow = N_deconvlow/E(i)*exp(-(E(i)./T_deconvlow));
            otherwise
                number = 0;
        end
        if (i ~= length(E)) %avoid problems at end of spectrum
           intervalpos(i)=abs(0.5*(E(i+1)-E(i)));
        else
           intervalpos(i)=abs(0.5*(E(i)-E(i-1)));
        end
        if (i ~= 1) %avoid problems at first RCF
           intervalneg(i)=abs(0.5*(E(i)-E(i-1)));
        else
           intervalneg(i)=abs(0.5*(E(i+1)-E(i)));
        end
        Ecurrent=number*(intervalneg(i)+intervalpos(i))*E(i);
        Ecurrentup=numberup*(intervalneg(i)+intervalpos(i))*E(i);
        Ecurrentlow=numberlow*(intervalneg(i)+intervalpos(i))*E(i);
        if (E(i) >= 4)
           Etotal=Etotal+Ecurrent;
           Etotalup=Etotalup+Ecurrentup;
           Etotallow=Etotallow+Ecurrentlow;
        end
    end
else
end

Eproton = Etotal*1e6*1.602e-19;
Eprotonup = Etotalup*1e6*1.602e-19;
Eprotonlow = Etotallow*1e6*1.602e-19;

conversion = Eproton/Elaser*100;
conversionup = Eprotonup/Elaser*100;
conversionlow = Eprotonlow/Elaser*100;

conversionErr = ((conversion-conversionlow)+(conversionup-conversion))/2;

ll4 = ['Laser energy = ',num2str(Elaser),' J'];
ll5 = ['Laser to proton energy (> 4 MeV) conversion efficieny: (',num2str(conversion,'%5.2f'), ' ', char(177), ' ', num2str(conversionErr,'%5.2f'),') %'];

annotation('textbox',[0.01 0.85 1 0.15],'string',{text;ll2;ll3;ll31;ll4;ll5;},...
               'LineStyle','none','FontName','Helvetica','FontSize',10, 'FontWeight', 'bold');
set(gca,'OuterPosition',[0 -0.005 1 0.75]);

%intergrate reversed, cause we want particle-number at cut-off exactly
lastEnergy = Emax_deconv; %length(energy)
restEn=Emax_deconv-floor(Emax_deconv);
energy = lastEnergy:-1:restEn; %1MeV bins, starting with E_cut_off down close to one

N = zeros(1,length(energy)-1);
Nup = N; Nlow = N; ratio = N; Nmain = N;

figure();
switch char(data.fitparam.fitfunction)
    case 'exponential'
        for i=1:length(energy)-1
            N(i)=integral(@(E2)N_deconv./E2.*exp(-E2./T_deconv),energy(i+1),energy(i));
            Nup(i)=integral(@(E2)N_deconvup./E2.*exp(-E2./T_deconvup),energy(i+1),energy(i));
            Nlow(i)=integral(@(E2)N_deconvlow./E2.*exp(-E2./T_deconvlow),energy(i+1),energy(i));
        end
    case 'transportfun'
        for i=length(energy)-1
            N(i)=integral(@(E2)N_deconv./E2.*exp(-E2./T_deconv),energy(i+1),energy(i));
            %ratio is area(aperture)/area(beam(E)) times
            %transportlosses-function for the special case of PLH2012Jun PQM
            %setup
            ratio(i) = handles.fitparam.div_r^2 / ( handles.fitparam.div_d * tan((handles.fitparam.div_a + handles.fitparam.div_b*energy(i) + handles.fitparam.div_c*energy(i)^2) *pi/180) )^2 * (1-exp(-0.0009525*energy(i)^(3.5973)));
            Nmain(i) = ratio(i) * integral(@(E2)handles.parameters.N0main./E2.*exp(-E2./handles.parameters.kTmain),energy(i+1),energy(i));
            if Nmain(i)<0, Nmain(i)=0; end
        end
    case 'exponential2'
        for i=length(energy)-1
            N(i)=integral(@(E2)N_deconv./E2.*exp(-E2.^2/T_deconv^2),energy(i+1),energy(i));
            Nup(i)=integral(@(E2)N_deconvup./E2.*exp(-E2.^2/T_deconvup^2),energy(i+1),energy(i));
            Nlow(i)=integral(@(E2)N_deconvlow./E2.*exp(-E2.^2/T_deconvlow^2),energy(i+1),energy(i));
        end
    case 'nature'
        for i=length(energy)-1
            N(i)=integral(@(E2)N_deconv./sqrt(2*E2*T_deconv).*exp(-sqrt(2*E2/T_deconv)),energy(i+1),energy(i));
            Nup(i)=integral(@(E2)N_deconvup./sqrt(2*E2*T_deconvup).*exp(-sqrt(2*E2/T_deconvup)),energy(i+1),energy(i));
            Nlow(i)=integral(@(E2)N_deconvlow./sqrt(2*E2*T_deconvlow).*exp(-sqrt(2*E2/T_deconvlow)),energy(i+1),energy(i));
        end
    case 'exponential_with_solidangle'
        fitopts = data.fitparam;
        for i=length(energy)-1
            angle=fitopts.div_a*sqrt(-2.0*log(i/fitopts.div_b))*(i/fitopts.div_b)^(1.0/fitopts.div_c); %half angle distribution of proton beam
            r1=fitopts.div_d*tan(angle*pi/180.); %beam radius at entrance of apperture
            solid=fitopts.div_r^2/r1^2; %ratio of apperture to real beam -> defines ratio of protons through appeture; assumption: homogenious spacial beam profile
            solid(solid>=1.0)=1.0; %ratio can max=1 -> all protons pass apperture; beam smaller apperture -> solid=1.0
            N(i)=integral(@(E2)solid*N_deconv./E2.*exp(-E2./T_deconv),energy(i+1),energy(i));
            Nup(i)=integral(@(E2)solid*N_deconvup./E2.*exp(-E2./T_deconvup),energy(i+1),energy(i));
            Nlow(i)=integral(@(E2)solid*N_deconvlow./E2.*exp(-E2./T_deconvlow),energy(i+1),energy(i));
        end
end

Ntest = 1e10;
ENmax = 10;
while Ntest > 1e8
    ENmax = ENmax+0.1;
    Ntest = integral(@(E2)N_deconv./E2.*exp(-E2./T_deconv),ENmax-1,ENmax);
end

%we have integrated inversly, flip left-right for x and y vector
N=fliplr(N);
Nup=fliplr(Nup);
Nlow=fliplr(Nlow);
energy=fliplr(energy);

%for first energy-bin there is no integration => after integration you will
%loose one energy, cause now its binned
energy=energy(2:end);

hold all
plot(energy,N,'--o')
plot(energy,Nup,'--k')
plot(energy,Nlow,'--k')
if isfield(data.data, 'imgArrayNP') && ~isempty(data.data.imgArrayNP)
    plot(E+1, squeeze(sum(sum(data.data.imgArrayNP,1),2)), '--d','MarkerSize',10, 'LineWidth',2);
end

%for cut-off visualization
plot([energy(end), energy(end)],[1e-12*N(end),N(end)],'--r','LineWidth',2)

if strcmpi(data.fitparam.fitfunction,'transportfun')
    plot(energy,Nmain)
end

EvsNP_fitted = horzcat(energy',N');
EvsDOSE_measured = [E',dose'];

set(gca,'YScale','log')
xlabel('Proton energy (MeV)');
ylabel('proton number N per unit energy of 1 MeV');
legend('deconvolved fit', '95% confidence band', '95% confidence band', 'deconved particles');

hold off
axis([0 max(energy)+1 min(N)*0.01 2*max(N)]);

ll02=['N_0 = (',num2str(N_deconv*10^(-N_exp),'%5.2f'), ' ', char(177), ' ', num2str(N_deconvErr,'%5.2f'),') \cdot 10^{', num2str(N_exp,'%1.0f'),'}', ...
    ' ; ', 'kT = (',num2str(T_deconv,'%5.2f'), ' ', char(177), ' ', num2str(T_deconvErr,'%5.2f'), ') MeV'];
ll031=['E_{cut} = ',num2str(Emax_deconv,'%3.2f'),' MeV', ...
    ' ; ', 'E_{expected} = ', num2str(ENmax,'%3.2f'),' MeV',' (Energy bin with 1e8 p)'];

N(energy <= 4) = 0;
Nup(energy <= 4) = 0;
Nlow(energy <= 4) = 0;

total=sum(N.*energy); %MeV
totalup=sum(Nup.*energy); %MeV
totallow=sum(Nlow.*energy); %MeV

conv=total.*1e6.*1.6e-19/Elaser*100;
convup=totalup.*1e6.*1.6e-19/Elaser*100;
convlow=totallow.*1e6.*1.6e-19/Elaser*100;
convErr = ((conv-convlow)+(convup-conv))/2;

ll04=['Conversion efficiency (> 4 MeV; without aperture): (',num2str(conv,'%5.2f'), ' ', char(177), ' ', num2str(convErr,'%5.2f'),') %'];
ll10=['Intensity = ',num2str(I,'%5.2e'),' W/cm^2'];
annotation('textbox',[0.01 0.85 0.8 0.15],'string',{text;ll02;ll031;ll04;ll10},'LineStyle','none','FontWeight','bold');

set(gca,'OuterPosition',[0,-0.005,1,0.75]);
box on

% ============================================
% subfunction - jacobianest
% ============================================
function [jac,err] = jacobianest(fun,x0)
% gradest: estimate of the Jacobian matrix of a vector valued function of n variables
% usage: [jac,err] = jacobianest(fun,x0)
%
% 
% arguments: (input)
%  fun - (vector valued) analytical function to differentiate.
%        fun must be a function of the vector or array x0.
% 
%  x0  - vector location at which to differentiate fun
%        If x0 is an nxm array, then fun is assumed to be
%        a function of n*m variables.
%
%
% arguments: (output)
%  jac - array of first partial derivatives of fun.
%        Assuming that x0 is a vector of length p
%        and fun returns a vector of length n, then
%        jac will be an array of size (n,p)
%
%  err - vector of error estimates corresponding to
%        each partial derivative in jac.
%
%
% Example: (nonlinear least squares)
%  xdata = (0:.1:1)';
%  ydata = 1+2*exp(0.75*xdata);
%  fun = @(c) ((c(1)+c(2)*exp(c(3)*xdata)) - ydata).^2;
%
%  [jac,err] = jacobianest(fun,[1 1 1])
%
%  jac =
%           -2           -2            0
%      -2.1012      -2.3222     -0.23222
%      -2.2045      -2.6926     -0.53852
%      -2.3096      -3.1176     -0.93528
%      -2.4158      -3.6039      -1.4416
%      -2.5225      -4.1589      -2.0795
%       -2.629      -4.7904      -2.8742
%      -2.7343      -5.5063      -3.8544
%      -2.8374      -6.3147      -5.0518
%      -2.9369      -7.2237      -6.5013
%      -3.0314      -8.2403      -8.2403
%
%  err =
%   5.0134e-15   5.0134e-15            0
%   5.0134e-15            0   2.8211e-14
%   5.0134e-15   8.6834e-15   1.5804e-14
%            0     7.09e-15   3.8227e-13
%   5.0134e-15   5.0134e-15   7.5201e-15
%   5.0134e-15   1.0027e-14   2.9233e-14
%   5.0134e-15            0   6.0585e-13
%   5.0134e-15   1.0027e-14   7.2673e-13
%   5.0134e-15   1.0027e-14   3.0495e-13
%   5.0134e-15   1.0027e-14   3.1707e-14
%   5.0134e-15   2.0053e-14   1.4013e-12
%
%  (At [1 2 0.75], jac should be numerically zero)
%
%
% See also: derivest, gradient, gradest
%
%
% Author: John D'Errico
% e-mail: woodchips@rochester.rr.com
% Release: 1.0
% Release date: 3/6/2007

% get the length of x0 for the size of jac
nx = numel(x0);

MaxStep = 100;
StepRatio = 2.0000001;

% was a string supplied?
if ischar(fun)
  fun = str2func(fun);
end

% get fun at the center point
f0 = fun(x0);
f0 = f0(:);
n = length(f0);
if n==0
  % empty begets empty
  jac = zeros(0,nx);
  err = jac;
  return
end

relativedelta = MaxStep*StepRatio .^(0:-1:-25);
nsteps = length(relativedelta);

% total number of derivatives we will need to take
jac = zeros(n,nx);
err = jac;
for i = 1:nx
  x0_i = x0(i);
  if x0_i ~= 0
    delta = x0_i*relativedelta;
  else
    delta = relativedelta;
  end

  % evaluate at each step, centered around x0_i
  % difference to give a second order estimate
  fdel = zeros(n,nsteps);
  for j = 1:nsteps
      a=swapelement(x0,i,x0_i + delta(j));
      b=swapelement(x0,i,x0_i - delta(j));
    fdif = fun(a) - fun(b);
    
    fdel(:,j) = fdif(:);
  end
  
  % these are pure second order estimates of the
  % first derivative, for each trial delta.
  derest = fdel.*repmat(0.5 ./ delta,n,1);
  
  % The error term on these estimates has a second order
  % component, but also some 4th and 6th order terms in it.
  % Use Romberg exrapolation to improve the estimates to
  % 6th order, as well as to provide the error estimate.
  
  % loop here, as rombextrap coupled with the trimming
  % will get complicated otherwise.
  for j = 1:n
    [der_romb,errest] = rombextrap(StepRatio,derest(j,:),[2 4]);
    
    % trim off 3 estimates at each end of the scale
    nest = length(der_romb);
    trim = [1:3, nest+(-2:0)];
    [der_romb,tags] = sort(der_romb);
    der_romb(trim) = [];
    tags(trim) = [];
    
    errest = errest(tags);
    
    % now pick the estimate with the lowest predicted error
    [err(j,i),ind] = min(errest);
    jac(j,i) = der_romb(ind);
  end
end


% =======================================
%      sub-functions
% =======================================
function vec = swapelement(vec,ind,val)
% swaps val as element ind, into the vector vec
vec(ind) = val;

% ============================================
% subfunction - romberg extrapolation
% ============================================
function [der_romb,errest] = rombextrap(StepRatio,der_init,rombexpon)
% do romberg extrapolation for each estimate
%
%  StepRatio - Ratio decrease in step
%  der_init - initial derivative estimates
%  rombexpon - higher order terms to cancel using the romberg step
%
%  der_romb - derivative estimates returned
%  errest - error estimates
%  amp - noise amplification factor due to the romberg step

srinv = 1/StepRatio;

% do nothing if no romberg terms
nexpon = length(rombexpon);
rmat = ones(nexpon+2,nexpon+1);
% two romberg terms
rmat(2,2:3) = srinv.^rombexpon;
rmat(3,2:3) = srinv.^(2*rombexpon);
rmat(4,2:3) = srinv.^(3*rombexpon);

% qr factorization used for the extrapolation as well
% as the uncertainty estimates
[qromb,rromb] = qr(rmat,0);

% the noise amplification is further amplified by the Romberg step.
% amp = cond(rromb);

% this does the extrapolation to a zero step size.
ne = length(der_init);
rhs = vec2mat(der_init,nexpon+2,ne - (nexpon+2));
rombcoefs = rromb\(qromb'*rhs);
der_romb = rombcoefs(1,:)';

% uncertainty estimate of derivative prediction
s = sqrt(sum((rhs - rmat*rombcoefs).^2,1));
rinv = rromb\eye(nexpon+1);
cov1 = sum(rinv.^2,2); % 1 spare dof
errest = s'*12.7062047361747*sqrt(cov1(1));

% ============================================
% subfunction - vec2mat
% ============================================
function mat = vec2mat(vec,n,m)
% forms the matrix M, such that M(i,j) = vec(i+j-1)
[i,j] = ndgrid(1:n,0:m-1);
ind = i+j;
mat = vec(ind);
if n==1
  mat = mat';
end