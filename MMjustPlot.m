clear all
close all
data = getappdata(0,'data');
ImgArray = data.data.ImageArray;
EloArray = data.data.ELOarray;
rcfconf = data.parameters.rcfconfig;
szImg = size(ImgArray);

if length(szImg) == 2
    szImg = cat(2,szImg,1);
end
szElo = size(EloArray);
Es = 1e-3;
E = min(EloArray(:,1,1)):Es:max(EloArray(:,1,1));
szE = size(E);

if length(szElo) == 2
    szElo = cat(2,szElo,1);
end
eloInt = zeros(szE(2),szElo(3));
for i = 1:szElo(3)
    eloInt(:,i) = interp1(EloArray(:,1,i),EloArray(:,4,i),E);
end
for i = 1:szImg(3)
    [depEsin(i),maxi] = max(eloInt(:,i));
    Ercf(i) = E(maxi);
end

% Convert Pixel to mm
[imgX,imgY] = meshgrid(1:szImg(1),1:szImg(2));
imgX = imgX' * 52/2241;
imgY = imgY' * 52/2241;

range = 4;
r = range;

for i = 1:szImg(3)
    figure(i)
    myCMap = colormap(flipud(gray));
%     myCMap = colormap(jet);
%     myCMap(1,:) = [1 1 1];
%     myCMap = [1 1 1;0.857142865657806 0.857142865657806 1;0.714285731315613 0.714285731315613 1;0.571428596973419 0.571428596973419 1;0.428571432828903 0.428571432828903 1;0.28571429848671 0.28571429848671 1;0.142857149243355 0.142857149243355 1;0 0 1;0 0.0625 1;0 0.125 1;0 0.1875 1;0 0.25 1;0 0.3125 1;0 0.375 1;0 0.4375 1;0 0.5 1;0 0.5625 1;0 0.625 1;0 0.6875 1;0 0.75 1;0 0.8125 1;0 0.875 1;0 0.9375 1;0 1 1;0.0625 1 0.9375;0.125 1 0.875;0.1875 1 0.8125;0.25 1 0.75;0.3125 1 0.6875;0.375 1 0.625;0.4375 1 0.5625;0.5 1 0.5;0.5625 1 0.4375;0.625 1 0.375;0.6875 1 0.3125;0.75 1 0.25;0.8125 1 0.1875;0.875 1 0.125;0.9375 1 0.0625;1 1 0;1 0.9375 0;1 0.875 0;1 0.8125 0;1 0.75 0;1 0.6875 0;1 0.625 0;1 0.5625 0;1 0.5 0;1 0.4375 0;1 0.375 0;1 0.3125 0;1 0.25 0;1 0.1875 0;1 0.125 0;1 0.0625 0;1 0 0;0.9375 0 0;0.875 0 0;0.8125 0 0;0.75 0 0;0.6875 0 0;0.625 0 0;0.5625 0 0;0.5 0 0];
    colormap(myCMap);
    hold off
    
%     Vq = interp2(ImgArray(:,:,i),3);
    
    surf(imgX,imgY,ImgArray(:,:,i),'LineStyle','none');
%     surf(imgXq,imgYq,Vq,'LineStyle','none');
    hold on
    view(0,90)

    cImg = ImgArray(:,:,i);
    [maxcImg,maxidx] = max(cImg(:));
    
    x0 = imgX(maxidx);
    y0 = imgY(maxidx);
    
%     xlim([x0-r,x0+r])
%     ylim([y0-r,y0+r])
%     caxis([0 max(max(max(ImgArray)))])
%     caxis([0 4000])
    
    xlim([min(min(imgX)) max(max(imgX))])
    ylim([min(min(imgY)) max(max(imgY))])
    xlabel('x(mm)')
    ylabel('y(mm)')
    
    title(strcat(rcfconf(i),{' '},num2str(Ercf(i)), '(MeV)'))
    
    cb = colorbar;
    cb.Label.String = 'Deposited E(MeV)';
end