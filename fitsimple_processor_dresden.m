function [simple_estimates, model, spectrum, output] = fitsimple_processor_dresden(data)

% get data
ELOarray = data.data.ELOarray;
last = size(data.data.ImageArray,3);
 
E = zeros(last,1);
E_error = zeros(last,1);
spectrum = zeros(last,2);   %p+ number vs. energy
dep_E = zeros(last,1);      %deposit energy
peak_pos = zeros(last,1);   %index of position of the highest stopping power for each RCF film
singleELO = zeros(last,1);  %stopping power at the peak

%step 1: calculate deposit energy%
for frame = 1:last
    dose = sum(sum(data.data.ImageArray(:,:,frame)));   %ImageArray ist MeV/px, dose ist MeV
    dep_E(frame) = dose;
    [a, peaks_position] = max(ELOarray(:,4,frame));
    peak_pos(frame) = peaks_position;
    singleELO_stop = sum(ELOarray(peaks_position,4,frame));
    singleELO(frame) = singleELO_stop;
    E(frame) = mean(ELOarray(peaks_position,1,frame));
end
%calculate average deposited energy of one proton in each sensitiv layer
ELO_single = mean(singleELO); 
%Annahmen jede Schicht ist gleich, also ist der Mittelwert aller Stopping
%energien sinnvoll
%alternativ: ELO_single = 0.6 %[MeV] results from SRIM data assumung stopping of
%energies up to 1.1-1.2 MeV
%maximum absch�tzung: ELO_single = 1 [MeV]

number = dep_E/ELO_single;  %first estimate for the number of protons if all energy originates from them.

%step 2: deconvolution
steps = 30;                 %number of deconvolution steps (steps has to be larger the RCF film)
calc_dep_E = zeros(last,steps+1);   %calculated deposit energy based on particle spectra
corr_number = zeros(last,steps+1);  %corrected number
calc_dep_E(:,1) = dep_E;            %initial value from simple estimate
corr_number(:,1) = number;
for st = 2:steps+1
    dep_highp = zeros(last,1);  %deposit energy from higher layer
    for frame = last:-1:1       %starting at the highest layers
        E_lost_layers = squeeze(ELOarray(peak_pos(frame),4,:)); %get energy lost in the sensitive layer for faster protons
        for ii = 1:frame-1
            dep_highp(ii) =  dep_highp(ii) + E_lost_layers(ii)*corr_number(frame,st-1); %add deposite energy from faster protons
        end
    end
    calc_dep_E(:,st) = corr_number(:,st-1).*mean(singleELO)+dep_highp;  %calculate deposit energy based on current particle estimation
    corr_number(:,st) = corr_number(:,st-1)-(calc_dep_E(:,st)-calc_dep_E(:,1))./singleELO;  %correct number based on current particle estimation ->loop
end

figure()
ax1 = subplot(2,1,1);
plot(E,dep_E,'+');
hold(ax1, 'on');
plot(E,calc_dep_E(:,2),'*');
plot(E,calc_dep_E(:,steps+1),'x');
set(gca,'YScale','log');
ylabel('deposit Energy (MeV)');
xlabel('Energy (MeV)');
legend('value from RCF', 'deposit energy from initial particle number', 'Deposit energy from corrected particle number')

%convert to /MeV:
%idee wir normieren auf einen Energiebin von 0,1 MeV (Das ist die stopping
%power des sensitiven Layers). Dazu muss ausgerechnet werden, weilchen Anteil
%die gestoppten Teilchen an alle, die durch den Layer durchfliegen haben.

%1. Teilchenzahl einigerma�en konstant:
spectrum_part = zeros(last,1);
energy_bin = 0.1;    %MeV energy width sensitiv layer (is in reality energy dependent as well)

%globale Exp Fit Funktion:
[model, text] = model_handler(data.fitparam.fitfunction);
options = optimset('Display', 'none', ...
                   'MaxFunEvals', data.fitparam.maxeval, ...
                   'TolX', 1e-10, ...
                   'TolFun', 1e-10, ...
                   'MaxIter', data.fitparam.maxiter);
[simple_estimates, ~, ~, output] = fminsearch(@(x) model(x, {E, log(corr_number(:,steps)), data.fitparam}), ...
    [1000000000 data.fitparam.T_start], options);
%plot(E,simple_estimates(1)*exp(-E/simple_estimates(2)),'-');


% Handle response, pick method of particle spectrum
choice2 = questdlg('Spectral assumption for MeV binning:',...
    'question',...
    'global_exponential','linear','None','Cancel');
switch choice2
    case 'global_exponential'
        fit = 'global';
    case 'linear'
        fit = 'linear';
    case 'None'
        fit = '';
end 
%different fit doesn't change very much!
%however should be changed for exponential spectra

for frame = 1:last
    %find index range to next sensitiv layer:
    if frame == last
        ind_range = peak_pos(frame):2*peak_pos(frame)-peak_pos(frame-1);    %set arb. index range for the last sensitive layer 
        a0 = corr_number(frame-1,steps);    %parameter 1 for weighting
        T = (corr_number(frame-1,steps)-corr_number(frame,steps))/(E(frame-1)-E(frame)); %parameter 1 for weighting
    else
        ind_range = peak_pos(frame):peak_pos(frame+1);
        %get weighting:
        a0 = corr_number(frame,steps);
        T = (corr_number(frame,steps)-corr_number(frame+1,steps))/(E(frame)-E(frame+1));
    end
    if strcmp(fit,'global')
        %weighting set by global exponential shaped function
        y_frame = simple_estimates(1)*exp(-E(frame)/simple_estimates(2));
        weight = simple_estimates(1)*exp(-ELOarray(ind_range,1,frame)/simple_estimates(2))/y_frame;
    elseif strcmp(fit,'linear')
        %linear weighting between the data points
        weight = 1+1/a0*T.*(ELOarray(ind_range,1,frame)-E(frame));
    else
        %particle number is the same everywhere
        weight = ones(size(frame));
    end
    total_dep_E = sum(ELOarray(ind_range,4,frame).*weight); %total deposit energy for the assumed energy spectra
    %total_dep_E = sum(ELOarray(ind_range,4,frame));
    proportion = ELO_single/total_dep_E;    %portion of deposite energy for peaked protons
    spectrum_part(frame) = corr_number(frame,steps+1)*proportion/energy_bin; %is this really correct?
end;

spectrum_corr(:,2) = spectrum_part;
%save as spectrum_corr
spectrum_corr(:,1) = E;

for frame = 1:last
   dose = sum(sum(data.data.ImageArray(:,:,frame)));        %ImageArray ist MeV/px, dose ist MeV
   %peaks_position = peaks(ELOarray(:,4,frame));
   [a, peaks_position] = max(ELOarray(:,4,frame));
   hold all
   %plot(ELOarray(:,1,frame), ELOarray(:,4,frame), '-', ...
   %    ELOarray(peaks_position,1,frame), ELOarray(peaks_position,4,frame), '+')
   %xlabel('E (MeV)')
   %ylabel('\Delta E/E (MeV)')
   %title('Check for proper peak auto-detection');
   %box on
   number = dose/sum(ELOarray(peaks_position,4,frame));
   E(frame) = mean(ELOarray(peaks_position,1,frame));
   E_error(frame) = max(abs(ELOarray(peaks_position,1,frame)-E(frame)));
   spectrum(frame,1) = E(frame);
   spectrum(frame,2) = number;
end
hold off

xdata = spectrum(:,1);
ydata = spectrum(:,2);

%%mising last forward calculation form the final spectrum.
%Output: Proton spectrum (particle number simple, deconvoled) and deposit energy diagram (from RCF, forward from final spectrum)

[model, text] = model_handler(data.fitparam.fitfunction);
options = optimset('Display', 'none', ...
                   'MaxFunEvals', data.fitparam.maxeval, ...
                   'TolX', 1e-10, ...
                   'TolFun', 1e-10, ...
                   'MaxIter', data.fitparam.maxiter);
[simple_estimates, ~, ~, output] = fminsearch(@(x) model(x, {xdata, log(ydata), data.fitparam}), ...
    [data.fitparam.N_start data.fitparam.T_start], options);

% calculate conversion efficiency for protons > 4 MeV (threshold as in published papers)
%summarize over all RCFs
ax2 = subplot(2,1,2);
hold all
x_fit = ceil(min(xdata)):0.1:ceil(max(xdata));
[~, FittedCurve] = model(simple_estimates, {x_fit, 0, data.fitparam});
plot(x_fit, exp(FittedCurve),'--k')
plot(xdata,ydata,'o')
%daten aus R�ckgerechneten Spektrum:
plot(spectrum_corr(:,1),spectrum_corr(:,2),'o')
set(gca,'YScale','log')
xlabel('E (MeV)');
ylabel('dN/dE (1 MeV^{-1})');
legend('exp. fit of initial spectrum', 'initial particle number', 'deconvoled particle number')
box on
%save the spectrum as textfile
% Construct a questdlg with three options
choice = questdlg('Do you want to save the spectrum?');
% Handle response
switch choice
    case 'Yes'
        %M = magic(3);
        [file, path] = uiputfile('*.xls','Save as');
        xlswrite([path file],spectrum_corr,'Tab','A1');
        %fid=fopen(fullfile(path,file), 'wt');
        %fprintf(fid,'%s\t%s\n','E','dN/dE (1/MeV)');
        %dlmwrite('fid',M,'delimiter','\t','-append')
        %fprintf(fid,'%f\t%f\n',xdata,ydata);
        %fclose(fid);
        %msgbox('done','Attention','none');
end 