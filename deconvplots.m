function deconvplots(varargin)
% Nice 3D Plots ;)
% Hollow Beam Experiment
% 07.09.2012 CB
% 11.04.2013 CB complete rework with definebeam_gui.m
% 19.04.2013 CB added imgArrayNP

data = getappdata(0, 'data');
statustext = varargin{1};

if ~isfield(data.data, 'ImageDustMask') || isempty(data.data.ImageDustMask)
    data.data.ImageDustMask = false(size(data.data.ImageMask));
    set(statustext,'String', 'old files, assuming no ImageDustMask');
    pause(1)
end

set(statustext,'String', 'Reconstruction: Calculating energy deposition curves ... ');
pause(0.1)

indexELOarray = size(data.data.ELOarray,3);
averageE = zeros(indexELOarray,2);
for mm = 1:indexELOarray
    %E = data.data.ELOarray(:,1,mm);
    %Eloss = data.data.ELOarray(:,2,mm);
    %beforemaxtab = peaksrecon(E, Eloss);
    [value, peaks_position] = max(data.data.ELOarray(:,4,mm));
    %clear E Eloss;
    %averageE(mm,1) = beforemaxtab(1,1)
    %averageE(mm,2) = beforemaxtab(1,2)
    averageE(mm,1) = data.data.ELOarray(peaks_position,1,mm);
    averageE(mm,2) = value;
end

set(statustext,'String', 'Reconstruction finished. Now PLOTS.');
pause(0.1)

% % % % 3-D-Plot of the laser-accelerated proton beam:
pixel_to_mm = 25.4/data.data.fileinfo(1).XResolution; %mm

Eintervals = (averageE(2:end,1)-averageE(1:end-1,1))*1000; %KeV
meaninterval = round(mean(Eintervals)/10)*10;

if isfield(data.data, 'imgArrayNP')
    % NP per 1 MeV intervall, per pixel
    Allfilms = squeeze(data.data.imgArrayNP);

    % define cutimage
%     [cutimagex, cutimagey] = deal(zeros(size(Allfilms,2),size(Allfilms,3)));
    cutimagey = zeros(size(Allfilms,2),size(Allfilms,3));
    cutimagex = zeros(size(Allfilms,1),size(Allfilms,3));
    dx = 10; %row/col thickness for integrating/lineaveraging

    if ~isfield(data, 'mdata') || isempty(data.mdata)
        % do init here; same as definebeam_gui.m
        len2 = size(data.data.ImageArray,3);
        data.mdata.x0 = zeros(len2,1); % hand set
        data.mdata.y0 = zeros(len2,1);
        data.mdata.x_pos = zeros(len2,1); % centroid
        data.mdata.y_pos = zeros(len2,1);
        data.mdata.angle = 0;
        data.mdata.angledeg = 0;

        % find all centroid
        level = 0.01;
        for frame = 1:size(data.data.ImageArray,3)
            ImageArray2 = data.data.ImageArray(:,:,frame);
            % calculate center of beam
            % convert to black and white
            BW = im2bw(ImageArray2,level);
            % get WeightedCentroid for Image in boundary ..
            if size(ImageArray2,3) == 3 % fix for RBG images
                BW = repmat(BW, [1 1 3]);
            end
            stats = regionprops(BW, ImageArray2, 'WeightedCentroid');
            clear BW;
            data.mdata.x_pos(frame) = stats(1).WeightedCentroid(1);
            data.mdata.y_pos(frame) = stats(1).WeightedCentroid(2);
        end

        % and set it
        setappdata(0, 'data', data);
    end

    if (data.mdata.angledeg ~= 0)
        % angle is set -> rotate! a) about centroid b) about hand set point
        set(statustext,'String', 'Rotating & Translating Images!');
        pause(0.1)
        for framei = 1:size(Allfilms,3);
            % shift to center % pad it up
            pad = round(size(Allfilms(:,:,framei))/5);
            if data.mdata.x0(framei) ~= 0 && data.mdata.y0(framei) ~= 0
                xt = round(data.mdata.x0(framei)-size(Allfilms(:,:,framei),1)/2);
                yt = round(data.mdata.y0(framei)-size(Allfilms(:,:,framei),2)/2);
            else
                xt = round(data.mdata.x_pos(framei)-size(Allfilms(:,:,framei),1)/2);
                yt = round(data.mdata.y_pos(framei)-size(Allfilms(:,:,framei),2)/2);
            end
            se = translate(strel(1), -[yt xt]);
            tmp = padarray(Allfilms(:,:,framei), pad);
            tmp = imdilate(tmp,se);
            % rotate
            tmp = imrotate(tmp, -data.mdata.angledeg, 'bilinear', 'crop');
            tmp(isnan(tmp)) = 0;
            % shift back
            se = translate(strel(1), [yt xt]);
            tmp = imdilate(tmp,se);
            Allfilms(:,:,framei) = tmp(pad(1)+1:end-pad(1), pad(2)+1:end-pad(2));
            clear tmp;
        end
        Allfilms(isinf(Allfilms)) = 0;
    end

    x_cog = zeros(size(cutimagex,2),1);
    y_cog = zeros(size(cutimagey,2),1);

    % now do the lineouts
    for framei = 1:size(Allfilms,3)
        if data.mdata.x0(framei) ~= 0 && data.mdata.y0(framei) ~= 0
            % position is set
            set(statustext,'String', 'Using beam center defined!');
            cutimagey(:,framei) = mean(Allfilms(round(data.mdata.y0(framei)-dx/2):round(data.mdata.y0(framei)+dx/2),:,framei),1);
            cutimagex(:,framei) = mean(Allfilms(:,round(data.mdata.x0(framei)-dx/2):round(data.mdata.x0(framei)+dx/2),framei),2);
            x_cog(framei) = data.mdata.x0(framei);
            y_cog(framei) = data.mdata.y0(framei);
        elseif data.mdata.x0(framei) == 0 && data.mdata.y0(framei) == 0
            % position is not set
            set(statustext,'String', 'Using beam centroid!');
            cutimagey(:,framei) = mean(Allfilms(round(data.mdata.y_pos(framei)-dx/2):round(data.mdata.y_pos(framei)+dx/2),:,framei),1);
            cutimagex(:,framei) = mean(Allfilms(:,round(data.mdata.x_pos(framei)-dx/2):round(data.mdata.x_pos(framei)+dx/2),framei),2);
            x_cog(framei) = data.mdata.x_pos(framei);
            y_cog(framei) = data.mdata.y_pos(framei);
        else
            % woot
            set(statustext,'String', ['Error in ' num2str(framei) '!'], 'ForegroundColor', 'red');
        end
    end
    clear Allfilms;

    % set to NaN for nicer image
    % and log scale it ...
    cutimagex = log(cutimagex);
    cutimagey = log(cutimagey);
    % cutimagex(cutimagex <= 1) = NaN;
    % cutimagey(cutimagey <= 1) = NaN;
    cutimagex(cutimagex <= 1) = 0;
    cutimagey(cutimagey <= 1) = 0;
%     cutimagex(isinf(cutimagex)) = 0;
%     cutimagey(isinf(cutimagey)) = 0;
%     cutimagex = abs(cutimagex);
%     cutimagey = abs(cutimagey);

    klx = (1:size(cutimagex,1))-round(size(cutimagex,1)/2);
    kly = (1:size(cutimagey,1))-round(size(cutimagey,1)/2);
    anglesx = atan(klx*pixel_to_mm / data.parameters.target.distance)*360/(2*pi);
    anglesy = atan(kly*pixel_to_mm / data.parameters.target.distance)*360/(2*pi);
    xa0  = atan((x_cog-max(klx))*pixel_to_mm / data.parameters.target.distance)*360/(2*pi);
    ya0  = atan((y_cog-max(kly))*pixel_to_mm / data.parameters.target.distance)*360/(2*pi);
    clear klx kly x_cog y_cog;
    
    % x
    figure();
    surf(averageE(:,1),anglesx, cutimagex,'FaceColor','interp',...
        'EdgeColor','none', 'FaceLighting','phong');
    axis tight;
    view(90,90);
    box on;
    xlabel('Proton energy (MeV)');
    ylabel('Envelope-divergence (�)');
    if isfield(data, 'mdata') && ~isempty(data.mdata)
        title(['X (' num2str(data.mdata.angledeg,'%3.2f') ' deg ) Ln proton number N per unit Energy 1 MeV per px^2']);
    else
        title('X Ln proton number N per unit Energy 1 MeV per px^2');
    end
    colormap(jet(2^10));
    colorbar;
    caxis([max(cutimagex(:))/2 max(cutimagex(:))]);
    zlim([max(cutimagex(:))/2 max(cutimagex(:))]);
    hold on
    plot3(averageE(:,1), ya0, max(cutimagex), 'black', 'linewidth', 2);
    hold off

    % y
    figure();
    surf(averageE(:,1),anglesy', cutimagey,'FaceColor','interp',...
        'EdgeColor','none', 'FaceLighting','phong');
    axis tight;
    view(90,90);
    box on;
    xlabel('Proton energy (MeV)');
    ylabel('Envelope-divergence (�)');
    if isfield(data, 'mdata') && ~isempty(data.mdata)
        title(['Y (' num2str(data.mdata.angledeg,'%3.2f') '+90 deg ) Ln proton number N per unit Energy 1 MeV per px^2']);
    else
        title('Y Ln proton number N per unit Energy 1 MeV per px^2');
    end
    colormap(jet(2^10));
    colorbar;
    caxis([max(cutimagey(:))/2 max(cutimagey(:))]);
    zlim([max(cutimagey(:))/2 max(cutimagey(:))]);
    hold on
    plot3(averageE(:,1), xa0, max(cutimagey), 'black', 'linewidth', 2);
    hold off
else
    Allfilms = squeeze(data.data.ImageArray);

    % define cutimage
%     [cutimagex, cutimagey] = deal(zeros(size(Allfilms,2),size(Allfilms,3)));
    cutimagey = zeros(size(Allfilms,2),size(Allfilms,3));
    cutimagex = zeros(size(Allfilms,1),size(Allfilms,3));
    dx = 10; %row/col thickness for integrating/lineaveraging

    if ~isfield(data, 'mdata') || isempty(data.mdata)
        % do init here; same as definebeam_gui.m
        len2 = size(data.data.ImageArray,3);
        data.mdata.x0 = zeros(len2,1); % hand set
        data.mdata.y0 = zeros(len2,1);
        data.mdata.x_pos = zeros(len2,1); % centroid
        data.mdata.y_pos = zeros(len2,1);
        data.mdata.angle = 0;
        data.mdata.angledeg = 0;

        % find all centroid
        for frame = 1:size(data.data.ImageArray,3)
            ImageArray2 = data.data.ImageArray(:,:,frame);
            % calculate center of beam
            % convert to black and white
            BW = im2bw(ImageArray2);
            % get WeightedCentroid for Image in boundary ..
            stats = regionprops(BW, ImageArray2, 'Area', 'WeightedCentroid');
            area = [stats.Area];
            [~, largestBlobIndex] = max(area);
            wgtc = stats(largestBlobIndex).WeightedCentroid;
            clear BW area;
            data.mdata.x_pos(frame) = wgtc(1);
            data.mdata.y_pos(frame) = wgtc(2);
        end

        % and set it
        setappdata(0, 'data', data);
    end

    if (data.mdata.angledeg ~= 0)
        % angle is set -> rotate! a) about centroid b) about hand set point
        set(statustext,'String', 'Rotating & Translating Images!');
        pause(0.1)
        for framei = 1:size(Allfilms,3);
            % shift to center % pad it up
            pad = round(size(Allfilms(:,:,framei))/5);
            if data.mdata.x0(framei) ~= 0 && data.mdata.y0(framei) ~= 0
                xt = round(data.mdata.x0(framei)-size(Allfilms(:,:,framei),1)/2);
                yt = round(data.mdata.y0(framei)-size(Allfilms(:,:,framei),2)/2);
            else
                xt = round(data.mdata.x_pos(framei)-size(Allfilms(:,:,framei),1)/2);
                yt = round(data.mdata.y_pos(framei)-size(Allfilms(:,:,framei),2)/2);
            end
            se = translate(strel(1), -[yt xt]);
            tmp = padarray(Allfilms(:,:,framei), pad);
            tmp = imdilate(tmp,se);
            % rotate
            tmp = imrotate(tmp, -data.mdata.angledeg, 'bilinear', 'crop');
            tmp(isnan(tmp)) = 0;
            % shift back
            se = translate(strel(1), [yt xt]);
            tmp = imdilate(tmp,se);
            Allfilms(:,:,framei) = tmp(pad(1)+1:end-pad(1), pad(2)+1:end-pad(2));
            clear tmp;
        end
        Allfilms(isinf(Allfilms)) = 0;
    end

    x_cog = zeros(size(cutimagex,2),1);
    y_cog = zeros(size(cutimagey,2),1);

    % now do the lineouts
    for framei = 1:size(Allfilms,3)
        if data.mdata.x0(framei) ~= 0 && data.mdata.y0(framei) ~= 0
            % position is set
            set(statustext,'String', 'Using beam center defined!');
            cutimagey(:,framei) = mean(Allfilms(round(data.mdata.y0(framei)-dx/2):round(data.mdata.y0(framei)+dx/2),:,framei),1);
            cutimagex(:,framei) = mean(Allfilms(:,round(data.mdata.x0(framei)-dx/2):round(data.mdata.x0(framei)+dx/2),framei),2);
            x_cog(framei) = data.mdata.x0(framei);
            y_cog(framei) = data.mdata.y0(framei);
        elseif data.mdata.x0(framei) == 0 && data.mdata.y0(framei) == 0
            % position is not set
            set(statustext,'String', 'Using beam centroid!');
            cutimagey(:,framei) = mean(Allfilms(round(data.mdata.y_pos(framei)-dx/2):round(data.mdata.y_pos(framei)+dx/2),:,framei),1);
            cutimagex(:,framei) = mean(Allfilms(:,round(data.mdata.x_pos(framei)-dx/2):round(data.mdata.x_pos(framei)+dx/2),framei),2);
            x_cog(framei) = data.mdata.x_pos(framei);
            y_cog(framei) = data.mdata.y_pos(framei);
        else
            % woot
            set(statustext,'String', ['Error in ' num2str(framei) '!'], 'ForegroundColor', 'red');
        end
    end
    clear Allfilms;

    % set to NaN for nicer image
    % and log scale it ...
    cutimagex = log(cutimagex);
    cutimagey = log(cutimagey);
    % cutimagex(cutimagex <= 1) = NaN;
    % cutimagey(cutimagey <= 1) = NaN;
    cutimagex(cutimagex <= 1) = 0;
    cutimagey(cutimagey <= 1) = 0;

    klx = (1:size(cutimagex,1))-round(size(cutimagex,1)/2);
    kly = (1:size(cutimagey,1))-round(size(cutimagey,1)/2);
    anglesx = atan(klx*pixel_to_mm / data.parameters.target.distance)*360/(2*pi);
    anglesy = atan(kly*pixel_to_mm / data.parameters.target.distance)*360/(2*pi);
    xa0  = atan((x_cog-max(klx))*pixel_to_mm / data.parameters.target.distance)*360/(2*pi);
    ya0  = atan((y_cog-max(kly))*pixel_to_mm / data.parameters.target.distance)*360/(2*pi);
    clear klx kly x_cog y_cog;

    % x
    figure();
    surf(averageE(:,1),anglesx,(cutimagex-log(pixel_to_mm^2)),'FaceColor','interp',...
        'EdgeColor','none', 'FaceLighting','phong');
    % daspect([5 15 2]);
    axis tight;
    view(90,90);
    box on;
    xlabel('Proton energy (MeV)');
    ylabel('Envelope-divergence (�)');
    if isfield(data, 'mdata') && ~isempty(data.mdata)
        title(['X (' num2str(data.mdata.angledeg,'%3.2f') ' deg ) Ln description of the proton particle spectrum per (' num2str(meaninterval,'%5.0f') ' keV*mm^2)']);
    else
        title(['X Ln description of the proton particle spectrum per (' num2str(meaninterval,'%5.0f') ' keV*mm^2)']);
    end
    colormap(jet(2^10));
    colorbar;
    caxis([max(cutimagex(:)-log(pixel_to_mm^2))/2 max(cutimagex(:)-log(pixel_to_mm^2))]);
    zlim([max(cutimagex(:)-log(pixel_to_mm^2))/2 max(cutimagex(:)-log(pixel_to_mm^2))]);
    hold on
    plot3(averageE(:,1), ya0, max((cutimagex-log(pixel_to_mm^2))), 'black', 'linewidth', 2);
    hold off

    % y
    figure();
    surf(averageE(:,1),anglesy',(cutimagey-log(pixel_to_mm^2)),'FaceColor','interp',...
        'EdgeColor','none', 'FaceLighting','phong');
    % daspect([5 15 2]);
    axis tight;
    view(90,90);
    box on;
    xlabel('Proton energy (MeV)');
    ylabel('Envelope-divergence (�)');
    if isfield(data, 'mdata') && ~isempty(data.mdata)
        title(['Y (' num2str(data.mdata.angledeg,'%3.2f') '+90 deg ) Ln description of the proton particle spectrum per (' num2str(meaninterval,'%5.0f') ' keV*mm^2)']);
    else
        title(['Y Ln description of the proton particle spectrum per (' num2str(meaninterval,'%5.0f') ' keV*mm^2)']);
    end
    colormap(jet(2^10));
    colorbar;
    caxis([max(cutimagey(:)-log(pixel_to_mm^2))/2 max(cutimagey(:)-log(pixel_to_mm^2))]);
    zlim([max(cutimagey(:)-log(pixel_to_mm^2))/2 max(cutimagey(:)-log(pixel_to_mm^2))]);
    hold on
    plot3(averageE(:,1), xa0, max((cutimagey-log(pixel_to_mm^2))), 'black', 'linewidth', 2);
    hold off
end
end