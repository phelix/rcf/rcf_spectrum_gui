function [ImageArray, fail, npixels, stderror] = counts2OD_microtek_lamp0RGB(ImageArray, rcftype)

%Microtek Artix Scan RGB images

fail = 0;
npixels = 0; %percentage of pixels saturated

if ~(size(ImageArray,3) == 3) % not RGB
    fail = 3;
else
    % 65535 must be dark for calibration
    ImageArray = imcomplement(ImageArray);
    if ~isempty(ImageArray(ImageArray > 65535))
        npixels = sum(ImageArray(ImageArray > 65535))/sum(sum(ImageArray));
        fail = 1;
    end
    % Background correction! - blank film
    % cat(dim, R, G, B)
    switch(rcftype)
        case 'H2'
            clean = cat(3, 7358, 5414, 27855);
            stderror = [1437 499 1176];
        case 'HD'
            clean = cat(3, 380, 67, 253);
            stderror = [47 77 63];
        case 'E3'
            clean = cat(3, 24665, 26272, 47978);
            stderror = [865 292 554];
        case 'M2'
            clean = cat(3, 9776, 9194, 11620);
            stderror = [698 448 1353];
        otherwise
            fail = 4;
            clean = cat(3, 0, 0, 0);
            stderror = [0 0 0];
    end
    if isa(ImageArray, 'uint16')
        bg = uint16(clean);
    else
        bg = clean;
    end
    ImageArray = bsxfun(@minus, ImageArray, bg);
end