function [model_hand, text] = model_handler(fitfun)
% global function for model selection
% insert model as case and below as function !
% define output text with variables for later print

switch(char(fitfun))
    case 'expfun' % plotdivergence.m; model 1
        model_hand = @expfun;
        text = 'angle(E) = %5.2f + %5.2f * E + %5.2f * E^2';
    case 'expfun2' % plotdivergence.m; model 1
        model_hand = @expfun2;
        text = 'angle(E) = %5.2f + %5.2f * E + %5.2f * E^2';
    case 'parfun' % plotdivergence.m; model 2
        model_hand = @parfun;
        text = 'angle(E) = %5.2f * sqrt(-2 * log(E/%5.2f))  * (E/%5.2f)^{1/%5.2f}';
    case {'exponential', 'transportfun'} % fitfunctions
        model_hand = @expfun_fit;
        text = 'exp. fit: dN/dE = N_0/E * exp(-E/kT)';
    case {'exponential_func', 'transportfun_func'}
        model_hand = @expfun_func;
        text = ''; 
    case {'exponential2_func'}
        model_hand = @expfun2_func;
        text = '';  
    case {'nature_func'}
        model_hand = @naturefun_func;
        text = '';
    case {'exponential_with_solidangle_func'}
        model_hand = @expsolidfun_func;
        text = '';  
    case 'exponential2' % fitfunctions
        model_hand = @expfun2_fit;
        text = 'exp. fit: dN/dE = N_0/E * exp(-E^2/kT^2)';
    case 'nature' % fitfunctions
        model_hand = @naturefun_fit;
        text = 'fit with dN/dE=N_0/sqrt(2*E*T_{hot})*exp(-sqrt(2*E/T_{hot}))';
    case 'exponential_with_solidangle' % fitfunctions
        model_hand = @expsolidfun_fit;
        text = 'fit with dN/dE=solid(E)*N_0/sqrt(2*E*T_{hot})*exp(-sqrt(2*E/T_{hot}))';
    case {'exponential_dose', 'transportfun_dose'} % fitfunctions deconv
        model_hand = @expfun_dose;
        text = 'fit with dN/dE=N_0/E*exp(-(E/T_{hot})), weighted with energy deposition';
    case 'exponential2_dose' % fitfunctions deconv
        model_hand = @expfun2_dose;
        text = 'fit with dN/dE=N_0/E*exp(-(E^2/T_{hot^2})), weighted with energy deposition';
    case 'exponentialn_dose' % fitfunctions deconv
        model_hand = @expfunn_dose;
        text = 'fit with dN/dE=N_0/E*exp(-(E^n/T_{hot}^n)), weighted with energy deposition';
    case 'nature_dose' % fitfunctions deconv
        model_hand = @naturefun_dose;
        text = 'fit with dN/dE=N_0/sqrt(2*E*T_{hot})*exp(-sqrt(2*E/T_{hot})), weighted with energy deposition';
    case 'exponential_with_solidangle_dose' % fitfunctions deconv
        model_hand = @expsolidfun_dose;
        text = 'fit with dN/dE=N_0/E*exp(-(E/T_{hot})), weighted with energy deposition';
    otherwise
        model_hand = NaN;
        text = 'Error! Unknown model!';
end
end

function [sse, FittedCurve] = expfun(params, datas)
    xdata = datas{1};
    ydata = datas{2};
    P1 = params(1);
    P2 = params(2);
    P3 = params(3);
    FittedCurve = P1 + P2 * xdata + P3 * xdata.^2;
    ErrorVector = FittedCurve - ydata;
    sse = sum(ErrorVector .^ 2);
end

function [sse, FittedCurve] = expfun2(params, datas)
    xdata = datas{1};
    ydata = datas{2};
    P1 = params(1);
    P2 = params(2);
    P3 = params(3);
    FittedCurve = P1 + P2 * xdata;
    ErrorVector = FittedCurve - ydata;
    sse = sum(ErrorVector .^ 2);
end

function [sse, FittedCurve] = parfun(params, datas)
    xdata = datas{1};
    ydata = datas{2};
    P1 = params(1);
    P2 = params(2);
    P3 = params(3);
    FittedCurve = P1 * sqrt(-2*log(xdata/P2)).*(xdata/P2).^(1/P3);
    ErrorVector = FittedCurve - ydata;
    sse = sum(ErrorVector .^ 2);
end

function [sse, FittedCurve] = expfun_fit(params, datas)
    xdata = datas{1};
    ydata = datas{2};
    P1 = params(1);
    P2 = params(2);
    FittedCurve = log(P1./xdata.*exp(-xdata./P2));
    ErrorVector = FittedCurve - ydata;
    sse = sum(ErrorVector .^ 2);
end

function [sse, FittedCurve] = expfun2_fit(params, datas)
	xdata = datas{1};
    ydata = datas{2};
    P1 = params(1);
    P2 = params(2);
    FittedCurve = log(P1./xdata.*exp(-xdata.^2/P2^2));
    ErrorVector = FittedCurve - ydata;
    sse = sum(ErrorVector .^ 2);
end

function [sse, FittedCurve] = naturefun_fit(params, datas)
    xdata = datas{1};
    ydata = datas{2};
    ydata=log(ydata);
    P1 = params(1);
    P2 = params(2);
    FittedCurve = log(P1./sqrt(2.*xdata.*P2).*exp(-sqrt(2.*xdata/P2)));
    ErrorVector = FittedCurve - ydata;
    sse = sum(ErrorVector .^ 2);
end

function [sse, FittedCurve] = expsolidfun_fit(params, datas)
    xdata = datas{1};
    ydata = datas{2};
    fitopts = datas{3};
    angle=fitopts.div_a*sqrt(-2.0*log(xdata./fitopts.div_b)).*(xdata./fitopts.div_b).^(1.0/fitopts.div_c); %half angle distribution of proton beam
    r1=fitopts.div_d*tan(angle*pi/180.); %beam radius at entrance of apperture
    solid=fitopts.div_r^2./r1.^2; %ratio of apperture to real beam -> defines ratio of protons through appeture; assumption: homogenious spacial beam profile
    solid(solid>=1.0)=1.0; %ratio can max=1 -> all protons pass apperture; beam smaller apperture -> solid=1.0
    P1 = params(1);
    P2 = params(2);
    FittedCurve = log(solid.*P1./xdata.*exp(-xdata./P2));
    ErrorVector = FittedCurve - ydata;
    sse = sum(ErrorVector .^ 2);
end

function [sse, FittedCurve, sseJ] = expfun_dose(params, datas)
    ydata = datas{2};
    xdata = datas{1};
    deltax = datas{5};
    deltay = datas{4};
    P1 = params(1);
    P2 = params(2); 
    P3 = params(3);
            
    %calc normed weights, relative on two axis; "diagonal error" => square
    %estimation => fulldeltarelative=((deltay/ydata)^2+(deltax/xdata)^2)^(1/2)="square arround datapoint"
    %square is good approximation, cause each datapoint is represented in
    %kind of statistical "quality"
    %weights are inverse of delta-error: w=1/fulldeltarelative
    %length(xdata) is just for real sseJ, cause optimization is independent
    %of factor in front => minimum is equal minimum times constant,
    %but real sseJ needs this cause (n=2, equal weights, normed)
    %=>2*(0.5*delta1+0.5*delta2)=>gives "weighted real sseJ"
    w=((deltay./ydata).^2 + (deltax./xdata).^2).^(-1/2);
    wnormed=length(xdata)*w./sum(w);
  
    data=getappdata(0,'data');
    ELOarray = data.data.ELOarray;
    
    %prevent division by zero; look into first layer for enery range
    ELOarray((ELOarray(:,1,1)==0),:,:)=1e-5;
    
    %eneryloss files cut-off at Emax
    ELOarray = ELOarray((ELOarray(:,1,1)<=P3),:,:);
    
    %integrate over particle spectrum,weighted by energy-loss in RCF
    %stack. 1-exp()-equation at end accounts for dose-sensitivity in
    %RCF.
    %correction by Olli - Bragg-Correction wil be done directly in RCF
    %Energyloss GUI => ELOarray(:,4,:)
    integrand = P1*1e12./ELOarray(:,1,:).*exp(-ELOarray(:,1,:)./P2).*ELOarray(:,4,:); %.*(1-exp(-0.23804*ELOarray(:,1,:)-0.62681)); 
    integrand = squeeze(integrand);
    dose = simpsum(integrand, P3);
    
    %sseJ is "real" sse, not log' one => for jacobian stuff 
    FittedCurve = log(dose);
    ErrorVector = FittedCurve - ydata;
    sseJ = sum(wnormed.*(dose - exp(ydata)).^2);
    sse = sum(wnormed.*ErrorVector.^2);
end

function [sse, FittedCurve, sseJ] = expfun2_dose(params, datas)
    ydata = datas{2};
    xdata = datas{1};
    deltax = datas{5};
    deltay = datas{4};
    P1 = params(1);
    P2 = params(2);
    P3 = params(3);
                 
    %calc normed weights, relative on two axis; "diagonal error" => square
    %estimation => fulldeltarelative=((deltay/ydata)^2+(deltax/xdata)^2)^(1/2)="square arround datapoint"
    %square is good approximation, cause each datapoint is represented in
    %kind of statistical "quality"
    %weights are inverse of delta-error: w=1/fulldeltarelative
    %length(xdata) is just for real sseJ, cause optimization is independent
    %of factor in front => minimum is equal minimum times constant,
    %but real sseJ needs this cause (n=2, equal weights, normed)
    %=>2*(0.5*delta1+0.5*delta2)=>gives "weighted real sseJ"
    w=((deltay./ydata).^2 + (deltax./xdata).^2).^(-1/2);
    wnormed=length(xdata)*w./sum(w);
  
    data=getappdata(0,'data');
    ELOarray = data.data.ELOarray;
    
    %prevent division by zero; look into first layer for enery range
    ELOarray((ELOarray(:,1,1)==0),:,:)=1e-5;
    
    %eneryloss files cut-off at Emax
    ELOarray = ELOarray((ELOarray(:,1,1)<=P3),:,:);
    
    %integrate over particle spectrum,weighted by energy-loss in RCF
    %stack. 1-exp()-equation at end accounts for dose-sensitivity in
    %RCF.
    %correction by Olli - Bragg-Correction wil be done directly in RCF
    %Energyloss GUI => ELOarray(:,4,:)
    integrand = P1*1e12./ELOarray(:,1,:).*exp(-ELOarray(:,1,:).^2/P2.^2).*ELOarray(:,4,:); %.*(1-exp(-0.23804*ELOarray(:,1,:)-0.62681));
    integrand = squeeze(integrand);
    dose = simpsum(integrand, P3);
    
    %sseJ is "real" sse, not log' one => for jacobian stuff 
    FittedCurve = log(dose);
    ErrorVector = FittedCurve - ydata;
    sseJ = sum(wnormed.*(dose - exp(ydata)).^2);
    sse = sum(wnormed.*ErrorVector.^2);    
end

function [sse, FittedCurve, sseJ] = naturefun_dose(params, datas)
    ydata = datas{2};
    xdata = datas{1};
    deltax = datas{5};
    deltay = datas{4};
    P1 = params(1);
    P2 = params(2);
    P3 = params(3);
            
    %calc normed weights, relative on two axis; "diagonal error" => square
    %estimation => fulldeltarelative=((deltay/ydata)^2+(deltax/xdata)^2)^(1/2)="square arround datapoint"
    %square is good approximation, cause each datapoint is represented in
    %kind of statistical "quality"
    %weights are inverse of delta-error: w=1/fulldeltarelative
    %length(xdata) is just for real sseJ, cause optimization is independent
    %of factor in front => minimum is equal minimum times constant,
    %but real sseJ needs this cause (n=2, equal weights, normed)
    %=>2*(0.5*delta1+0.5*delta2)=>gives "weighted real sseJ"
    w=((deltay./ydata).^2 + (deltax./xdata).^2).^(-1/2);
    wnormed=length(xdata)*w./sum(w);
  
    data=getappdata(0,'data');
    ELOarray = data.data.ELOarray;
    
    %prevent division by zero; look into first layer for enery range
    ELOarray((ELOarray(:,1,1)==0),:,:)=1e-5;
    
    %eneryloss files cut-off at Emax
    ELOarray = ELOarray((ELOarray(:,1,1)<=P3),:,:);
    
    %integrate over particle spectrum,weighted by energy-loss in RCF
    %stack. 1-exp()-equation at end accounts for dose-sensitivity in
    %RCF.
    %correction by Olli - Bragg-Correction wil be done directly in RCF
    %Energyloss GUI => ELOarray(:,4,:)
    integrand = 1e12*P1./sqrt(2*ELOarray(:,1,:)*P2).*exp(-sqrt(2*ELOarray(:,1,:)/P2)).*ELOarray(:,4,:); %.*(1-exp(-0.23804*ELOarray(:,1,:)-0.62681));
    integrand = squeeze(integrand);
    dose = simpsum(integrand, P3);
    
    %sseJ is "real" sse, not log' one => for jacobian stuff 
    FittedCurve = log(dose);
    ErrorVector = FittedCurve - ydata;
    sseJ = sum(wnormed.*(dose - exp(ydata)).^2);
    sse = sum(wnormed.*ErrorVector.^2);    
end

function [sse, FittedCurve, sseJ] = expsolidfun_dose(params, datas)
    fitopts = datas{3};
    ydata = datas{2};
    xdata = datas{1};
    deltax = datas{5};
    deltay = datas{4};
    P1 = params(1);
    P2 = params(2);
    P3 = params(3);

    %calc normed weights, relative on two axis; "diagonal error" => square
    %estimation => fulldeltarelative=((deltay/ydata)^2+(deltax/xdata)^2)^(1/2)="square arround datapoint"
    %square is good approximation, cause each datapoint is represented in
    %kind of statistical "quality"
    %weights are inverse of delta-error: w=1/fulldeltarelative
    %length(xdata) is just for real sseJ, cause optimization is independent
    %of factor in front => minimum is equal minimum times constant,
    %but real sseJ needs this cause (n=2, equal weights, normed)
    %=>2*(0.5*delta1+0.5*delta2)=>gives "weighted real sseJ"
    w=((deltay./ydata).^2 + (deltax./xdata).^2).^(-1/2);
    wnormed=length(xdata)*w./sum(w);
  
    data=getappdata(0,'data');
    ELOarray = data.data.ELOarray;
    
    %prevent division by zero; look into first layer for enery range
    ELOarray((ELOarray(:,1,1)==0),:,:)=1e-5;
    
    %eneryloss files cut-off at Emax
    ELOarray = ELOarray((ELOarray(:,1,1)<=P3),:,:);
    
    %integrate over particle spectrum,weighted by energy-loss in RCF
    %stack. 1-exp()-equation at end accounts for dose-sensitivity in
    %RCF.
    %correction by Olli - Bragg-Correction wil be done directly in RCF
    %Energyloss GUI => ELOarray(:,4,:)
    angle = fitopts.div_a*sqrt(-2.0*log(ELOarray(:,1,:)./fitopts.div_b)).*(ELOarray(:,1,:)./fitopts.div_b).^(1.0/fitopts.div_c); %half angle distribution of proton beam
    r1=fitopts.div_d*tan(angle*pi/180.); %beam radius at entrance of apperture
    solid=fitopts.div_r^2./r1.^2; %ratio of apperture to real beam -> defines ratio of protons through appeture; assumption: homogenious spacial beam profile
    solid(solid>=1.0)=1.0; %ratio can max=1 -> all protons pass apperture; beam smaller apperture -> solid=1.0

    integrand = 1e12*P1*solid./ELOarray(:,1,:).*exp(-ELOarray(:,1,:)./P2).*ELOarray(:,4,:); %.*(1-exp(-0.23804*ELOarray(:,1,:)-0.62681)); 
    integrand = squeeze(integrand);
    dose = simpsum(integrand, P3);
    
    %sseJ is "real" sse, not log' one => for jacobian stuff 
    FittedCurve = log(dose);
    ErrorVector = FittedCurve - ydata;
    sseJ = sum(wnormed.*(dose - exp(ydata)).^2);
    sse = sum(wnormed.*ErrorVector.^2);    
end

%
%for jacobian error estimations
%
function func = expfun_func(Emaxfit,datas)
    %we only make an error estimation for N0 and kT; Emax will be fixed to
    %Emax from fit
    data=getappdata(0,'data');
    ELOarray = data.data.ELOarray;
    
    %prevent division by zero; look into first layer for enery range
    ELOarray((ELOarray(:,1,1)==0),:,:)=1e-5;
    %only take values for integration which are smaller or equal Emaxfit    
    ELOarray = ELOarray((ELOarray(:,1,1)<=Emaxfit),:,:);
    
    func = @(params) simpsum(squeeze(1e12*params(1)./ELOarray(:,1,:).*exp(-ELOarray(:,1,:)./params(2)).*ELOarray(:,4,:)), Emaxfit);
end

function func = expfun2_func(Emaxfit,datas)
    %we only make an error estimation for N0 and kT; Emax will be fixed to
    %Emax from fit
    data=getappdata(0,'data');
    ELOarray = data.data.ELOarray;
    
    %prevent division by zero; look into first layer for enery range
    ELOarray((ELOarray(:,1,1)==0),:,:)=1e-5;
    %only take values for integration which are smaller or equal Emaxfit    
    ELOarray = ELOarray((ELOarray(:,1,1)<=Emaxfit),:,:);
    
    func = @(params) simpsum(squeeze(1e12*params(1)./ELOarray(:,1,:).*exp(-ELOarray(:,1,:).^2./params(2).^2).*ELOarray(:,4,:)), Emaxfit);
end

function func = naturefun_func(Emaxfit,datas)
    %we only make an error estimation for N0 and kT; Emax will be fixed to
    %Emax from fit
    data=getappdata(0,'data');
    ELOarray = data.data.ELOarray;
    
    %prevent division by zero; look into first layer for enery range
    ELOarray((ELOarray(:,1,1)==0),:,:)=1e-5;
    %only take values for integration which are smaller or equal Emaxfit    
    ELOarray = ELOarray((ELOarray(:,1,1)<=Emaxfit),:,:);

    func = @(params) simpsum(squeeze(1e12*params(1)./sqrt(2*ELOarray(:,1,:)*params(2)).*exp(-sqrt(2*ELOarray(:,1,:)./params(2))).*ELOarray(:,4,:)), Emaxfit);
end

function func = expsolidfun_func(Emaxfit,datas)
    %we only make an error estimation for N0 and kT; Emax will be fixed to
    %Emax from fit
    data=getappdata(0,'data');
    ELOarray = data.data.ELOarray;
    fitopts = datas{3};
    
    %prevent division by zero; look into first layer for enery range
    ELOarray((ELOarray(:,1,1)==0),:,:)=1e-5;
    %only take values for integration which are smaller or equal Emaxfit    
    ELOarray = ELOarray((ELOarray(:,1,1)<=Emaxfit),:,:);
    
    angle = fitopts.div_a*sqrt(-2.0*log(ELOarray(:,1,:)./fitopts.div_b)).*(ELOarray(:,1,:)./fitopts.div_b).^(1.0/fitopts.div_c); %half angle distribution of proton beam
    r1=fitopts.div_d*tan(angle*pi/180.); %beam radius at entrance of apperture
    solid=fitopts.div_r^2./r1.^2; %ratio of apperture to real beam -> defines ratio of protons through appeture; assumption: homogenious spacial beam profile
    solid(solid>=1.0)=1.0; %ratio can max=1 -> all protons pass apperture; beam smaller apperture -> solid=1.0

    func = @(params) simpsum(squeeze(1e12*params(1)*solid./ELOarray(:,1,:).*exp(-ELOarray(:,1,:)./params(2)).*ELOarray(:,4,:)), Emaxfit);
end


