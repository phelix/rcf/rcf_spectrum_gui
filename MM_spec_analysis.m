%% Calc number of protons for each (rcf-)layer

%% Dec. Var.
clear all
% close all
data = getappdata(0,'data');
ImgArray = data.data.ImageArray;
EloArray = data.data.ELOarray;
rcfconf = data.parameters.rcfconfig;

szImg = size(ImgArray);

depEsum = zeros(szImg(3),1);
depEsin = zeros(szImg(3),1);
Ercf = zeros(szImg(3),1);
Ercfs = zeros(szImg(3),1);
szElo = size(EloArray);

%Gr��e der Energieintervale in MeV:
Es = 1e-3;

%Dec. Energiebereich
E = min(EloArray(:,1,1)):Es:max(EloArray(:,1,1));
szE = size(E);

%Die Gr��e der Energieintervalle im EloArray wird angepasst und in einem
%entsprechenden Array (eloInt) gespeichert
eloInt = zeros(szE(2),szElo(3));
for i = 1:szElo(3)
    eloInt(:,i) = interp1(EloArray(:,1,i),EloArray(:,4,i),E);
end

%Die Protonenergien, die die meiste Energie in den Schichten verlieren
%werden berechnet:
for i = 1:szImg(3)
    [depEsin(i),maxi] = max(eloInt(:,i));
    Ercf(i) = E(maxi);
    depEsum(i) =sum(sum(ImgArray(:,:,i)));
    
    istart = find(eloInt(:,i)>0,1);
    Ercfs(i) = E(istart);
end

%Jeder Schicht wird eine Energiebreite dErcf zugewiesen:
dErcf = zeros(szImg(3),1);
for i = 1:szImg(3)
%     if strcmp(rcfconf(i),'H1B')
%         dErcf(i) = 0.1;
%     elseif strcmp(rcfconf(i),'E1D')
%         dErcf(i) = 0.165;
%     elseif strcmp(rcfconf(i),'E3')
%         dErcf(i) = 0.165;
%     elseif strcmp(rcfconf(i),'H2')
%         dErcf(i) = 0.1;
%     end
    
    if strcmp(cellfun(@(v)v(1),rcfconf(i)),'H')
        dErcf(i) = 0.1;
    elseif strcmp(cellfun(@(v)v(1),rcfconf(i)),'E')
        dErcf(i) = 0.165;
    end
    
end

%Jede Schicht bekommt eine "Startenergie" und eine "Endenergie" zugewiesen.
%Au�erdem wird eine Cut-Off-Energie gew�hlt. Die Teilchenzahl (nop) bei diesen
%Energien (Ercfs) werden sp�ter variiert und zwischen ihnen wird linear
%interpoliert.
Ercfs = sort([Ercfs;Ercfs]);
for i = 1:szImg(3)
    Ercfs(2*i) = Ercfs(2*i) + dErcf(i);
end
Ercfs = [Ercfs;Ercfs(end-1) + 1];

interpol = 'linear';
depEsum0 = depEsum;

%% Dec. Anfangsspektrum
% Um ein sinnvolles Anfangsspektrum zu erhalten wird angenommen, dass die
% deponierte Energie in jedem RCF durch ein gestopptes Proton erzeugt
% wurde. Die Teilchenzahlen f�r alle anderen Energien werden linear
% interpoliert. Das so erhaltene Teilchenspektrum �bersch�tzt den Energieverlust
% logischerweise.
nop = depEsum./depEsin./dErcf;
p_in_rcf0 = nop;

%nop2 enth�lt die Teilchenzahl bei den Energien Ercfs
nop2 = [nop,nop];
nop2 = nop2';
nop2 = nop2(:);
nop2 = [nop2;0];

%nopInt enth�lt die Teilchenzahl f�r jedes Energie in E
nopInt = interp1(Ercfs,nop2,E,interpol);

%Hier wird lediglich NaN in [] umgewandelt. Die NaNs entstehen, wenn der
%Bereiche in dem interpoliert werden soll zu gro� ist.
%Bsp.: interp1(1:3,1:3,1:4,'linear')
idx = find(isnan(nopInt));
nopInt(idx) = [];
E(idx) = [];
eloInt(idx,:) = [];
szE = size(E);

%Im diesem Plot ist das Anfangsspektrum zu sehen
figure
hold off
plot(E,nopInt)
hold on
plot(Ercfs,nop2,'o')
title('Particle spectrum before deconvolution')  
xlabel('E (MeV)')
ylabel('Number of protons per 1keV')
nopInt0 = nopInt;

%Der Energieverlust des Anfangsspektrums wird berechnet:
depEsumi = zeros(1,szImg(3));
for i = fliplr(1:szE(2))
    depEsumi = depEsumi+nopInt(i)*squeeze(eloInt(i,:));
end

%Wie bereits erw�hnt �bersch�tzt das Anfangsspektrum den Energieverlust.
%Dies ist im folgenden Plot zu sehen:
figure
hold off
plot(Ercf,depEsumi,'ro','MarkerFaceColor','r')
hold on
plot(Ercf,depEsum0,'bo','MarkerFaceColor','b')
title('Measured energy loss (blue) and energy loss of particle spectrum before deconvolution (red)')  
ylabel('Deponierte Energie (MeV)')
xlabel('Energie (MeV)')

%% Start deconvolution

Ercfs0 = Ercfs;
dnop = 1e+9;
dnopj = dnop;

idx1 = zeros(1,length(E));
dErcf_2 = dErcf/2;
% Achtung! Die Entfaltung ist nicht Eindeutig. Es gibt viele Teilchenspektren die
% den gleichen Energieverlust in den RCFs aufweisen.

%Die Teilchenanzahl (nop) bei den Energien (Ercfs) wird nun so lange um
%dnopj ver�ndert bis der Energieverlust des Teilchenspektrums und der gemessenen
%Energieverlust �bereinstimmen. Die Routine beginnt dabei bei der letzten
%Schicht (h�chsten Energie) und arbeitet sich dann systematisch nach vorn.
%Falls das Teilchenspektrum den Energieverlust immer noch �bersch�tzt,
%obowhl die Teilchenanzahl(nop) bei einer Energie(Ercfs) 0 ist wird die
%Energie (Ercfs) erh�ht, sodass die Teilchenzahl zwischen den beiden
%Schichten vermindert wird.
%(Entschuldigt bitte die Menge an Variablen, Code-Wiederholungen und For/While-Schleifen :)


for j = fliplr(1:szImg(3))
    dnopj = dnop;
    while abs(depEsumi(j)/depEsum0(j)-1) > 0.01

        if depEsumi(j)>= depEsum0(j)
            nop(j) = nop(j)-dnopj;
        else
            nop(j) = nop(j)+dnopj;
            dnopj = dnopj*0.5;
        end
        if nop(j) < 0
            nop(j) = 0;
            Ercfs(2*j) = Ercfs(2*j) + Es;
            if Ercfs(2*j)>Ercfs(2*j+1)
                break
            end
        end
        nop2 = [nop,nop];
        nop2 = nop2';
        nop2 = nop2(:);
        nop2 = [nop2;0];
        nopInt = interp1(Ercfs,nop2,E,interpol);
        nopInt(isnan(nopInt)) = 0;
        nopInt(idx1==1) = 0;
        depEsumi(j) = sum(nopInt'.*squeeze(eloInt(:,j)));
    end
end

% In diesm Plot wird der gemessene Energieverlust und der
% Energieverlust des erhaltenenen Teilchenspektrums verglichen:
figure
bar([Ercf,Ercf],[depEsum0,depEsumi'])
title('Measured energy loss (blue) and energy loss of the deconvoluted spectrum (yellow)')  
ylabel('Deponierte Energie (MeV)')
xlabel('Energie (MeV)')

% Dieser Plot zeigt das erhaltene Teilchenspektrum:
figure
hold off
bar(E,nopInt)
hold on
p_in_rcf = zeros(1,szImg(3));
% Mit dieser Option gibt man an, ob die Protonen die in einer Schicht
% gestoppt wurden (case:0) berechnet wird oder die Anzahl der Protonen die Energie
% in der Schicht verloren haben (case:1)
integrate_nopInt = 0;
for i = 1:szImg(3)
    
    if integrate_nopInt == 0
        idx = find((Ercf(i) - dErcf_2(i)) < E & (Ercf(i) + dErcf_2(i)) > E);
    else
        idx = find((Ercf(i) - dErcf_2(i)) < E);
    end
    
    idx = int64(idx);
    p_in_rcf(i) = sum(nopInt(idx));
    
    plot([(Ercf(i) - dErcf_2(i)),(Ercf(i) - dErcf_2(i))],[0 max(nopInt)],'r--')
    if integrate_nopInt == 0
        plot([(Ercf(i) + dErcf_2(i)),(Ercf(i) + dErcf_2(i))],[0 max(nopInt)],'r--')
    else
        plot([E(end),E(end)],[0 max(nopInt)],'k--')
    end
    
end
ylim([0 max(nopInt)])
title('Particle spectrum after deconvolution')  
ylabel('Number of protons per 1keV')
xlabel('Energie (MeV)')

% Es wird die Anzahl der Protonen in den Schichten ausgegeben:
figure
bar(Ercf,p_in_rcf)
if integrate_nopInt == 0
    title('Number of protons which are stopped in the layers')
else
    title('Number of protons which have lost energy in the layers')
end
xlabel('Energie (MeV)')
ylabel('Number of protons')

depEsumi = zeros(1,szImg(3));
for i = fliplr(1:szE(2))
    depEsumi = depEsumi+nopInt(i)*squeeze(eloInt(i,:));
end

%% Plot
% Plottet die einzelnen Schichten mit vielen Zusatzinformationen. F�r die
% Bestimmung der Fokusgr��e wird noch eine Faltung implementiert, da
% der Pixel mit der "meisten" deponierten Energie nicht zwangsl�ufig der
% Mittelpunkt des Fokus ist.

% Die Faltung zur Fokusbestimmung wurde am 23.06.20 implementiert

ploton = 0;

if ploton == 1
for i = 1:szImg(3)
    [imgX,imgY] = meshgrid(1:szImg(1),1:szImg(2));
    figure
    set(gcf, 'renderer', 'zbuffer')
%     myCMap = colormap(flipud(gray));
    myCMap = colormap(jet);
%     myCMap(1,:) = [1 1 1];
    myCMap = [1 1 1;0.857142865657806 0.857142865657806 1;0.714285731315613 0.714285731315613 1;0.571428596973419 0.571428596973419 1;0.428571432828903 0.428571432828903 1;0.28571429848671 0.28571429848671 1;0.142857149243355 0.142857149243355 1;0 0 1;0 0.0625 1;0 0.125 1;0 0.1875 1;0 0.25 1;0 0.3125 1;0 0.375 1;0 0.4375 1;0 0.5 1;0 0.5625 1;0 0.625 1;0 0.6875 1;0 0.75 1;0 0.8125 1;0 0.875 1;0 0.9375 1;0 1 1;0.0625 1 0.9375;0.125 1 0.875;0.1875 1 0.8125;0.25 1 0.75;0.3125 1 0.6875;0.375 1 0.625;0.4375 1 0.5625;0.5 1 0.5;0.5625 1 0.4375;0.625 1 0.375;0.6875 1 0.3125;0.75 1 0.25;0.8125 1 0.1875;0.875 1 0.125;0.9375 1 0.0625;1 1 0;1 0.9375 0;1 0.875 0;1 0.8125 0;1 0.75 0;1 0.6875 0;1 0.625 0;1 0.5625 0;1 0.5 0;1 0.4375 0;1 0.375 0;1 0.3125 0;1 0.25 0;1 0.1875 0;1 0.125 0;1 0.0625 0;1 0 0;0.9375 0 0;0.875 0 0;0.8125 0 0;0.75 0 0;0.6875 0 0;0.625 0 0;0.5625 0 0;0.5 0 0];
    colormap(myCMap);

    if length(size(ImgArray)) == 2
        cImg = ImgArray(:,:);
    else
        cImg = ImgArray(:,:,i);
    end

    
    [maxcImg,maxidx] = max(cImg(:));
    
    %---------------------------------------------------------------------
    % Fokus berechnung (23.06.2020):
    
    focus_radius = 1; %pixel (muss gerade sein)
    inc_radius = 0.5; %pixel (muss gerade sein)
    max_radius = 50; %Wenn der max. radius zu gro� gew�hlt wird kann es zu einer sehr hohen Rechenzeit kommen
    dep_energy_focus = 0;
    dep_energy_layer = sum(cImg(:));
    
    while (max(max(dep_energy_focus))/dep_energy_layer < 0.5) && focus_radius < max_radius
        circle = ones(2*focus_radius+1);
        cg = -focus_radius:1:focus_radius;
        [cX,cY] = meshgrid(cg,cg);
        circle((cX.^2 + cY.^2) > focus_radius^2) = 0;
        dep_energy_focus = conv2(cImg,circle,'same');
        focus_radius = focus_radius + inc_radius;
    end
    %---------------------------------------------------------------------
    
%     Fs = 0.01;
%     q = 0;
%     sumcImg = sum(cImg(:));
%     while q < 0.5
%         Fs = Fs + 0.01;
%         idx = find((imgX-x0).^2 + (imgY-y0).^2 <= Fs^2);
%         q = sum(cImg(idx))/sumcImg;
%     end    
%     
%     xlim([min(min(imgX)) max(max(imgX))])
%     ylim([min(min(imgY)) max(max(imgY))])
%     caxis([0 max(max(max(ImgArray)))])
%     
%     x0 = imgX(maxidx);
%     y0 = imgY(maxidx);

    % Convert Pixel to mm %valid?
    Fs = focus_radius* 52/2241;
    [~,Fs_ind] = max(dep_energy_focus(:));
    
    imgX = imgX' * 52/2241; 
    imgY = imgY' * 52/2241;
    
    x0 = imgX(Fs_ind);
    y0 = imgY(Fs_ind);
    
    hold off
    
    if length(size(ImgArray)) == 2
        surf(imgX-x0,imgY-y0,ImgArray(:,:),'LineStyle','none');
    else
        surf(imgX-x0,imgY-y0,ImgArray(:,:,i),'LineStyle','none');
    end

    hold on
    view(0,90)

    n = 0:pi/100:2*pi;
    h = plot3(Fs*sin(n),Fs*cos(n),maxcImg*ones(size(n)),'LineWidth',2,'LineStyle',':',...
    'Color','m');
%     set(h,'ZData',ones(size(n))) 
    
    AFs = pi*Fs^2;
    dt = 742*1e-12;
%     dt = 7*1e-9;
    
    xlabel('x(mm)')
    ylabel('y(mm)')
    
    title(strcat(rcfconf(i),{' '},num2str(Ercf(i)), '(MeV)'))
    
    cb = colorbar;
    cb.Label.String = 'Deposited E(MeV)';
    
    str1 = strcat('Protons: ',num2str(p_in_rcf(i)*1e-6),' x10^6');
    str2 = strcat('E: ',num2str(Ercf(i)),' MeV');
    str3 = strcat('Pulse length: ',num2str(dt*1e+12),' ps');
    if focus_radius < max_radius
        str4 = strcat('Focal radius: ',num2str(Fs),' mm');
    else
        str4 = strcat('Focal radius: ',num2str(Fs),' mm (max)');
    end
    str5 = strcat('Fluence: ',num2str(round(p_in_rcf(i)/2/AFs*1e-6,2)),' x10^6 protons/mm�');
    str6 = strcat('Intensity: ',num2str(round(p_in_rcf(i)/2/AFs/dt*1e-15,2)),'  x10^1^5 protons/(s mm�)');
    str7 = strcat('Current: ',num2str(round(p_in_rcf(i)*1.602*1e-16/dt,2)),'  (mA)');
    
    annotation('textbox',[.6 .8 .1 .1],'String',{str1;str2;str3;str4;str5;str6;str7},'BackgroundColor','w')
    drawnow()
end
end