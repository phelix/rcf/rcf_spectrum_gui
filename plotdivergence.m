function plotdivergence (fitmodel,style)
% plot energy-resolved divergence

data = getappdata(0, 'data');

d_target_rcf = data.parameters.target.distance;
dotsperinch = data.data.fileinfo.XResolution;

%define how many layers to take
count_lay = size(data.data.ImageArray,3);

%init
energies = zeros(1,count_lay);
energies_error = zeros(count_lay,1);
angle_deg = zeros(1,count_lay);
error_angle_deg = zeros(count_lay,1);

if (strcmp(style,'particlesdiv') && isfield(data.data, 'imgArrayNP'))
    tmp = data.data.imgArrayNP;
    % trick to find non zero values
    tmp(tmp<=0) = inf;
    maxlevel = max(min(min(tmp,[],1),[],2));
%     maxlevel = .1 * max(max(tmp(:,:,end)))
    clear tmp;
%     maxlevel = 0.4 * max(max(data.data.imgArrayNP(:,:,end)));
else
    level = 0.001;
end

% limit = max(max(data.data.ImageArray(:,:,end)))*0.1;

%calculate Bragg-peak energy for each RCF
% average for MD
for i = 1:count_lay
    %peaks_position = peaks(data.data.ELOarray(:,2,i));
    [a, peaks_position] = max(data.data.ELOarray(:,4,i));
    %energies(i) = mean(data.data.ELOarray(peaks_position,1,i));
    energies(i) = data.data.ELOarray(peaks_position,1,i);

    
    if (strcmp(style,'particlesdiv') && isfield(data.data, 'imgArrayNP'))
        level = maxlevel/max(max(data.data.imgArrayNP(:,:,i)));
        if level >= 1
            level = 0.001;
        end
    end
    BW = im2bw(data.data.ImageArray(:,:,i)/max(max(data.data.ImageArray(:,:,i))), level);
    BW = imfill(BW,'holes');
%     sum(sum(BW))

    stats = regionprops(BW, 'Area', 'PixelIdxList' );
    area = [stats.Area];
    [~, largestBlobIndex] = max(area);
    blobIdx = stats(largestBlobIndex).PixelIdxList;
    BW(:) = false;
    BW(blobIdx) = true;

    BW = bwconvhull(BW);

    % find where BW is not 0, return linear index, translate first element
    % to row and col with ind2sub ....
    tmp3 = find(BW);
    [row, col] = ind2sub(size(BW), tmp3(1));
    clear tmp3;
    % traces the beam edge 
    boundary = bwtraceboundary(BW,[row, col],'SW');
    clear BW;

    % calculates the radius
    D1 = max(boundary(:,1))-min(boundary(:,1));
    D2 = max(boundary(:,2))-min(boundary(:,2));
    
    %error bars, always 0.5 MeV for HD, 1 MeV for MD or M2 and 1.5 MeV for HS
    %needs to be verified for "new" films H2, E3 => later
    switch (char(data.parameters.rcfconfig(i)))
        case {'HD', 'H2'}
            energies_error(i)=0.25;
        case {'MD', 'M2', 'E3'}
            energies_error(i)=0.5;
        case 'HS'
            energies_error(i)=0.75;
    end
    
    radius = ((D1+D2)/2)/2;
    radius_mm = radius*25.4/dotsperinch;

    max_deviation = max([D1/2-radius,D2/2-radius]);
    max_deviation_mm = max_deviation*25.4/dotsperinch;
    
    %calculates the angle
    angle_rad = atan(radius_mm/d_target_rcf);
    angle_deg(i) = angle_rad/(2*pi)*360;
    error_angle_deg(i) = max_deviation_mm/d_target_rcf/(2*pi)*360;
end

% fit curve to data; different models
if strcmp(fitmodel,'simpledivfit')
    %fit parabolic curve to data
    [model, text] = model_handler('expfun');
elseif strcmp(fitmodel,'simpledivfit2')
    %fit parabolic curve to data
    [model, text] = model_handler('expfun2');
elseif strcmp(fitmodel,'plasexpdivfit')
    %fit parabolic curve to data
    [model, text] = model_handler('parfun');
end

if strcmp(style,'normalizeddiv')
    est = fminsearch(@(x) model(x, {energies./max(energies), angle_deg}), [max(angle_deg) 1 4]);
    x_fit=0:0.01:1;
elseif (strcmp(style,'absolutediv') || strcmp(style,'particlesdiv'))
    est = fminsearch(@(x) model(x, {energies, angle_deg}), [max(angle_deg) max(energies) 4]);
    x_fit=min(energies):0.01:max(energies);
end
% get fited curve values
[~, y_fit] = model(est, {x_fit, 0});

figure();
if strcmp(style,'normalizeddiv')
    errorbarxy(energies./max(energies),angle_deg,energies_error./max(energies),error_angle_deg,[],[],[],[]);
    axis([0 1 0 ceil(max(angle_deg))+5]);
elseif (strcmp(style,'absolutediv') || strcmp(style,'particlesdiv'))
    errorbarxy(energies,angle_deg,energies_error,error_angle_deg,[],[],[],[]);
    axis([0 ceil(max(energies))+4 0 ceil(max(angle_deg))+5]);
end
set(gca, 'FontSize', 16);
hold on
plot(x_fit,y_fit,'-r','LineWidth',2);
hold off
xlabel ('proton energy (MeV)');
ylabel ('envelope half opening angle (deg)');

if strcmp(fitmodel,'plasexpdivfit')
    est = [est(1) est(2) est(2) est(3)];
end

% write fit-formula
if strcmp(style,'particlesdiv')
    annotation('textbox', [.11 0.85 0.8 0.15], ...
        'string', {sprintf(text, est); ['cut N#P/px^2: ' num2str(maxlevel)]}, ...
        'LineStyle', 'none', ...
        'FontSize', 12);
else
    annotation('textbox', [.11 0.85 0.8 0.15], ...
        'string', sprintf(text, est), ...
        'LineStyle', 'none', ...
        'FontSize', 12);
    maxlevel = 0; % just for save reasons
end

data.results.divergence = {est, model, text, maxlevel};
setappdata(0, 'data', data);