function [tmpImageArray, tmpErrMeVpx] = OD2MeV_dresdenDosis(data, handles, frame)

tmpImageArray = data.data.ImageArray(:,:,frame);
rcf_config = upper(char(data.parameters.rcfconfig(frame)));
res = data.data.fileinfo.XResolution;
if isempty(res)
    res = 1;
end
mop = false;

% scale from dose to kev/mm^2/�m
% then later multiply by layer thickness �m
% 1.08 g/cm^3 for HD and MD type active layer
% 1.2 g/cm^3 for EBT type active layer
scaleHD = 1.08*1e-3/(10^3*10^3)/1.602176634e-16;
scaleEBT = 1.2*1e-3/(10^3*10^3)/1.602176634e-16;

% Just to be sure that there are no negative values
tmpImageArray(tmpImageArray < 0) = 0;

switch rcf_config
    case 'EBT3'
        scale = scaleEBT*27;
    otherwise
        mop = true;
        disp('Unknown layer.')
        scale = 1;
end

if ~mop
    tmpErrMeVpx = zeros(size(tmpImageArray,1),size(tmpImageArray,2));
    tmpImageArray = tmpImageArray*scale;
    %convert from dose in MeV/px
    tmpImageArray = 25.4^2/res^2*1e-3 .*tmpImageArray;
else
    tmpErrMeVpx = zeros(size(tmpImageArray,1),size(tmpImageArray,2));
end

clear stdabwBG data rcfconfig scanner fileinfo Ic calib
end