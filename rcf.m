function [cmap] = rcf(maplength)
% ColEdit function [cmap] = rcf(maplength);
%
% colormap m-file written by ColEdit
% version 1.0 on 15-Apr-2005
%
% input  :	[maplength]	[64]	- colormap length
%
% output :	cmap			- colormap RGB-value array
 
% set red points
r = [ [];...
    [0 0.96809];...
    [0.082051 0.68085];...
    [0.18462 0.41489];...
    [0.375 0.19149];...
    [0.625 0.085106];...
    [0.875 0.06383];...
    [1 0.010638];...
    [] ];
 
% set green points
g = [ [];...
    [0 0.97872];...
    [0.125 0.84043];...
    [0.21795 0.73404];...
    [0.375 0.60638];...
    [0.625 0.29787];...
    [0.875 0.095745];...
    [1 0];...
    [] ];
 
% set blue points
b = [ [];...
    [0 0.93617];...
    [0.125 0.98936];...
    [0.22308 0.94681];...
    [0.375 0.82979];...
    [0.625 0.75532];...
    [0.82308 0.48936];...
    [1 0.095745];...
    [] ];
% ColEditInfoEnd
 
% get colormap length
if nargin==1 
  if length(maplength)==1
    if maplength<1
      maplength = 64;
    elseif maplength>256
      maplength = 256;
    elseif isinf(maplength)
      maplength = 64;
    elseif isnan(maplength)
      maplength = 64;
    end
  end
else
  maplength = 64;
end
 
% interpolate colormap
np = linspace(0,1,maplength);
rr = interp1(r(:,1),r(:,2),np,'linear');
gg = interp1(g(:,1),g(:,2),np,'linear');
bb = interp1(b(:,1),b(:,2),np,'linear');
 
% compose colormap
cmap = [rr(:),gg(:),bb(:)];