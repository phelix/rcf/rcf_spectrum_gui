function varargout = definebeam_gui(varargin)
% DEFINEBEAM_GUI MATLAB code for definebeam_gui.fig
%      DEFINEBEAM_GUI, by itself, creates a new DEFINEBEAM_GUI or raises the existing
%      singleton*.
%
%      H = DEFINEBEAM_GUI returns the handle to a new DEFINEBEAM_GUI or the handle to
%      the existing singleton*.
%
%      DEFINEBEAM_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DEFINEBEAM_GUI.M with the given input arguments.
%
%      DEFINEBEAM_GUI('Property','Value',...) creates a new DEFINEBEAM_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before definebeam_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to definebeam_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help definebeam_gui

% Last Modified by GUIDE v2.5 20-Sep-2013 14:27:03

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @definebeam_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @definebeam_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before definebeam_gui is made visible.
function definebeam_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to definebeam_gui (see VARARGIN)

% Choose default command line output for definebeam_gui
handles.output = hObject;

% reuse data or init
data = getappdata(0, 'data');
length = size(data.data.ImageArray,3);
if ~isfield(data, 'mdata') || isempty(data.mdata)
    % complete init ... easy
    data.mdata.x0 = zeros(length,1); % hand set
    data.mdata.y0 = zeros(length,1);
    data.mdata.x_pos = zeros(length,1); % centroid
    data.mdata.y_pos = zeros(length,1);
    data.mdata.angle = 0;
    data.mdata.angledeg = 0;
else
    % partial init ... bad, but musthave for down-compability ... better ?!?
    if ~isfield(data.mdata, 'x0'); data.mdata.x0 = zeros(length,1); end
    if ~isfield(data.mdata, 'y0'); data.mdata.y0 = zeros(length,1); end
    if ~isfield(data.mdata, 'x_pos'); data.mdata.x_pos = zeros(length,1); end
    if ~isfield(data.mdata, 'y_pos'); data.mdata.y_pos = zeros(length,1); end
    if ~isfield(data.mdata, 'angle'); data.mdata.angle = 0; end
    if ~isfield(data.mdata, 'angledeg'); data.mdata.angledeg = 0; end
end
% and finaly set values to GUI
set(handles.rcflistbox,'String', data.parameters.rcffiles);
set(handles.rcflistbox, 'Value', 1);
set(handles.angleedit, 'String', data.mdata.angledeg);

% find all centroid
for frame = 1:length
    ImageArray2 = data.data.ImageArray(:,:,frame);
    % calculate center of beam
	% convert to black and white
    BW = im2bw(ImageArray2);
    % get WeightedCentroid for Image in boundary ..
    stats = regionprops(BW, ImageArray2, 'Area', 'WeightedCentroid');
    area = [stats.Area];
    [~, largestBlobIndex] = max(area);
    wgtc = stats(largestBlobIndex).WeightedCentroid;
    clear BW;
    data.mdata.x_pos(frame) = wgtc(1);
    data.mdata.y_pos(frame) = wgtc(2);
end

% and set the data
setappdata(0, 'data', data);
% END reuse data or init

% init data;
set(handles.axes1, 'Visible', 'off', 'Units', 'normalized');
axis square;
colormap rcf_mod;

%plot RCF & more
makeCalculations(handles);

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = definebeam_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function donebutton_Callback(hObject, eventdata, handles)
close definebeam_gui;

function rcflistbox_Callback(hObject, eventdata, handles)
makeCalculations(handles);

% --- Executes on button press in setcenterbutton.
function setcenterbutton_Callback(hObject, eventdata, handles)
% hObject    handle to setcenterbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
frame = get(handles.rcflistbox,'Value');
data = getappdata(0, 'data');
set(handles.text1, 'String', 'Select Beam Center ...');
[xcenter, ycenter] = ginput(1);
data.mdata.x0(frame)=xcenter;
data.mdata.y0(frame)=ycenter;
setappdata(0, 'data', data);
makeCalculations(handles);

set(handles.text1, 'String', 'Beam Center set ... !');

% --- Executes on button press in setincidentanglebutton.
function setincidentanglebutton_Callback(hObject, eventdata, handles)
% hObject    handle to setincidentanglebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

data = getappdata(0, 'data');
[filename, pathname] = uigetfile( ...
    {'*.png',  'Select Phelix Shot Image (*.png)'; ...
    '*.*',  'All Files (*.*)'}, ...
    'Pick a shot image file', ...
    data.parameters.rcfpath);

if ~isequal(filename,0)
    t = figure();
    shotimage = imread(fullfile(pathname, filename));
    imagesc(shotimage);
    
    hold on;
    axis square;
    [xl1, yl1]=ginput(2);
    plot(xl1,yl1,'--r','LineWidth',2);
    if (xl1(2)<xl1(1))
        xl1 = flipud(xl1); yl1 = flipud(yl1);
    end
    data.mdata.angle = atan((yl1(1)-yl1(2))/(xl1(2)-xl1(1)));
    data.mdata.angledeg = data.mdata.angle*180/pi;
    hold off;
    close(t);
    setappdata(0, 'data', data);
    makeCalculations(handles);
    set(handles.text1, 'String', ['Angle set! ' num2str(data.mdata.angledeg)]);
    set(handles.angleedit, 'String', data.mdata.angledeg);
end

function [yline, xline]  = drawslope(I, alpha, refpoint)
%      
%        function to draw a line at a desired angle <alpha>, 
%        which goes through a reference point <refpoint>
%        function inputs:
%       <I>:                       Image for which the drawing is desired.
%                                       Range:3Dim, grayscale, or logical
%        <alpha>:           Angle in degrees, where 0 is complete horizontal
%                                       Range:   -Inf   -   +Inf    (cyclic after + (-) 180 degrees of course)
%        <refpoint> :   Reference interception point with the slope
%                                        Range: A varaible of size 2x1, where refpoint(1) is horizontal and (2) is vertical
%       function outputs:
%       <yline> :             Y axis coordinates of both line ends
%       <xline>:              X axis coordinates of both line ends
%               
%                                         outputs should be used with plot function as following:
%                                         plot(yline,xline,...plot specifiers)
%         
%           Example usage:
%             Image = imread('someImage');
%             image_centroid =  [100 240];
%             [yline xline]  = drawslope(Image, -60, image_centroid);
%             imshow(Image);
%             hold on;
%             plot(yline, xline, 'blue','LineWidth',2);  
%
%               
%       Written by Elad Boneh, 15.8.2009
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[x, y, z] = size(I);
%Infinite lines protection
if (mod(alpha,180) <  0.01)
    alpha = 0.01;
elseif (mod(alpha,90) <  0.01)
    alpha = 90.001;
end
horizontal_distance = ((refpoint(2)-1)/tand(alpha));
if ((refpoint(1) + horizontal_distance) > y)
	vertical_distance = ((refpoint(1)-1)/cotd(alpha));
    verctical_distance_reversed = ((y-refpoint(1))/cotd(alpha));
    xline = [(refpoint(2) + vertical_distance), (refpoint(2) - verctical_distance_reversed)];
    yline = [1, y];
else
    horizontal_distance_reversed = ((x-refpoint(2))/tand(alpha));
    xline = [1, x];
    yline = [(refpoint(1)+ horizontal_distance), (refpoint(1)- horizontal_distance_reversed)];
end

% --- Executes on button press in removebutton.
function removebutton_Callback(hObject, eventdata, handles)
data = getappdata(0, 'data');
x_pos = data.mdata.x_pos;
y_pos = data.mdata.y_pos;
data.mdata = [];
length = size(data.data.ImageArray,3);
data.mdata.x0 = zeros(length,1);
data.mdata.y0 = zeros(length,1);
data.mdata.x_pos = x_pos;
data.mdata.y_pos = y_pos;
data.mdata.angle = 0;
data.mdata.angledeg = 0;
setappdata(0, 'data', data);

makeCalculations(handles);

set(handles.text1, 'String', 'Data removed !');
set(handles.angleedit, 'String', data.mdata.angledeg);

function angleedit_Callback(hObject, eventdata, handles)
% hObject    handle to angleedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

data = getappdata(0, 'data');
data.mdata.angledeg = str2double(get(handles.angleedit, 'String'));
data.mdata.angle = data.mdata.angledeg/180*pi;
setappdata(0, 'data', data);
makeCalculations(handles);

function makeCalculations(handles)
frame = get(handles.rcflistbox,'Value');
% function for everything ;)
data = getappdata(0, 'data');
ImageArray2 = data.data.ImageArray(:,:,frame);
imagesc(ImageArray2);
hold on
    
BW_bound = im2bw(ImageArray2);

stats = regionprops(BW_bound, 'Area', 'PixelIdxList' );
area = [stats.Area];
[~, largestBlobIndex] = max(area);
blobIdx = stats(largestBlobIndex).PixelIdxList;
BW_bound(:) = false;
BW_bound(blobIdx) = true;

BW_bound = bwconvhull(BW_bound);

% find where BW is not 0, return linear index, translate first element
% to row and col with ind2sub ....
tmp3 = find(BW_bound);
[row, col] = ind2sub(size(BW_bound), tmp3(1));
clear tmp3;
% traces the beam edge 
boundary = bwtraceboundary(BW_bound,[row, col],'SW');

plot(boundary(:,2),boundary(:,1),'g','LineWidth',2); % plot boundary
plot(data.mdata.x_pos(frame), data.mdata.y_pos(frame),'go'); % plot centroid
line([data.mdata.x_pos(frame) data.mdata.x_pos(frame)], ...
    [0 size(ImageArray2,1)], 'LineStyle', '--', 'Color', 'g'); % plot y line
line([0 size(ImageArray2,2)], ...
    [data.mdata.y_pos(frame) data.mdata.y_pos(frame)], 'LineStyle', '--', 'Color', 'g'); % plot x line

if data.mdata.x0(frame) ~= 0 && data.mdata.y0(frame) ~= 0
    plot(data.mdata.x0(frame), data.mdata.y0(frame), 'yo'); % plot hand set center
end

% check if angle is set
if data.mdata.x0(frame) ~= 0 && data.mdata.y0(frame) ~= 0
    [rlx, rly] = drawslope(ImageArray2, data.mdata.angledeg+90, [data.mdata.x0(frame) data.mdata.y0(frame)]);
    [rlx2, rly2] = drawslope(ImageArray2, data.mdata.angledeg, [data.mdata.x0(frame) data.mdata.y0(frame)]);
    line(rlx, rly, 'LineStyle', '--', 'Color', 'black');
    line(rlx2, rly2, 'LineStyle', '--');
elseif data.mdata.angle ~= 0 && data.mdata.x0(frame) == 0 && data.mdata.y0(frame) == 0
    [rlx, rly] = drawslope(ImageArray2, data.mdata.angledeg+90, [data.mdata.x_pos(frame) data.mdata.y_pos(frame)]);
    [rlx2, rly2] = drawslope(ImageArray2, data.mdata.angledeg, [data.mdata.x_pos(frame) data.mdata.y_pos(frame)]);
    line(rlx, rly, 'LineStyle', '--', 'Color', 'black');
    line(rlx2, rly2, 'LineStyle', '--');
end
hold off

