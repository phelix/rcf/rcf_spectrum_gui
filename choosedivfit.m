function varargout = choosedivfit(varargin)
% CHOOSEDIVFIT MATLAB code for choosedivfit.fig
%      CHOOSEDIVFIT, by itself, creates a new CHOOSEDIVFIT or raises the existing
%      singleton*.
%
%
%      H = CHOOSEDIVFIT returns the handle to a new CHOOSEDIVFIT or the handle to
%      the existing singleton*.
%
%      CHOOSEDIVFIT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CHOOSEDIVFIT.M with the given input arguments.
%
%      CHOOSEDIVFIT('Property','Value',...) creates a new CHOOSEDIVFIT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before choosedivfit_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to choosedivfit_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help choosedivfit

% Last Modified by GUIDE v2.5 15-May-2013 12:50:33

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @choosedivfit_OpeningFcn, ...
                   'gui_OutputFcn',  @choosedivfit_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before choosedivfit is made visible.
function choosedivfit_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to choosedivfit (see VARARGIN)

% Choose default command line output for choosedivfit
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = choosedivfit_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in startdivfit.
function startdivfit_Callback(hObject, eventdata, handles)
% hObject    handle to startdivfit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = getappdata(0, 'data');
data.fitparam.divfitfunction = get(get(handles.uipanel1, 'SelectedObject'), 'tag');
data.fitparam.divfitstyle = get(get(handles.uipanel2, 'SelectedObject'), 'tag');
setappdata(0, 'data', data);
plotdivergence(data.fitparam.divfitfunction, data.fitparam.divfitstyle);
close choosedivfit;

% --- Executes on button press in closebutton.
function closebutton_Callback(hObject, eventdata, handles)
% hObject    handle to closebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close choosedivfit
