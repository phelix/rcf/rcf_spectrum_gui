function varargout = RCF_spectrum_GUI(varargin)
% RCF_SPECTRUM_GUI M-file for RCF_spectrum_GUI.fig
%
%      NEEDS: Image Processing Toolbox.
%             Check with the command 'ver' if it is installed on your
%             system
% 
%      RCF_SPECTRUM_GUI, by itself, creates a new RCF_SPECTRUM_GUI or raises the existing
%      singleton*.
%
%      H = RCF_SPECTRUM_GUI returns the handle to a new RCF_SPECTRUM_GUI or the handle to
%      the existing singleton*.
%
%      RCF_SPECTRUM_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RCF_SPECTRUM_GUI.M with the given input arguments.
%
%      RCF_SPECTRUM_GUI('Property','Value',...) creates a new RCF_SPECTRUM_GUI or raises the
%      existing singleton*.  Sltarting from the left, property value pairs are
%      applied to the GUI before RCF_spectrum_GUI_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to RCF_spectrum_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright 2002-2003 The MathWorks, Inc.

% Edit the above text to modify the response to help RCF_spectrum_GUI

% Last Modified by GUIDE v2.5 22-Jun-2020 10:31:12

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @RCF_spectrum_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @RCF_spectrum_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before RCF_spectrum_GUI is made visible.a
function RCF_spectrum_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to RCF_spectrum_GUI (see VARARGIN)

set(handles.figwin, 'Visible', 'off', 'Units', 'normalized');

%disable all buttons despite the first ones
set(handles.openexistingbutton,'Enable','On');
set(handles.parametersbutton,'Enable','On');
set(handles.rcfloadbutton,'Enable','Off');
set(handles.rcfdeletebutton,'Enable','Off');
set(handles.modifybutton,'Enable','Off');
set(handles.div_pushbutton,'Enable','Off');
set(handles.fitsimplebutton,'Enable','Off');
set(handles.fitdeconvbutton,'Enable','Off');
set(handles.savebutton,'Enable','Off');
set(handles.definebeam,'Enable','Off');
set(handles.deconvplots,'Enable','Off');
set(handles.rcfstack_linlog,'Enable','Off');
set(handles.getfig,'Enable','Off');
set(handles.conv2NP,'Enable','Off');
set(handles.MMspecanabutton,'Enable','Off');
set(handles.Rehwaldbutton,'Enable','Off');
handles.invertedstack='normal';

% first start, clean up everything
try rmappdata(0, 'data'); end
% and init
data = [];
data.process = 0;
setappdata(0,'data', data);

guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = RCF_spectrum_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

function parametersbutton_Callback(hObject, eventdata, handles)
uiwait(shotparam_fig);
set(handles.rcfloadbutton,'Enable','On');
set(handles.savebutton,'Enable','On');
guidata(hObject,handles);

function rcfloadbutton_Callback(hObject, eventdata, handles)
data = getappdata(0,'data');
try
    tmp = imfinfo(fullfile(data.parameters.rcfpath, data.parameters.rcffiles{1}));
catch
    set(handles.statustext,'String', 'RCF File not found!', 'ForegroundColor', 'red');
    return;
end

if ~isempty(tmp)
    % reset all "further" data
    try data = rmfield(data, 'data'); end
    try data = rmfield(data, 'mdata'); end
    try data = rmfield(data, 'results'); end
    try data = rmfield(data, 'fitparam'); end
    setappdata(0, 'data', data);
    data.data.fileinfo = tmp;
    
    % reset ui
    set(handles.openexistingbutton,'Enable','On');
    set(handles.parametersbutton,'Enable','On');
    set(handles.rcfloadbutton,'Enable','On');
    set(handles.rcfdeletebutton,'Enable','On');
    set(handles.savebutton,'Enable','On');
    set(handles.getfig,'Enable','Off');
    set(handles.modifybutton,'Enable','Off');
    set(handles.definebeam,'Enable','Off');
    set(handles.div_pushbutton,'Enable','Off');
    set(handles.fitsimplebutton,'Enable','Off');
    set(handles.fitdeconvbutton,'Enable','Off');
    set(handles.savebutton,'Enable','On');
    set(handles.definebeam,'Enable','Off');
    set(handles.deconvplots,'Enable','Off');
    set(handles.conv2NP,'Enable','Off');
    set(handles.rcfstack_linlog,'Enable','Off');
    set(handles.MMspecanabutton,'Enable','Off');
    set(handles.Rehwaldbutton,'Enable','Off');
    % reset ui END
    
    % read data from files
    set(handles.statustext, 'String', 'Loading data...', 'ForegroundColor','black');
    drawnow;
    data = readRCF(data, handles);
    
    % do convert counts2OD
    % CB 20.12.2019 seperate Scanners
    data.data.stdabwBG = zeros(length(data.parameters.rcffiles), size(data.data.ImageArray,3));
    for frame = 1:length(data.parameters.rcffiles)
        set(handles.statustext,'String',['Converting to optical density. Film ',num2str(frame)], 'ForegroundColor','black');
        drawnow;
        if isfield(data.parameters, 'scannerpop')
            switch char(data.parameters.scannerpop)
                case 'ArtixScan 1800F, Lamp 0, RGB'
                    [data.data.ImageArray(:,:,:,frame), fail, npixels, data.data.stdabwBG(frame,:)] = ...
                    counts2OD_microtek_lamp0RGB(data.data.ImageArray(:,:,:,frame), data.parameters.rcfconfig{frame});
                case 'CoolScan 9000ED, Lamp 0, RGB'
                    [data.data.ImageArray(:,:,:,frame), fail, npixels, data.data.stdabwBG(frame,:)] = ...
                    counts2OD_coolscan_lamp0RGB(data.data.ImageArray(:,:,:,frame), data.parameters.rcfconfig{frame});
                case 'Dresden Scanner'
                    [data.data.ImageArray(:,:,:,frame), fail, npixels, data.data.stdabwBG(frame,:)] = ...
                    counts2OD_dresdenScan(data.data.ImageArray(:,:,:,frame), data.parameters.rcfconfig{frame});
                case 'Dresden Dosis'
                    [data.data.ImageArray(:,:,frame), fail, npixels, data.data.stdabwBG(frame)] = ...
                    counts2OD_dresdenDosis(data.data.ImageArray(:,:,frame), data.parameters.rcfconfig{frame});
                otherwise
                    data.data.ImageArray(:,:,:,frame)=NaN;
                    fail = 2;
                    npixels = 0;
            end
        else
            % old versions compatibility
            switch char(data.parameters.scanner)
                case 'microtek_lamp0RGB'
                    [data.data.ImageArray(:,:,:,frame), fail, npixels, data.data.stdabwBG(frame,:)] = ...
                    counts2OD_microtek_lamp0RGB(data.data.ImageArray(:,:,:,frame), data.parameters.rcfconfig{frame});
                case 'coolscan_lamp0RGB'
                    [data.data.ImageArray(:,:,:,frame), fail, npixels, data.data.stdabwBG(frame,:)] = ...
                    counts2OD_coolscan_lamp0RGB(data.data.ImageArray(:,:,:,frame), data.parameters.rcfconfig{frame});
                case 'dresdenScan'
                    [data.data.ImageArray(:,:,:,frame), fail, npixels, data.data.stdabwBG(frame,:)] = ...
                    counts2OD_dresdenScan(data.data.ImageArray(:,:,:,frame), data.parameters.rcfconfig{frame});
                otherwise
                    data.data.ImageArray(:,:,:,frame)=NaN;
                    fail = 2;
                    npixels = 0;
            end
        end

        %color coding for saturation warning; less than 5% => green, between
        %5 and 15% => orange, over 15% => red
        colorc = 'black';
        if npixels < 0.05
            colorc = 'blue';
        elseif npixels <= 0.15 
            colorc = [1 .5 0]; %orange
        elseif npixels > 0.15
            colorc = 'red';
        end

        if ~isequal(fail, 0)
            if isequal(fail, 1)
                set(handles.statustext,'String', [num2str(npixels*100,'%3.1f'), '% of pixels from RCF layer ',num2str(frame),' are saturated! Result may be wrong, maybe delete it?!'], 'ForegroundColor', colorc);
            elseif isequal(fail, 2)
                set(handles.statustext,'String','Scanner not recognised!', 'ForegroundColor','red');
            elseif isequal(fail, 3)
                set(handles.statustext,'String','Wrong Image Data!', 'ForegroundColor','red');
            elseif isequal(fail, 4)
                set(handles.statustext,'String',['No Background Data! Film Type ' data.parameters.rcfconfig{frame} ' unknown'], 'ForegroundColor','red');
            end
            return; % quit function
        end
    end

    set(handles.statustext,'String','Converting to optical density... finished', 'ForegroundColor','black');
    drawnow;

    % do convert OD2MeV
    %make new array for RGB and RGB+IR stuff, cause we end up with just one
    %channel after conversion into MeV/px
    tmpImageArray = zeros(...
    size(data.data.ImageArray,1), ...	% height
    size(data.data.ImageArray,2), ...	% width
    length(data.parameters.rcffiles), 'single');  % files
    %make errMeVpx Array
    tmpErrMeVpx = tmpImageArray;

    %convert in MeV/px for later dose integration
    % CB 20.12.2019 seperate Scanners
    for frame=1:length(data.parameters.rcffiles)
        set(handles.statustext,'String', ['Converting to MeV/px. Film ',num2str(frame),' - Type: ', char(data.parameters.rcfconfig(frame))],'ForegroundColor','black');
        drawnow;
        if isfield(data.parameters, 'scannerpop')
            switch char(data.parameters.scannerpop)
                case 'ArtixScan 1800F, Lamp 0, RGB'
                    [tmpImageArray(:,:,frame), tmpErrMeVpx(:,:,frame)] = OD2MeV_microtek_lamp0RGB(data, handles, frame);
                case 'CoolScan 9000ED, Lamp 0, RGB'
                    [tmpImageArray(:,:,frame), tmpErrMeVpx(:,:,frame)] = OD2MeV_coolscan_lamp0RGB(data, handles, frame);
                case 'Dresden Scanner'
                    [tmpImageArray(:,:,frame), tmpErrMeVpx(:,:,frame)] = OD2MeV_dresdenScan(data, handles, frame);
                case 'Dresden Dosis'
                    [tmpImageArray(:,:,frame), tmpErrMeVpx(:,:,frame)] = OD2MeV_dresdenDosis(data, handles, frame);
                otherwise
            end
        else
            % old versions compatibility
            switch char(data.parameters.scanner)
                case 'microtek_lamp0'
                    [tmpImageArray(:,:,frame), tmpErrMeVpx(:,:,frame)] = OD2MeV_microtek_lamp0(data, handles, frame);
                case 'microtek_lamp0RGB'
                    [tmpImageArray(:,:,frame), tmpErrMeVpx(:,:,frame)] = OD2MeV_microtek_lamp0RGB(data, handles, frame);
                case 'coolscan_lamp0RGB'
                    [tmpImageArray(:,:,frame), tmpErrMeVpx(:,:,frame)] = OD2MeV_coolscan_lamp0RGB(data, handles, frame);
                case 'dresdenScan'
                    [tmpImageArray(:,:,frame), tmpErrMeVpx(:,:,frame)] = OD2MeV_dresdenScan(data, handles, frame);
                otherwise
            end
        end
    end

    data.data.ImageArray = tmpImageArray;
    data.data.errMeVpx = tmpErrMeVpx;

    set(handles.statustext,'String','Converting to MeV/px... finished','ForegroundColor','black');

    % everything done
    data.process = 5;
    setappdata(0, 'data', data);
    
    set(handles.modifybutton,'Enable','On');
    set(handles.definebeam,'Enable','On');
    set(handles.div_pushbutton,'Enable','On');
    set(handles.deconvplots,'Enable','On');
    set(handles.fitsimplebutton,'Enable','On');
    set(handles.rcfstack_linlog,'Enable','On');
    set(handles.getfig,'Enable','On');
    set(handles.Rehwaldbutton,'Enable','On');
    
    %plot whole stack
    showstack(data, handles, handles.figwin);
    set(handles.statustext,'String','Data loaded... ','ForegroundColor','black');
else
    warndlg('RCF image resolution not set! Check images','!! Warning !!')
    set(handles.statustext,'String','RCF image resolution not set! Check images!','ForegroundColor','red');
end

guidata(hObject,handles);

function openexistingbutton_Callback(hObject, eventdata, handles)
data = getappdata(0,'data');
if isfield(data, 'parameters') && isfield(data.parameters, 'savepath') && ~isempty(data.parameters.savepath)
    [name, pathname] = uigetfile('*.mat','Save Workspace As', data.parameters.savepath);
else
    [name, pathname] = uigetfile('*.mat','Save Workspace As');
end
if ~isequal(name,0)
    cla;
    set(handles.statustext,'String','Opening data...','ForegroundColor','black');
    drawnow;
    set(handles.openexistingbutton,'Enable','On');
    set(handles.parametersbutton,'Enable','On');
    set(handles.rcfloadbutton,'Enable','Off');
    set(handles.rcfdeletebutton,'Enable','Off');
    set(handles.savebutton,'Enable','Off');
    set(handles.modifybutton,'Enable','Off');
    set(handles.div_pushbutton,'Enable','Off');
    set(handles.fitsimplebutton,'Enable','Off');
    set(handles.fitdeconvbutton,'Enable','Off');
    set(handles.definebeam,'Enable','Off');
    set(handles.deconvplots,'Enable','Off');
    set(handles.rcfstack_linlog,'Enable','Off');
    set(handles.getfig,'Enable','Off');
    set(handles.conv2NP,'Enable','Off');
    set(handles.MMspecanabutton,'Enable','Off');
    set(handles.Rehwaldbutton,'Enable','Off');
    
    try rmappdata(0,'data'); end
    data = load(fullfile(pathname, name)); % load "data" from file ..
    
    %compatibility to older versions; we have now numbered rcfs in
    %parameters for better delete possibilities
    if ~isfield(data.parameters, 'rcfconfig_numb')
        data.parameters.rcfconfig_numb = cell(1,length(data.parameters.rcfconfig));
        for i=1:length(data.parameters.rcfconfig)
            data.parameters.rcfconfig_numb{i} = [num2str(i,'%02i'),' ',data.parameters.rcfconfig{i}];
        end
    end
    
    try setappdata(0,'data', data); end
    
    fail = 0;
    % get process flag
    % 0 : begin completely new;
    % 5 : Converted to MeV/px and during image processing
    % 7 : After Simple Fit
    % 8 : After Deconv Fit
    % 10 : After Parameters
    switch (data.process)
        case 5
            set(handles.rcfloadbutton,'Enable','On');
            set(handles.rcfdeletebutton,'Enable','On');
            set(handles.savebutton,'Enable','On');
            set(handles.getfig,'Enable','On');
            set(handles.modifybutton,'Enable','On');
            set(handles.definebeam,'Enable','On');
            set(handles.div_pushbutton,'Enable','On');
            set(handles.deconvplots,'Enable','On');
            set(handles.fitsimplebutton,'Enable','On');
            set(handles.rcfstack_linlog,'Enable','On');
            set(handles.Rehwaldbutton,'Enable','On');
        case {7, 8}
            set(handles.rcfloadbutton,'Enable','On');
            set(handles.rcfdeletebutton,'Enable','On');
            set(handles.savebutton,'Enable','On');
            set(handles.getfig,'Enable','On');
            set(handles.modifybutton,'Enable','On');
            set(handles.definebeam,'Enable','On');
            set(handles.div_pushbutton,'Enable','On');
            set(handles.deconvplots,'Enable','On');
            set(handles.fitsimplebutton,'Enable','On');
            set(handles.fitdeconvbutton,'Enable','On');
            set(handles.conv2NP,'Enable','On');
            set(handles.rcfstack_linlog,'Enable','On');
            set(handles.MMspecanabutton,'Enable','On');
            set(handles.Rehwaldbutton,'Enable','On');
        case 10
            set(handles.rcfloadbutton,'Enable','On');
            set(handles.savebutton,'Enable','On');
        otherwise
            fail = 1;
    end
    if fail
        set(handles.statustext, 'String', 'Data loading failed!', 'ForegroundColor','red');
    else
        if (data.process ~= 10)
            %display all images in montage
            showstack(data, handles, handles.figwin);
            
            if size(data.data.ELOarray,2)==3
                %backward compatibility to old eloss file without quenching
                %correction => give warning message and copy uncorrected values to
                %place where corrected values is in actual version
                set(handles.statustext,'String','Warning! RCF quenching effects are disabled, use new Energyloss GUI !','ForegroundColor','red');
                drawnow;
                %copy column 2 to 4 in ELOarray:
                data.data.ELOarray(:,4,:)=data.data.ELOarray(:,2,:);
                pause(2.5)        
            end
        end
        
        set(handles.statustext,'String',['Data loaded! Process Flag: ' num2str(data.process)],'ForegroundColor','black');
        setappdata(0, 'data', data);
        guidata(hObject,handles);
    end
end

function modifybutton_Callback(hObject, eventdata, handles)
uiwait(imageprocessing);
set(handles.statustext,'String','Processed data','ForegroundColor','black');
data = getappdata(0, 'data');
showstack(data, handles, handles.figwin);
guidata(hObject,handles);

function definebeam_Callback(hObject, eventdata, handles)
uiwait(definebeam_gui);
set(handles.statustext,'String','Beam Data Defined! ... ','ForegroundColor','black');
data = getappdata(0, 'data');
showstack(data, handles, handles.figwin);
guidata(hObject,handles);

% --- Executes on button press in div_pushbutton.
function div_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to div_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%to do: dialog to choose model; run plotdivergence with model 1 or 2
uiwait(choosedivfit);

% --- Executes on button press in deconvplots.
function deconvplots_Callback(hObject, eventdata, handles)
% hObject    handle to deconvplots (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
deconvplots(handles.statustext);

% --- Executes on button press in fitsimplebutton.
function fitsimplebutton_Callback(hObject, eventdata, handles)
% hObject    handle to fitsimplebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiwait(choosefit);
data = getappdata(0, 'data');
if isequal(data.fitparam.yesno, 1)
    set(handles.statustext,'String','Simple fit in operation...','ForegroundColor','black');
    [simple_estimates, simplemodel, spectrum, output] = fitsimple_processor(data);
%     set new values
    data.fitparam.N_start = simple_estimates(1);
    data.fitparam.T_start = simple_estimates(2);
    set(handles.statustext,'String',['Simple fit done. Iterations: ',num2str(output.iterations)],'ForegroundColor', 'black');
    set(handles.fitdeconvbutton,'Enable','On');
    data.process = 7;
    data.results.fitsimple = {simple_estimates, simplemodel, spectrum, output};
    setappdata(0, 'data', data);
end
clear data
guidata(hObject,handles)

% --- Executes on button press in fitdeconvbutton.
function fitdeconvbutton_Callback(hObject, eventdata, handles)
% hObject    handle to fitdeconvbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiwait(choosefit);
data = getappdata(0, 'data');
if isequal(data.fitparam.yesno, 1)
    set(handles.statustext,'String','Deconvolution fit in operation...','ForegroundColor','black');
    [deconv_estimates, model, meanerror, exitflag, output, EvsDOSE_measured, EvsNP_fitted, ENmax] = ...
        fitdeconv_processor(data);
    data.fitparam.N_start = deconv_estimates(1);
    data.fitparam.T_start = deconv_estimates(2);
    switch exitflag
        case 1
            set(handles.statustext,'String',['Deconvolution fit done. Iterations: ',num2str(output.iterations),...
                '. Mean deviation of fit and data: ',num2str(meanerror,'%5.2f'),' %.'],'ForegroundColor','black');
        case 0
            set(handles.statustext,'String','Maximum number of function evaluations or iterations was reached. Fit not optimal!','ForegroundColor','red');
        case -1
            set(handles.statustext,'String','Algorithm was terminated by the output function. Fit not optimal!','ForegroundColor','red');
    end
    handles.process=8;
    data.results.fitdeconv = {deconv_estimates, model, meanerror, exitflag, output, EvsDOSE_measured, EvsNP_fitted, ENmax};
    setappdata(0, 'data', data);
    set(handles.conv2NP,'Enable','On');
    set(handles.MMspecanabutton,'Enable','On');
end
clear data
guidata(hObject,handles)

% --- Executes on button press in savebutton.
function savebutton_Callback(hObject, eventdata, handles)
% hObject    handle to savebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.statustext,'String','Saving file...','ForegroundColor','black');
drawnow;
data = getappdata(0,'data');
if isfield(data.parameters, 'savepath') && ~isempty(data.parameters.savepath)
    [name, data.parameters.savepath] = uiputfile('*.mat','Save Workspace As', data.parameters.savepath);
else
    [name, data.parameters.savepath] = uiputfile('*.mat','Save Workspace As');
end
if ~isequal(name,0)
    save(fullfile(data.parameters.savepath, name), '-struct', 'data');
    setappdata(0, 'data', data);
    set(handles.statustext,'String','Saving file done','ForegroundColor','black');
end
clear data

% --- Executes on selection change in rcfstack_linlog.
function rcfstack_linlog_Callback(hObject, eventdata, handles)
% hObject    handle to rcfstack_linlog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getappdata(0,'data');
showstack(data,handles, handles.figwin);
clear data

% --- Executes on button press in getfig.
function getfig_Callback(hObject, eventdata, handles)
% hObject    handle to getfig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getappdata(0,'data');
h = figure();
showstack(data, handles, h);
clear data

% --- Executes on button press in closebutton.
function closebutton_Callback(hObject, eventdata, handles)
% hObject    handle to closebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try rmappdata(0, 'data'); end
close RCF_spectrum_GUI;

% --- Executes on button press in conv2NP.
function conv2NP_Callback(hObject, eventdata, handles)
% hObject    handle to conv2NP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
conv2particles(handles.statustext);

% --- Executes on button press in rcfdeletebutton.
function rcfdeletebutton_Callback(hObject, eventdata, handles)
% hObject    handle to rcfdeletebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = getappdata(0, 'data');
backup = data.parameters;
uiwait(deleteRCF);

if getappdata(0, 'yesno')==0
    data.parameters=backup;
    setappdata(0, 'data', data);
end

data = getappdata(0, 'data');
showstack(data, handles, handles.figwin);
clear data
guidata(hObject,handles);


% --- Executes on button press in MMspecanabutton.
function MMspecanabutton_Callback(hObject, eventdata, handles)
% hObject    handle to MMspecanabutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MM_spec_analysis

% --- Executes on button press in Rehwaldbutton.
function Rehwaldbutton_Callback(hObject, eventdata, handles)
% hObject    handle to Rehwaldbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = getappdata(0, 'data');
data.fitparam.fitfunction = 'exponential';
data.fitparam.N_start = 1.0e+10;
data.fitparam.T_start = 1;
data.fitparam.maxiter = 1000;
data.fitparam.maxeval = 1000;
data.fitparam.div_a = 20;
data.fitparam.div_b = 25;
data.fitparam.div_c = 3;
data.fitparam.div_d = 75;
data.fitparam.div_r = 15;
data.fitparam.N0main = 1.0e+13;
data.fitparam.kTmain = 4;
data.fitparam.Emax_bnd = 50;
data.fitparam.yesno=1;

[simple_estimates, simplemodel, spectrum, output] = fitsimple_processor_dresden(data);
    
%   set new values
data.fitparam.N_start = simple_estimates(1);
data.fitparam.T_start = simple_estimates(2);
set(handles.fitdeconvbutton,'Enable','On');
data.process = 7;
data.results.fitsimple = {simple_estimates, simplemodel, spectrum, output};
setappdata(0, 'data', data);

clear data
guidata(hObject,handles)